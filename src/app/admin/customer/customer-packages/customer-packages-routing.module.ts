import { NgModule } from '@angular/core';
import { RouterModule, Routes, PreloadAllModules } from '@angular/router';
import { ListPackagesComponent } from './list-packages/list-packages.component';
import { AddPackagesComponent } from './add-packages/add-packages.component';
import { EditPackagesComponent } from './edit-packages/edit-packages.component';
import { AclAuthervice } from '../../../common/services/access-control/acl-auth.service';

export const PackagesRoutes: Routes = [
   { path: '', component: ListPackagesComponent, canActivate: [AclAuthervice], data: { roles: ["Packages"] } },
   { path: 'add', component: AddPackagesComponent, canActivate: [AclAuthervice], data: { roles: ["Packages -Add"] } },
   { path: 'edit/:id', component: EditPackagesComponent, canActivate: [AclAuthervice], data: { roles: ["Packages -Edit"] } }
];
