import { ShukranTransactionsComponent } from './shukran-transactions/shukran-transactions.component';
import { AclAuthervice } from './../../../common/services/access-control/acl-auth.service';
import { ShukranListComponent } from './shukran-list/shukran-list.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ShukranDashboardComponent } from './shukran-dashboard/shukran-dashboard.component';

export const shukranRoutes: Routes = [
  { path: '', component: ShukranDashboardComponent, canActivate: [AclAuthervice], data: { roles: ["Shukran"] } },
  { path: 'list', component: ShukranListComponent, canActivate: [AclAuthervice], data: { roles: ["Shukran-list"] } },
  { path: 'transaction', component: ShukranTransactionsComponent, canActivate: [AclAuthervice], data: { roles: ["Shukran-transaction"] } }
];


