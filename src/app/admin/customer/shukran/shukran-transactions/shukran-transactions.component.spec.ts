import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShukranTransactionsComponent } from './shukran-transactions.component';

describe('ShukranTransactionsComponent', () => {
  let component: ShukranTransactionsComponent;
  let fixture: ComponentFixture<ShukranTransactionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShukranTransactionsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShukranTransactionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
