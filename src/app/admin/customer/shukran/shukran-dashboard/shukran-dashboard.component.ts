import { Router } from '@angular/router';
import { EncDecService } from './../../../../common/services/encrypt-decrypt-service/encrypt_decrypt_service';
import { AccessControlService } from './../../../../common/services/access-control/access-control.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-shukran-dashboard',
  templateUrl: './shukran-dashboard.component.html',
  styleUrls: ['./shukran-dashboard.component.css']
})
export class ShukranDashboardComponent implements OnInit {
  public companyId: any = [];
  public aclCheck;
  session: any;
  email: any;
  constructor(public _aclService: AccessControlService,
    public encDecService: EncDecService,
    private router: Router) {
    this.aclDisplayService();
  }

  ngOnInit() {
  }

  public aclDisplayService() {
    this.aclCheck = JSON.parse(localStorage.getItem('userMenu'));
  }
}


