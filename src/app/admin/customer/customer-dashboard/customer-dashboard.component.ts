import { Component, OnInit } from '@angular/core';
import { AccessControlService } from '../../../common/services/access-control/access-control.service';
import { Router } from '@angular/router';
import { EncDecService } from '../../../common/services/encrypt-decrypt-service/encrypt_decrypt_service';

@Component({
  selector: 'app-customer-dashboard',
  templateUrl: './customer-dashboard.component.html',
  styleUrls: ['./customer-dashboard.component.css']
})
export class CustomerDashboardComponent implements OnInit {
  public companyId: any = [];
  public aclCheck;
  session: any;
  email: any;
  constructor(public _aclService: AccessControlService,
    public encDecService: EncDecService,
    private router: Router) {
    // const company_id: any = localStorage.getItem('user_company');
    // this.session= localStorage.getItem('Sessiontoken');
    // this.email = localStorage.getItem('user_email');
    // this.companyId.push(company_id)
    this.aclDisplayService();
  }

  ngOnInit() {

  }
  public aclDisplayService() {
    this.aclCheck = JSON.parse(localStorage.getItem('userMenu'));
  }
}
