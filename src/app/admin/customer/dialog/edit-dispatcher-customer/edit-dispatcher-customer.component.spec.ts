import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditDispatcherCustomerComponent } from './edit-dispatcher-customer.component';

describe('EditDispatcherCustomerComponent', () => {
  let component: EditDispatcherCustomerComponent;
  let fixture: ComponentFixture<EditDispatcherCustomerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditDispatcherCustomerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditDispatcherCustomerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
