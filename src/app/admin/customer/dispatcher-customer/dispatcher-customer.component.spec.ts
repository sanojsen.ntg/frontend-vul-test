import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DispatcherCustomerComponent } from './dispatcher-customer.component';

describe('DispatcherCustomerComponent', () => {
  let component: DispatcherCustomerComponent;
  let fixture: ComponentFixture<DispatcherCustomerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DispatcherCustomerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DispatcherCustomerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
