import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddNewuserRolemanagementComponent } from './add-newuser-rolemanagement.component';

describe('AddNewuserRolemanagementComponent', () => {
  let component: AddNewuserRolemanagementComponent;
  let fixture: ComponentFixture<AddNewuserRolemanagementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddNewuserRolemanagementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddNewuserRolemanagementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
