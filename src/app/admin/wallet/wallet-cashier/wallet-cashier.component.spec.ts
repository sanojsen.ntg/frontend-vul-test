import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WalletCashierComponent } from './wallet-cashier.component';

describe('WalletCashierComponent', () => {
  let component: WalletCashierComponent;
  let fixture: ComponentFixture<WalletCashierComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WalletCashierComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WalletCashierComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
