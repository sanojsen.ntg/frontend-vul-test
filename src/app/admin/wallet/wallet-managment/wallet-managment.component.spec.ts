import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WalletManagmentComponent } from './wallet-managment.component';

describe('WalletManagmentComponent', () => {
  let component: WalletManagmentComponent;
  let fixture: ComponentFixture<WalletManagmentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WalletManagmentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WalletManagmentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
