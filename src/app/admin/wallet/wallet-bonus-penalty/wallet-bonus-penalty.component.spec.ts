import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WalletBonusPenaltyComponent } from './wallet-bonus-penalty.component';

describe('WalletBonusPenaltyComponent', () => {
  let component: WalletBonusPenaltyComponent;
  let fixture: ComponentFixture<WalletBonusPenaltyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WalletBonusPenaltyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WalletBonusPenaltyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
