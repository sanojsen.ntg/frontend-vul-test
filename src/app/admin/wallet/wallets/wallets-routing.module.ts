import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes, PreloadAllModules } from '@angular/router';
import { AclAuthervice } from '../../../common/services/access-control/acl-auth.service';
import { WalletAddComponent } from '../wallets/wallet-add/wallet-add.component';
import { WalletEditComponent } from '../wallets/wallet-edit/wallet-edit.component';
import { WalletListComponent } from '../wallets/wallet-list/wallet-list.component';
export const WalletsRoute: Routes = [
      { path: '', component: WalletListComponent, canActivate: [AclAuthervice], data: { roles: ["Wallets-List"] } },
      { path: 'wallet-edit', component: WalletEditComponent, canActivate: [AclAuthervice], data: { roles: ["Wallets-Edit"] } },
      { path: 'wallet-add', component: WalletAddComponent, canActivate: [AclAuthervice], data: { roles: ["Wallet - Add"] } },
];

