import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ListShiftsComponent } from './list-shifts/list-shifts.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatInputModule, MatNativeDateModule } from '@angular/material';
import { RouterModule, Routes } from '@angular/router';
import {MatDialogModule} from '@angular/material';
import {ProgressBarModule} from "angular-progress-bar"
import { ShiftDialogComponent} from '../../../common/dialog/shift-dialog/shift-dialog.component';
import { MatSelectModule } from '@angular/material/select';

@NgModule({
  imports: [
    CommonModule,
    NgbModule,
    MatTooltipModule,
    OwlDateTimeModule,
    OwlNativeDateTimeModule,
    FormsModule,
    MatNativeDateModule,
    MatInputModule,
    MatAutocompleteModule,
    RouterModule,
    MatDialogModule,
    ReactiveFormsModule,
    ProgressBarModule,
    MatSelectModule
  ],
  declarations: [ListShiftsComponent, ShiftDialogComponent],
  entryComponents: [ ShiftDialogComponent ]
})
export class ShiftsModule { }
