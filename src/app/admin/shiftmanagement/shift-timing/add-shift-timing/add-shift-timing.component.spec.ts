import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddShiftTimingComponent } from './add-shift-timing.component';

describe('AddShiftTimingComponent', () => {
  let component: AddShiftTimingComponent;
  let fixture: ComponentFixture<AddShiftTimingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddShiftTimingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddShiftTimingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
