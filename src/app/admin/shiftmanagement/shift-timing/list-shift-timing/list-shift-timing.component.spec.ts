import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListShiftTimingComponent } from './list-shift-timing.component';

describe('ListShiftTimingComponent', () => {
  let component: ListShiftTimingComponent;
  let fixture: ComponentFixture<ListShiftTimingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListShiftTimingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListShiftTimingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
