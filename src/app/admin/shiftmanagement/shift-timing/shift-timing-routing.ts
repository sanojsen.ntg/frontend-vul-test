/**
 * These are JavaScript import statements. Angular doesn’t know anything about these.
 */
import { NgModule } from '@angular/core';
import { RouterModule, Routes , PreloadAllModules } from '@angular/router';

/**
 * import components
 */
import { ListShiftTimingComponent } from './list-shift-timing/list-shift-timing.component';
import { AddShiftTimingComponent } from './add-shift-timing/add-shift-timing.component';
import { EditShiftTimingComponent } from './edit-shift-timing/edit-shift-timing.component';

/**
 * Routing path
 * @type {({path: string; component: ListShiftTimingComponent} |
 * {path: string; component: AddShiftTimingComponent} |
 * {path: string; component: EditShiftTimingComponent})[]}
 */
export const shifttimingRoutes: Routes = [
  { path: '', component : ListShiftTimingComponent},
  { path: 'add', component : AddShiftTimingComponent},
  {path: 'edit/:id', component: EditShiftTimingComponent}
];
