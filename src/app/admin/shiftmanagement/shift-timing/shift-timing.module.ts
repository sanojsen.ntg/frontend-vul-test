/**
 * These are JavaScript import statements. Angular doesn’t know anything about these.
 */
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule, Routes , PreloadAllModules } from '@angular/router';
import { AmazingTimePickerModule } from 'amazing-time-picker';
import { MatSelectModule } from '@angular/material/select';
import { MatTooltipModule } from '@angular/material/tooltip';

/**
 * import components and services
 */
import { EditShiftTimingComponent } from './edit-shift-timing/edit-shift-timing.component';
import { ListShiftTimingComponent } from './list-shift-timing/list-shift-timing.component';
import { ShiftTimingsService } from '../../../common/services/shift-timings/shift-timings.service';
import {ShiftTimingsrestService} from '../../../common/services/shift-timings/shift-timingsrest.service';
import { AddShiftTimingComponent } from './add-shift-timing/add-shift-timing.component';
import { TimeFormatPipe } from '../../../common/pipes/time_pipe';
/**
 * The @NgModule decorator lets Angular know that this is an NgModule.
 * @NgModule decorator with its metadata
 */
@NgModule({
  imports: [    /* These are NgModule imports. */
    CommonModule,
    NgbModule,
    FormsModule,
    RouterModule,
    ReactiveFormsModule,
    AmazingTimePickerModule,
    MatSelectModule,
    MatTooltipModule
  ],
  declarations: [ListShiftTimingComponent, AddShiftTimingComponent, EditShiftTimingComponent,TimeFormatPipe],
  providers: [ShiftTimingsService, ShiftTimingsrestService]
})

/**
 * Module class
 */
export class ShiftTimingModule { }
