import { NgModule } from '@angular/core';
import { RouterModule, Routes , PreloadAllModules } from '@angular/router';
import { shifttimingRoutes } from './shift-timing/shift-timing-routing';
import { shiftsRoutes } from './shifts/shifts-routing.module';
import { ShiftmanagementComponent } from './shiftmanagement/shiftmanagement.component';

export const shiftManagementRoutes: Routes = [
  { path:'',component:ShiftmanagementComponent},
  { path: 'shift-timings', children: shifttimingRoutes },
  { path: 'shifts', children: shiftsRoutes }
];
