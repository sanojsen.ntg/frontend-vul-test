import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrderDispHistoryComponent } from './order-disp-history.component';

describe('OrderDispHistoryComponent', () => {
  let component: OrderDispHistoryComponent;
  let fixture: ComponentFixture<OrderDispHistoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrderDispHistoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrderDispHistoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
