import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import { ActivatedRoute } from '@angular/router';
import { AccessControlService } from '../../common/services/access-control/access-control.service';
import { EncDecService } from '../../common/services/encrypt-decrypt-service/encrypt_decrypt_service';
@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.css']
})
export class OrderComponent implements OnInit {
  public aclCheck;
  constructor(public _aclService: AccessControlService, private route: ActivatedRoute,
    public encDecService: EncDecService,
    private router: Router) {
    this.aclDisplayService();
  }

  ngOnInit() {
  }
  public aclDisplayService() {
    this.aclCheck = JSON.parse(localStorage.getItem('userMenu'));
    // const company_id: any = localStorage.getItem('user_company');
    // const session: any = localStorage.getItem('Sessiontoken');
    // const email: any = localStorage.getItem('user_email');
    // var params = {
    //   company_id: [company_id]
    // }
    // var encrypted = this.encDecService.nwt(session, params);
    // var enc_data = {
    //   data: encrypted,
    //   email: email
    // }
    // this._aclService.getAclUserMenu(enc_data).then((dec) => {
    //   if (dec && dec.status == 200) {
    //     var data: any = this.encDecService.dwt(session, dec.data);
    //     this.aclCheck = data.menu;
    //   }
    // })

  }
}
