import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FlaggedOrdersComponent } from './flagged-orders.component';

describe('FlaggedOrdersComponent', () => {
  let component: FlaggedOrdersComponent;
  let fixture: ComponentFixture<FlaggedOrdersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FlaggedOrdersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FlaggedOrdersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
