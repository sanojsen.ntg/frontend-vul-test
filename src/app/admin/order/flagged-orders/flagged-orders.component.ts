import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { OrderService } from '../../../common/services/order/order.service';
import { MatAutocompleteSelectedEvent } from '@angular/material';
import * as moment from 'moment/moment';
import { ToastsManager } from 'ng2-toastr';
import { JwtService } from '../../../common/services/api/jwt.service';
import { Router } from '@angular/router';
import { Angular2Csv } from 'angular2-csv';
import { EncDecService } from '../../../common/services/encrypt-decrypt-service/encrypt_decrypt_service';
import { DriverService } from '../../../common/services/driver/driver.service';
import { AccessControlService } from '../../../common/services/access-control/access-control.service';

@Component({
  selector: 'app-flagged-orders',
  templateUrl: './flagged-orders.component.html',
  styleUrls: ['./flagged-orders.component.css']
})
export class FlaggedOrdersComponent implements OnInit {
  orderCtrl: FormControl = new FormControl();
  allOrderIddata: any;
  ordersFilter: any = [];
  unique_order_id: any = [];
  issue_type = [];
  public selectedMoment;
  public selectedMoment1;
  public max = new Date();
  generated_by: any = 'customer';
  searchLoader: boolean = false;
  orderData: any[];
  pageSize = 10;
  pageNo = 0;
  orderLength: any;
  public searchSubmit = false;
  session: string;
  email: string;
  companyId: any = [];
  downloads: boolean;

  constructor(private _orderService: OrderService,
    public toastr: ToastsManager,
    public jwtService: JwtService,
    private router: Router,
    public encDecService: EncDecService,
    private _aclService: AccessControlService,
    private _driverService: DriverService) {
    const company_id: any = localStorage.getItem('user_company');
    this.session = localStorage.getItem('Sessiontoken');
    this.email = localStorage.getItem('user_email');
    this.companyId.push(company_id);
  }

  ngOnInit() {
    this.selectedMoment = this.getDate(moment().subtract(1, 'days').format('YYYY-MM-DD HH:mm:ss'));
    this.selectedMoment1 = this.getDate1(moment(new Date()).format('YYYY-MM-DD HH:mm:ss'));
    this.orderCtrl.valueChanges
      .debounceTime(500)
      .distinctUntilChanged()
      .switchMap((query) => {
        var params = {
          company_id: this.companyId,
          offset: 0,
          limit: 10,
          sortOrder: 'desc',
          sortByColumn: '_id',
          search_keyword: query
        }
        var encrypted = this.encDecService.nwt(this.session, params);
        var enc_data = {
          data: encrypted,
          email: this.email
        }
        return this._orderService.getAllOrderId(enc_data)
      }
      )
      .subscribe(dec => {
        if (dec && dec.status == 200) {
          var res: any = this.encDecService.dwt(this.session, dec.data);
          this.allOrderIddata = res.getOrders;
        }
      });
    this.driverCtrl.valueChanges
      .debounceTime(400)
      .switchMap((query) => {
        var params = {
          company_id: this.companyId,
          offset: 0,
          limit: 10,
          sortOrder: 'desc',
          sortByColumn: '_id',
          search_keyword: query
        }
        var encrypted = this.encDecService.nwt(this.session, params);
        var enc_data = {
          data: encrypted,
          email: this.email
        }
        return this._driverService.searchForDriver(enc_data)
      })
      .subscribe(dec => {
        if (dec && dec.status == 200) {
          var res: any = this.encDecService.dwt(this.session, dec.data);
          this.driverList = res.driver;
        }
      });
    this.getOrders();
    this.aclDisplayService();
  }
  public aclDisplayService() {
    var params = {
      company_id: this.companyId
    }
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: this.email
    }
    this._aclService.getAclUserMenu(enc_data).then((dec) => {
      if (dec && dec.status == 200) {
        var data: any = this.encDecService.dwt(this.session, dec.data);
        for (let i = 0; i < data.menu.length; i++) {
          if (data.menu[i] == 'Downloads-flagged-orders') {
            this.downloads = true;
          }
        };
      }
    });
  }
  removeOrder(fruit): void {
    const index = this.ordersFilter.indexOf(fruit);
    if (index >= 0) {
      this.ordersFilter.splice(index, 1);
      this.unique_order_id.splice(index, 1)
    }
  }
  orderIdselected(event: MatAutocompleteSelectedEvent): void {
    if (this.ordersFilter.indexOf(event.option.value.unique_order_id) > -1) {
      return
    }
    else {
      this.ordersFilter.push(event.option.value.unique_order_id);
      this.unique_order_id.push(event.option.value.unique_order_id);
      this.orderCtrl.setValue(null);
    }
  }
  getDate(selectedMoment): any {
    if (selectedMoment) {
      let dateOld: Date = new Date(selectedMoment);
      return dateOld;
    }
  }
  getDate1(selectedMoment1): any {
    if (selectedMoment1) {
      let dateOld: Date = new Date(selectedMoment1);
      return dateOld;
    }
  }
  checkShiftstartTime() {
    this.selectedMoment1 = '';
  }
  public statusColor(data) {
    if (this.generated_by == data) data = "rgb(191, 0, 0)";
    else data = "#211c47";
    return data;
  }
  public colorFilter(status) {
    this.generated_by = status;
    this.getOrders();
  }
  public getOrders() {
    this.searchLoader = true;
    this.orderData = [];
    this.pageNo = 1;
    let params;
    if (this.unique_order_id && this.unique_order_id.length > 0) {
      params = {
        company_id: this.companyId,
        offset: 0,
        limit: this.pageSize,
        sortOrder: 'desc',
        sortByColumn: 'updated_at',
        unique_order_id: this.unique_order_id ? this.unique_order_id : '',
        start_date: '',
        end_date: '',
        driver_id: [],
        generate_by: '',
        issues: []
      };
    } else {
      params = {
        company_id: this.companyId,
        offset: 0,
        limit: this.pageSize,
        sortOrder: 'desc',
        sortByColumn: 'updated_at',
        unique_order_id: this.unique_order_id ? this.unique_order_id : '',
        startDate: this.selectedMoment ? moment(this.selectedMoment).format('YYYY-MM-DD HH:mm:ss') : '',
        endDate: this.selectedMoment1 ? moment(this.selectedMoment1).format('YYYY-MM-DD HH:mm:ss') : '',
        driver_id: this.driverSelectedList,
        generate_by: this.generated_by ? this.generated_by : '',
        issues: this.issue_type
      };
    }
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: this.email
    }
    this._orderService.getFlaggedTrips(enc_data).then((dec) => {
      if (dec && dec.status == 200) {
        var data: any = this.encDecService.dwt(this.session, dec.data);
        this.orderData = data.data;
        this.orderLength = data.count;
      }
      this.searchLoader = false;
    });
  }
  public creatingcsv = false;
  public getconstantsforcsv() {
    if (this.creatingcsv) {
      this.toastr.warning('Export Operation already in progress.');
    } else {
      this.creatingcsv = true;
      var params = { company_id: this.companyId };
      var encrypted = this.encDecService.nwt(this.session, params);
      var enc_data = {
        data: encrypted,
        email: this.email
      }
      this._orderService.getRejectedCSVConstants(enc_data).then((dec) => {
        if (dec) {
          if (dec.status == 200) {
            var res: any = this.encDecService.dwt(this.session, dec.data);
            let offset = res.data;
            this.createCsv(offset.order_fetch_interval, offset.order_fetch_offset);
          } else {
            this.creatingcsv = false
            this.toastr.error("Please try again")
          }
        }
      })
    }
  }
  public livetrackingOrder;
  public progressvalue = 0;
  public createCsv(interval_value, offset) {
    let params = {
      offset: 0,
      limit: 1,
      company_id: this.companyId,
      sortOrder: 'desc',
      sortByColumn: 'updated_at',
      unique_order_id: this.unique_order_id ? this.unique_order_id : '',
      startDate: this.selectedMoment ? moment(this.selectedMoment).format('YYYY-MM-DD HH:mm:ss') : '',
      endDate: this.selectedMoment1 ? moment(this.selectedMoment1).format('YYYY-MM-DD HH:mm:ss') : '',
      driver_id: this.driverSelectedList,
      generate_by: this.generated_by ? this.generated_by : '',
      issues: this.issue_type
    };
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: this.email
    }
    this._orderService.getFlaggedTrips(enc_data).then((res) => {
      if (res && res.status == 201) {
        this.creatingcsv = false;
        this.toastr.error('Please try again after some time');
      } else if (res && res.status == 200) {
        var res: any = this.encDecService.dwt(this.session, res.data);
        var count = res.count;
        let res1Array = [];
        //alert("total_orders" + count);
        let i = 0;
        let fetch_status = true;
        let that = this;
        this.livetrackingOrder = setInterval(function () {
          if (fetch_status) {
            if (res1Array.length >= count) {
              that.progressvalue = 100;
              that.creatingcsv = false;
              clearInterval(that.livetrackingOrder);
              let labels = [
                'ID',
                'Customer cancellation reason',
                'Flagged drivers',
                'Issue types',
                'Order timed out drivers',
                'Rejected drivers',
                'Timestamp'
              ];
              var options =
              {
                fieldSeparator: ',',
                quoteStrings: '"',
                decimalseparator: '.',
                showLabels: true,
                showTitle: false,
                useBom: true,
                headers: (labels)
              };
              new Angular2Csv(res1Array, 'Flagged Trip speeds-Order' + moment().format('YYYY-MM-DD_HH_mm_ss'), options);
            } else {
              fetch_status = false;
              let diff = count - i;
              let perc = diff / count;
              let remaining = 100 * perc;
              that.progressvalue = 100 - remaining;
              let new_params = {
                offset: i,
                limit: parseInt(offset),
                sortOrder: 'desc',
                sortByColumn: 'updated_at',
                unique_order_id: that.unique_order_id ? that.unique_order_id : '',
                startDate: that.selectedMoment ? moment(that.selectedMoment).format('YYYY-MM-DD HH:mm:ss') : '',
                endDate: that.selectedMoment1 ? moment(that.selectedMoment1).format('YYYY-MM-DD HH:mm:ss') : '',
                driver_id: that.driverSelectedList,
                generate_by: that.generated_by ? that.generated_by : '',
                issues: that.issue_type,
                company_id: that.companyId
              };
              var encrypted = that.encDecService.nwt(that.session, new_params);
              var enc_data = {
                data: encrypted,
                email: that.email
              }
              that._orderService.getFlaggedTrips(enc_data).then((res) => {
                if (res && res.status == 200) {
                  var data: any = that.encDecService.dwt(that.session, res.data);
                  res = data.data;
                  if (res.length > 0) {
                    for (let j = 0; j < res.length; j++) {
                      const csvArray = res[j];
                      let flagged_drivers = '';
                      if (csvArray.flaggedDrivers && csvArray.flaggedDrivers.length > 0) {
                        for (var fc in csvArray.flaggedDrivers)
                          flagged_drivers += csvArray.flaggedDrivers[fc].username + ', ';
                      }
                      let issue_type = '';
                      if (csvArray.flagged_cases.issues_type && csvArray.flagged_cases.issues_type.length > 0) {
                        for (var fc in csvArray.flagged_cases.issues_type)
                          issue_type += csvArray.flagged_cases.issues_type[fc] + ', ';
                      }
                      let order_timed_out_drivers = '';
                      if (csvArray.flagged_cases.order_timed_out_drivers && csvArray.flagged_cases.order_timed_out_drivers.length > 0) {
                        for (var fc in csvArray.flagged_cases.order_timed_out_drivers)
                          order_timed_out_drivers += csvArray.flagged_cases.order_timed_out_drivers[fc] + ', ';
                      }
                      let rejected_drivers = '';
                      if (csvArray.flagged_cases.rejected_drivers && csvArray.flagged_cases.rejected_drivers.length > 0) {
                        for (var fc in csvArray.flagged_cases.rejected_drivers)
                          rejected_drivers += csvArray.flagged_cases.rejected_drivers[fc] + ', ';
                      }
                      res1Array.push({
                        id: csvArray.unique_order_id ? csvArray.unique_order_id : 'N/A',
                        custome_cancellation: csvArray.flagged_cases && csvArray.flagged_cases.customer_cancellation ? csvArray.flagged_cases.customer_cancellation : 'N/A',
                        flagged_drivers: flagged_drivers ? flagged_drivers : 'N/A',
                        issue_type: issue_type ? issue_type : 'N/A',
                        order_timed_out_drivers: order_timed_out_drivers ? order_timed_out_drivers : 'N/A',
                        rejected_drivers: rejected_drivers ? rejected_drivers : 'N/A',
                        timestamp: csvArray.created_at ? csvArray.created_at : 'N/A'
                      });
                      if (j == res.length - 1) {
                        i = i + parseInt(offset);
                        fetch_status = true;
                      }
                    }
                  }
                }
              }).catch((Error) => {
                that.toastr.warning('Network reset, csv creation restarted')
                that.createCsv(interval_value, offset);
              })
            }
          }
        }, parseInt(interval_value));
      }
    })
  }
  reset() {
    this.pageNo = 1;
    this.unique_order_id = [];
    this.ordersFilter = [];
    this.generated_by = "customer";
    this.ngOnInit();
    this.issue_type = [],
      this.searchSubmit = false;
  }
  pagingAgent(data) {
    this.searchLoader = true;
    this.orderData = [];
    //this.orderData = [];
    this.pageNo = (data * this.pageSize) - this.pageSize;
    const params = {
      offset: this.pageNo,
      limit: this.pageSize,
      company_id: this.companyId,
      sortOrder: 'desc',
      sortByColumn: 'updated_at',
      unique_order_id: this.unique_order_id ? this.unique_order_id : '',
      startDate: this.selectedMoment ? moment(this.selectedMoment).format('YYYY-MM-DD HH:mm:ss') : '',
      endDate: this.selectedMoment1 ? moment(this.selectedMoment1).format('YYYY-MM-DD HH:mm:ss') : '',
      driver_id: this.driverSelectedList,
      generate_by: this.generated_by ? this.generated_by : '',
      issues: this.issue_type
    };
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: this.email
    }
    this._orderService.getFlaggedTrips(enc_data).then((dec) => {
      if (dec && dec.status == 200) {
        var data: any = this.encDecService.dwt(this.session, dec.data);
        this.orderData = data.data;
        this.orderLength = data.count;
      }
      this.searchLoader = false;
    });
  }
  public driverList: Array<string> = [];
  public driverSelectedList: Array<string> = [];
  driverCtrl: FormControl = new FormControl();
  driversFilter: any = [];
  public driverIdselected(value: any): void {
    if (this.driversFilter.indexOf(value.option.value) > -1) {
      return
    }
    else {
      this.driversFilter.push(value.option.value);
      this.driverSelectedList.push(value.option.value._id);
      this.driverCtrl.setValue(null);
    }
  }
  public removeDriver(fruit: any): void {
    const index = this.driversFilter.indexOf(fruit);
    if (index >= 0) {
      this.driversFilter.splice(index, 1);
      this.driverSelectedList.splice(index, 1)
    }
  }
  public driver_id;
  displayFnDriver(data): string {
    return data ? data.username : data;
  }
  displayFnOrderId(data): string {
    return data ? '' : '';
  }
}
