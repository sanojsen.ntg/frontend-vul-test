import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { Router } from '@angular/router';
import { BlockedDeviceService } from '../../../../common/services/blocked-devices/blocked-devices.service';
import { DeviceService } from '../../../../common/services/devices/devices.service';
import { JwtService } from '../../../../common/services/api/jwt.service';
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';
import { MatDialog, MatDialogRef, MAT_DIALOG_DEFAULT_OPTIONS } from '@angular/material';
import { DeleteDialogComponent } from '../../../../common/dialog/delete-dialog/delete-dialog.component'
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { Overlay } from '@angular/cdk/overlay';
import { ToastsManager } from 'ng2-toastr';
import { EncDecService } from '../../../../common/services/encrypt-decrypt-service/encrypt_decrypt_service';


@Component({
  selector: 'app-list-blocked-devices',
  templateUrl: './list-blocked-devices.component.html',
  styleUrls: ['./list-blocked-devices.component.css']
})
export class ListBlockedDevicesComponent implements OnInit {

  public companyId: any = [];
  key: string = '';
  reverse: boolean = false;
  sortOrder = 'asc';
  public keyword;
  del = false;
  searchValue: string = '';
  pageSize = 10;
  public pageNo;
  public searchSubmit = false;
  public blockedDevice;
  totalPage = [1];
  public _id;
  blockedDeviceLength;
  page = 1;
  public blockedDeviceData;
  email: string;
  session: string;

  constructor(private _blockedDevicesService: BlockedDeviceService,
    public _deviceService: DeviceService,
    private router: Router,
    public jwtService: JwtService,
    public dialog: MatDialog,
    public overlay: Overlay,
    private spinnerService: Ng4LoadingSpinnerService,
    public toastr: ToastsManager,
    public encDecService: EncDecService,
    vcr: ViewContainerRef) {
    const company_id: any = localStorage.getItem('user_company');
    this.session = localStorage.getItem('Sessiontoken');
    this.email = localStorage.getItem('user_email');
    this.companyId.push(company_id);
  }

  ngOnInit() {
    this.getBlockedDeviceListing();
  }

  public getBlockedDeviceListing() {
    this.searchSubmit = true;
    const params = {
      offset: 0,
      limit: this.pageSize,
      sortOrder: 'desc',
      sortByColumn: 'updated_at',
      company_id: this.companyId
    };
    this.del = true;
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: this.email
    }
    this._blockedDevicesService.getBlockDevicesForDeletion(enc_data).then((dec) => {
      if (dec && dec.status == 200) {
        var data: any = this.encDecService.dwt(this.session, dec.data);
        this.blockedDeviceData = data.blockDevices;
        this.blockedDeviceLength = data.blockDevices.length;
        this.blockedDeviceLength = data.totalCount;
      }
      this.searchSubmit = false;
      this.del = false;
    });
  }

  openDialog(id): void {
    let dialogRef = this.dialog.open(DeleteDialogComponent, {
      width: '450px',
      scrollStrategy: this.overlay.scrollStrategies.noop(),
      data: { text: 'Are you sure you want to remove this Record' }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result == true) {
        this.toastr.success('Blocked device deleted successfully');
        this.del = true;
        this.deleteBlockedDevice(id);
      } else {
        this.toastr.error('Blocked device deletion cancelled');
      }
    });
  }

  public deleteBlockedDevice(id) {
    this.searchSubmit = true;
    var params = {
      company_id: this.companyId,
      _id: id
    }
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: this.email
    }
    this._blockedDevicesService.updateBlockedDeviceForDeletion(enc_data).then((dec) => {
      if (dec && dec.status == 200) {
        this.blockedDeviceData.splice(id, 1);
      }
      this.searchSubmit = false;
      this.refresh();
    });
  }

  pagingAgent(data) {
    this.searchSubmit = true;
    this.blockedDeviceData = [];
    this.pageNo = (data * this.pageSize) - this.pageSize;
    const params = {
      offset: this.pageNo,
      limit: this.pageSize,
      sortOrder: 'desc',
      sortByColumn: 'updated_at',
      company_id: this.companyId,
      deviceid: null
    };
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: this.email
    }
    this._blockedDevicesService.getBlockedDevicesListing(enc_data).then((dec) => {
      if (dec && dec.status == 200) {
        var data: any = this.encDecService.dwt(this.session, dec.data);
        this.blockedDeviceData = data.blockDevices;
        this.searchSubmit = false;
        this.blockedDeviceLength = data.blockDevices.length;
      }
      this.searchSubmit = false;
    });
  }

  sort(key) {
    this.searchSubmit = true;
    this.key = key;
    if (this.sortOrder == 'desc') {
      this.sortOrder = 'asc';
    } else {
      this.sortOrder = 'desc';
    }

    this.blockedDeviceData = [];
    const params = {
      offset: this.pageNo,
      limit: this.pageSize,
      sortOrder: this.sortOrder,
      sortByColumn: this.key,
      deviceid: this.keyword,
      company_id: this.companyId
    };
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: this.email
    }
    this._blockedDevicesService.getBlockedDevicesListing(enc_data).then((dec) => {
      if (dec && dec.status == 200) {
        var data: any = this.encDecService.dwt(this.session, dec.data);
        this.blockedDeviceData = data.blockDevices;
        this.searchSubmit = false;
        this.reverse = !this.reverse;
      }
      this.searchSubmit = false;
    });
  }

  public getBlockedDevices(keyword) {
    this.searchSubmit = true;
    const params = {
      offset: 0,
      limit: this.pageSize,
      sortOrder: 'desc',
      sortByColumn: 'updated_at',
      deviceid: keyword,
      company_id: this.companyId
    };
    this.blockedDeviceData = [];
    if (keyword) {
      const params = {
        offset: 0,
        limit: this.pageSize,
        sortOrder: 'desc',
        sortByColumn: 'updated_at',
        deviceid: keyword,
        company_id: this.companyId
      };
      var encrypted = this.encDecService.nwt(this.session, params);
      var enc_data = {
        data: encrypted,
        email: this.email
      }
      this.del = true;
      this._blockedDevicesService.getBlockedDevicesListing(enc_data).then((dec) => {
        if (dec && dec.status == 200) {
          var data: any = this.encDecService.dwt(this.session, dec.data);
          this.blockedDeviceData = data.blockDevices;
          this.searchSubmit = false;
          this.blockedDeviceLength = data.blockDevices.length;
        }
        this.searchSubmit = false;
        this.del = false;
      });
    } else {
      const params = {
        offset: 0,
        limit: this.pageSize,
        sortOrder: 'desc',
        sortByColumn: 'updated_at',
        deviceid: '',
        company_id: this.companyId
      };
      var encrypted = this.encDecService.nwt(this.session, params);
      var enc_data = {
        data: encrypted,
        email: this.email
      }
      this.del = true;
      this.blockedDeviceData = [];
      this._blockedDevicesService.getBlockedDevicesListing(enc_data).then((dec) => {
        if (dec && dec.status == 200) {
          var data: any = this.encDecService.dwt(this.session, dec.data);
          this.blockedDeviceData = data.blockDevices;
          this.blockedDeviceLength = data.blockDevices.count;
          this.blockedDeviceLength = data.blockDevices.length;
        }
        this.searchSubmit = false;
        this.del = false;
      });
    }
  }

  refresh() {
    this.del = true;
    this.searchValue = '';
    this.ngOnInit();
  }
}
