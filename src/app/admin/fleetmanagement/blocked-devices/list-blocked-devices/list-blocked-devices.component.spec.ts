import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListBlockedDevicesComponent } from './list-blocked-devices.component';

describe('ListBlockedDevicesComponent', () => {
  let component: ListBlockedDevicesComponent;
  let fixture: ComponentFixture<ListBlockedDevicesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListBlockedDevicesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListBlockedDevicesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
