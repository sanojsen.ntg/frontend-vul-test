import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListDriverStatsComponent } from './list-driver-stats.component';

describe('ListDriverStatsComponent', () => {
  let component: ListDriverStatsComponent;
  let fixture: ComponentFixture<ListDriverStatsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListDriverStatsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListDriverStatsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
