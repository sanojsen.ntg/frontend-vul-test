import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListDriverAcceptanceComponent } from './list-driver-acceptance.component';

describe('ListDriverAcceptanceComponent', () => {
  let component: ListDriverAcceptanceComponent;
  let fixture: ComponentFixture<ListDriverAcceptanceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListDriverAcceptanceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListDriverAcceptanceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
