import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { EncDecService } from '../../../../common/services/encrypt-decrypt-service/encrypt_decrypt_service';
import { DriverService } from '../../../../common/services/driver/driver.service';

@Component({
  selector: 'app-driver-shift-activity',
  templateUrl: './driver-shift-activity.component.html',
  styleUrls: ['./driver-shift-activity.component.css']
})
export class DriverShiftActivityComponent {
  driver_id: any;
  pNo: number=1;
  searchLoader: boolean;
  company_id: string;
  session: string;
  email: string;
  pageNo: number;
  pageSize=10;
  shiftData: any[];
  shiftLength = 0;
  username: any;


  constructor(public dialogRef: MatDialogRef<DriverShiftActivityComponent>,
    public encDecService: EncDecService,
    public _driverService: DriverService,
    @Inject(MAT_DIALOG_DATA) public data: any) {
    this.company_id = localStorage.getItem('user_company');
    this.session = localStorage.getItem('Sessiontoken');
    this.email = localStorage.getItem('user_email');
    this.driver_id = data.driver_id;
    this.username=data.username;
    this.getShiftInfo();
  }
  public getShiftInfo() {
    this.pNo = 1;
    this.searchLoader = true;
    const params = {
      offset: 0,
      limit: 10,
      driver_id: this.driver_id,
      company_id: [this.company_id],
      startDate: '',
      endDate: ''
    };
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: this.email
    }
    this._driverService.getDriverShiftInfo(enc_data).subscribe((dec) => {
      if (dec && dec.status == 200) {
        var data: any = this.encDecService.dwt(this.session, dec.data);
        this.shiftData = data.driver;
        //data.driver
        this.shiftLength = data.count;
      }
      this.searchLoader = false;
    });
  }
  pagingAgent(data) {
    this.searchLoader = true;
    this.shiftData = [];
    this.pageNo = (data * this.pageSize) - this.pageSize;
    this.pNo=data;
    const params = {
      offset: this.pageNo,
      limit: this.pageSize,
      driver_id: this.driver_id,
      company_id: [this.company_id],
      startDate: '',
      endDate: ''
    };
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: this.email
    }
    this._driverService.getDriverShiftInfo(enc_data).subscribe((dec) => {
      if (dec && dec.status == 200) {
        var data: any = this.encDecService.dwt(this.session, dec.data);
        this.shiftData = data.driver;
      }
      this.searchLoader = false;
    });
  }
  // expandRow(shift, index) {
  //   if (shift.shiftLogs && shift.shiftLogs.length > 0) {
  //     let i = 1;
  //     if (shift.expanded) {
  //       i=shift.shiftLogs.length;
  //       while(i>0){
  //         this.shiftData.splice(index + i, 1);
  //         i--;
  //       }
  //     }
  //     else {
  //       shift.shiftLogs.forEach(element => {
  //         this.shiftData.splice(index + i, 0, element);
  //         i++;
  //       });
  //     }
  //     this.shiftData[index].expanded = shift.expanded ? false : true;
  //   }
  // }
}
