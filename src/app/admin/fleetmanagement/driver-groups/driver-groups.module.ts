import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule, Routes , PreloadAllModules } from '@angular/router';
import { MatTooltipModule } from '@angular/material/tooltip';
import { ListDriverGroupsComponent } from './list-driver-groups/list-driver-groups.component';
import { DriverGroupService } from '../../../common/services/driver_groups/driver_groups.service';
import { DriverGroupRestService } from '../../../common/services/driver_groups/driver_groupsrest.service';
import { AddDriverGroupsComponent } from './add-driver-groups/add-driver-groups.component';
import { EditDriverGroupsComponent } from './edit-driver-groups/edit-driver-groups.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { MatSelectModule } from '@angular/material/select';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    RouterModule,
    FormsModule,
    MatTooltipModule,
    NgbModule,
    MatSelectModule
  ],
  declarations: [ListDriverGroupsComponent, AddDriverGroupsComponent, EditDriverGroupsComponent],
  exports: [
    RouterModule
  ],
  providers :[ DriverGroupService,DriverGroupRestService]
})
export class DriverGroupsModule { }
