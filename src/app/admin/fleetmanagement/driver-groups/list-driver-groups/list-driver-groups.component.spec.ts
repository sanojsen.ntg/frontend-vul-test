import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListDriverGroupsComponent } from './list-driver-groups.component';

describe('ListDriverGroupsComponent', () => {
  let component: ListDriverGroupsComponent;
  let fixture: ComponentFixture<ListDriverGroupsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListDriverGroupsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListDriverGroupsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
