import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { Router } from '@angular/router';
import { AdditionalService } from '../../../../common/services/additional_service/additional_service.service';
import { MatDialog, MatDialogRef, MAT_DIALOG_DEFAULT_OPTIONS } from '@angular/material';
import { DeleteDialogComponent } from '../../../../common/dialog/delete-dialog/delete-dialog.component';
import { VehicleModelsService } from '../../../../common/services/vehiclemodels/vehiclemodels.service';
import { Overlay } from '@angular/cdk/overlay';
import { ToastsManager } from 'ng2-toastr';
import { AccessControlService } from '../../../../common/services/access-control/access-control.service';
import { JwtService } from '../../../../common/services/api/jwt.service';
import { EncDecService } from '../../../../common/services/encrypt-decrypt-service/encrypt_decrypt_service';


@Component({
  selector: 'app-list-service',
  templateUrl: './list-service.component.html',
  styleUrls: ['./list-service.component.css']
})
export class ListServiceComponent implements OnInit {
  public serviceGroupData;
  key: string = '';
  searchValue: string = '';
  reverse: boolean = false;
  pageSize = 10;
  public keyword;
  public name;
  public is_search = false;
  pageNo = 0;
  totalPage = [1];
  public _id;
  page = 1;
  sortOrder = 'asc';
  public serviceGroupLength;
  public searchSubmit = false;
  del = false;
  public service_type = '';

  public aclAdd = false;
  public aclEdit = false;
  public aclDelete = false;
  public companyId: any = [];
  email: string;
  session: string;

  constructor(private router: Router,
    private _additional_Service: AdditionalService,
    public dialog: MatDialog,
    public overlay: Overlay,
    private _toasterservice: ToastsManager,
    vcr: ViewContainerRef,
    public _aclService: AccessControlService,
    public encDecService: EncDecService,
    public jwtService: JwtService, ) {
    const company_id: any = localStorage.getItem('user_company');
    this.session = localStorage.getItem('Sessiontoken');
    this.email = localStorage.getItem('user_email');
    this.companyId.push(company_id);
    this.aclDisplayService();
  }

  ngOnInit() {
    this.is_search = false;
    const params = {
      offset: 0,
      limit: this.pageSize,
      sortOrder: 'desc',
      sortByColumn: 'updated_at',
      company_id: this.companyId
    };
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: this.email
    }
    this.del = true;
    this._additional_Service.getaddService(enc_data).subscribe((dec) => {
      if (dec && dec.status == 200) {
        var data: any = this.encDecService.dwt(this.session, dec.data);
        this.serviceGroupData = data.getService;
        this.serviceGroupLength = data.count;
      }
      this.del = false;
    });
  }
  public aclDisplayService() {
    var params = {
      company_id: this.companyId
    }
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: this.email
    }
    this._aclService.getAclUserMenu(enc_data).then((dec) => {
      if (dec && dec.status == 200) {
        var data: any = this.encDecService.dwt(this.session, dec.data);
        for (let i = 0; i < data.menu.length; i++) {
          if (data.menu[i] == "Additional Services - Add") {
            this.aclAdd = true;
          } else if (data.menu[i] == "Additional Services - Edit") {
            this.aclEdit = true;
          } else if (data.menu[i] == "Shift Timings - Delete") {
            this.aclDelete = true;
          }
        };
      }
    })
  }

  /**
   * Confirmation popup for deleting particular additional service record
   * @param id 
   */
  openDialog(i): void {
    let dialogRef = this.dialog.open(DeleteDialogComponent, {
      width: '450px',
      scrollStrategy: this.overlay.scrollStrategies.noop(),
      data: { text: 'Are you sure you want to remove this data' }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result == true) {
        this.del = true;
        this.deleteVehicle(i);
      }
    });
  }

  /**
   * Delete vehicle
   * @param id 
   */
  public deleteVehicle(id) {
    this.searchSubmit = true;
    var params = {
      company_id: this.companyId,
      _id: id
    }
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: this.email
    }
    this._additional_Service.updateServiceForDeletion(enc_data).then((dec) => {
      if (dec) {
        if (dec.status === 200) {
          this.serviceGroupData.splice(id, 1);
          this._toasterservice.success('Additional service deleted successfully.');
        } else if (dec.status === 201) {
        } else if (dec.status === 203) {
          this._toasterservice.warning('This additional service is assign to vehicle so you cannot delete it.');
        } else if (dec.status === 207) {
          this._toasterservice.warning('This additional service is assign to tariff so you cannot delete it.');
        }
      }
      this.refetchVehicle();
      this.serviceGroupData.splice(id, 1);
      this.searchSubmit = false;
    });
  }

  /**
    * To refresh vehicle records showing in the grid
    */
  public refetchVehicle() {
    if (this.is_search == true) {
      this.getServices();
    }
    else {
      this.del = true;
      this.ngOnInit();
    }
  }

  /**
   * To reset the search filters and reload the additional service records
   */
  public reset() {
    this.name = '';
    this.service_type = '';
    this.ngOnInit();
  }

  /**
  * Pagination for additional service module
  * @param data
  */
  pagingAgent(data) {

    this.pageNo = (data * this.pageSize) - this.pageSize;
    let params;
    if (this.is_search) {
      params = {
        offset: this.pageNo,
        limit: this.pageSize,
        sortOrder: this.key ? this.sortOrder : 'desc',
        sortByColumn: this.key ? this.key : 'updated_at',
        name: this.name ? this.name : '',
        service_type: this.service_type ? this.service_type : '',
        company_id: this.companyId
      };
    } else {
      params = {
        offset: this.pageNo,
        limit: this.pageSize,
        sortOrder: this.key ? this.sortOrder : 'desc',
        sortByColumn: this.key ? this.key : 'updated_at',
        company_id: this.companyId
      };
    }
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: this.email
    }
    this._additional_Service.getaddService(enc_data).subscribe((dec) => {
      if (dec && dec.status == 200) {
        var data: any = this.encDecService.dwt(this.session, dec.data);
        this.serviceGroupData = data.getService;
        this.serviceGroupLength = data.count;
      }
    });
  }

  /**
   * 
   * For sorting additional service records 
   * @param data
   */
  sort(key) {
    this.key = key;
    if (this.sortOrder == 'desc') {
      this.sortOrder = 'asc';
    } else {
      this.sortOrder = 'desc';
    }

    let params;
    if (this.is_search) {
      params = {
        offset: this.pageNo,
        limit: this.pageSize,
        sortOrder: this.sortOrder,
        sortByColumn: this.key,
        name: this.name ? this.name : '',
        service_type: this.service_type ? this.service_type : '',
        company_id: this.companyId
      };
    } else {
      params = {
        offset: this.pageNo,
        limit: this.pageSize,
        sortOrder: this.sortOrder,
        sortByColumn: this.key,
        company_id: this.companyId
      };
    }
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: this.email
    }
    this.del = true;
    this._additional_Service.getaddService(enc_data).subscribe((dec) => {
      if (dec && dec.status == 200) {
        var data: any = this.encDecService.dwt(this.session, dec.data);
        this.serviceGroupData = data.getService
        this.serviceGroupLength = data.count;
      }
      this.searchSubmit = false;
      this.del = false;
    });
  }

  /**
   * For searching additional service records
   */
  public getServices() {
    this.searchSubmit = true;
    this.is_search = true;
    const params = {
      offset: 0,
      limit: this.pageSize,
      sortOrder: 'desc',
      sortByColumn: 'updated_at',
      name: this.name ? this.name : '',
      service_type: this.service_type ? this.service_type : '',
      company_id: this.companyId
    };
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: this.email
    }
    this.del = true;
    this._additional_Service.getaddService(enc_data).subscribe((dec) => {
      if (dec && dec.status == 200) {
        var data: any = this.encDecService.dwt(this.session, dec.data);
        this.serviceGroupData = data.getService
        this.serviceGroupLength = data.count;
      }
      this.searchSubmit = false;
      this.del = false;
    });
  }
}

