import { NgModule } from '@angular/core';
import { RouterModule, Routes , PreloadAllModules } from '@angular/router';
import {ListVehicleModelsComponent} from './list-vehicle-models/list-vehicle-models.component';
import {AddVehicleModelComponent} from './add-vehicle-model/add-vehicle-model.component';
import {EditVehicleModelComponent} from './edit-vehicle-model/edit-vehicle-model.component';
import { AclAuthervice } from '../../../common/services/access-control/acl-auth.service';

export const vehicleModelsRoutes: Routes = [
  { path: '', component: ListVehicleModelsComponent, canActivate: [AclAuthervice], data: {roles: ["Vehicle Models - List"]}},
  { path: 'add', component: AddVehicleModelComponent, canActivate: [AclAuthervice], data: {roles: ["Vehicle Models - Add"]}},
  { path: 'edit/:id', component: EditVehicleModelComponent, canActivate: [AclAuthervice], data: {roles: ["Vehicle Models - Edit"]}}
];
