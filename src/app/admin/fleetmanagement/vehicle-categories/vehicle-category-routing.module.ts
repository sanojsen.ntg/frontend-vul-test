import { NgModule } from '@angular/core';
import { RouterModule, Routes, PreloadAllModules } from '@angular/router';
import { ListVehicleCategoryComponent } from './list-vehicle-category/list-vehicle-category.component';
import { AddVehicleCategoryComponent } from './add-vehicle-category/add-vehicle-category.component';
import { EditVehicleCategoryComponent } from './edit-vehicle-category/edit-vehicle-category.component';
import { AclAuthervice } from '../../../common/services/access-control/acl-auth.service';

export const vehicleCategoryRoutes: Routes = [
  { path: '', component: ListVehicleCategoryComponent, canActivate: [AclAuthervice], data: { roles: ["Vehicle Models - List"] } },
  { path: 'add', component: AddVehicleCategoryComponent, canActivate: [AclAuthervice], data: { roles: ["Vehicle Models - Add"] } },
  { path: 'edit/:id', component: EditVehicleCategoryComponent, canActivate: [AclAuthervice], data: { roles: ["Vehicle Models - Edit"] } }
];
