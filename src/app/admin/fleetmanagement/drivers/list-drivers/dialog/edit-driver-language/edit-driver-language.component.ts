import { Component, OnInit, ViewChild, Inject, ViewContainerRef } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DriverService } from '../../../../../../common/services/driver/driver.service';
import { MatSelectModule } from '@angular/material/select';
import { ToastsManager } from 'ng2-toastr';
import { Router } from '@angular/router';
import { JwtService } from '../../../../../../common/services/api/jwt.service';
import { EncDecService } from '../../../../../../common/services/encrypt-decrypt-service/encrypt_decrypt_service';

@Component({
  selector: 'app-edit-driver-language',
  templateUrl: './edit-driver-language.component.html',
  styleUrls: ['./edit-driver-language.component.css'],
})

export class EditDriverLanguageComponent implements OnInit {

  public editForm: FormGroup;
  public selectedValue: any;
  public driverData;
  public driverLanguageData;
  public id;
  public _id;
  public DriverLanguageById;
  public Data;
  public driverDataDetail;
  public driverAddData1;
  public driverAddData;
  public companyId: any = [];
  email: string;
  session: string;

  constructor(
    public _driverService: DriverService,
    public dialogRef: MatDialogRef<EditDriverLanguageComponent>,
    private toastr: ToastsManager,
    private vcr: ViewContainerRef,
    private formBuilder: FormBuilder,
    private router: Router,
    public encDecService: EncDecService,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public jwtService: JwtService) {
    const company_id: any = data.company_id;
    this.session = localStorage.getItem('Sessiontoken');
    this.email = localStorage.getItem('user_email');
    company_id.forEach(element => {
      this.companyId.push(element);
    });
    this.driverAddData1 = data.id;
    this.editForm = formBuilder.group({
      driver_language_id: ['']
    });
  }

  ngOnInit() {
    this.driverAddData = this.driverAddData1;
    this.getDriverLanguage();
    const DriverData = {
      company_id: this.companyId,
      _id: this.driverAddData._id
    }
    var encrypted = this.encDecService.nwt(this.session, DriverData);
    var enc_data = {
      data: encrypted,
      email: this.email
    }
    this._driverService.getDriverLanguageById(enc_data).then((dec) => {
      if (dec && dec.status == 200) {
        var res: any = this.encDecService.dwt(this.session, dec.data);
        this.Data = res.DriverLanguageById.driver_language_id;
        this.editForm.controls['driver_language_id'].setValue(this.Data);
      }
    }
    );
  }

  updateeDriverLanguage() {
    if (!this.editForm.valid) {
      this.toastr.error('All fields are required.');
      return;
    }
    else {
      const DriverData = {
        driver_language_id: this.editForm.value.driver_language_id,
        company_id: this.companyId,
        _id: this.driverAddData._id
      }
      var encrypted = this.encDecService.nwt(this.session, DriverData);
      var enc_data = {
        data: encrypted,
        email: this.email
      }
      this._driverService.updateDriverLanguage(enc_data).then((dec) => {
        if (dec) {
          if (dec.status === 200) {
            this.toastr.success('Driver language Updated successfully.');
          } else if (dec.status === 201) {
            this.toastr.error('Driver language Updation failed.');
          }
        }
      }).catch((error) => {
        this.toastr.error('Driver language Updation failed.');
      });
      this.dialogRef.close();
    }
  }

  closePop() {
    this.dialogRef.close();
  }

  public deleteDriverLanguage(index, _id) {
    this.driverAddData.driver_language_id.splice(index, 1);
    var i = this.Data.indexOf(_id);
    this.Data.splice(i, 1);
    this.editForm.controls['driver_language_id'].setValue(this.Data);
  }

  getDriverLanguage() {
    const params = {
      offset: 0,
      limit: 0,
      sortOrder: 'desc',
      sortByColumn: 'updated_at',
    };
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: this.email
    }
    this._driverService.getDriverLanguage(enc_data).then((dec) => {
      if (dec && dec.status == 200) {
        var data: any = this.encDecService.dwt(this.session, dec.data);
        this.driverLanguageData = data.getDriverLanguages;
      }
    });
  }

}
