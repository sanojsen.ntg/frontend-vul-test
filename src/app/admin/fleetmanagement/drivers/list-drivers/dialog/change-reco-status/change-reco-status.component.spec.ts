import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChangeRecoStatusComponent } from './change-reco-status.component';

describe('ChangeRecoStatusComponent', () => {
  let component: ChangeRecoStatusComponent;
  let fixture: ComponentFixture<ChangeRecoStatusComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChangeRecoStatusComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChangeRecoStatusComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
