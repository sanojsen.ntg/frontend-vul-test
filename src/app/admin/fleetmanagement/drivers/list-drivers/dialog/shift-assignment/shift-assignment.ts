import { Component, Inject, OnInit, ViewChild, ViewContainerRef } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ShiftTimingsService } from '../../../../../../common/services/shift-timings/shift-timings.service';
import { DriverService } from '../../../../../../common/services/driver/driver.service';
import { ToastsManager } from 'ng2-toastr';
import { Router } from '@angular/router';
import { JwtService } from '../../../../../../common/services/api/jwt.service';
import { EncDecService } from '../../../../../../common/services/encrypt-decrypt-service/encrypt_decrypt_service';

@Component({
  selector: 'app-shift-assignment',
  templateUrl: 'shift-assignment.html',
  styleUrls: ['./shift-assignment.css']
})
export class ShiftAssignmentComponent {
  public driverData;
  public driver_id;
  public ShiftTimingsData;
  public SelectedShiftTimings: any = [];
  public companyId: any = [];
  public ShiftTimings: any = {
    shift_timings_id: ''
  };
  email: string;
  session: string;

  constructor(private _shiftTimingsService: ShiftTimingsService,
    public toastr: ToastsManager,
    private router: Router,
    private _driverService: DriverService,
    public dialogRef: MatDialogRef<ShiftAssignmentComponent>,
    public jwtService: JwtService,
    private vcr: ViewContainerRef,
    public encDecService: EncDecService,
    @Inject(MAT_DIALOG_DATA) public data: any) {
    this.toastr.setRootViewContainerRef(vcr);
    //const company_id: any = localStorage.getItem('user_company');
    const company_id: any = data.company_id;
    this.session = localStorage.getItem('Sessiontoken');
    this.email = localStorage.getItem('user_email');
    company_id.forEach(element => {
      this.companyId.push(element);
    });
    this.driverData = data.id;
  }

  ngOnInit() {
    this.driver_id = this.driverData._id;
    this.ShiftTimings.shift_timings_id = this.driverData.shift_timings_id ? this.driverData.shift_timings_id._id : '';
    this.SelectedShiftTimings.push(this.driverData.shift_timings_id);
    const params = {
      offset: 0,
      limit: 0,
      sortOrder: 'desc',
      sortByColumn: 'updated_at',
      company_id: this.companyId
    };
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: this.email
    }
    this._shiftTimingsService.getShiftTiming(enc_data).then((dec) => {
      if (dec && dec.status == 200) {
        var data: any = this.encDecService.dwt(this.session, dec.data);
        this.ShiftTimingsData = data.shift_timings;
      }
    });
  }

  /**
   * Save Shift Timings
   *
   * @param formData
   */
  onSubmit(formData): void {
    if (formData.valid) {
      this.ShiftTimings['company_id'] = this.companyId;
      this.ShiftTimings['_id'] = this.driver_id;
      var encrypted = this.encDecService.nwt(this.session, this.ShiftTimings);
      var enc_data = {
        data: encrypted,
        email: this.email
      }
      this._driverService.updateShiftTimings(enc_data)
        .then((dec) => {
          if (dec) {
            if (dec.status === 200) {
              var res: any = this.encDecService.dwt(this.session, dec.data);
              this.toastr.success('shift timings added successfully.');
              this.driverData.shift_timings_id._id = res.updatedDriverShift.shift_timings_id;
            } else if (dec.status === 201) {
              this.toastr.error('Add shift timings failed.');
            } else if (dec.status === 203) {
              this.toastr.error('Driver logged in, Shift timings cannot be updated.');
            }
            setTimeout(() => {
              this.dialogRef.close();
            }, 1000);
          }
        })
        .catch((error) => {
        });
    } else {
      this.toastr.error('Add shift timings failed.');
    }
  }

  closePop() {
    this.dialogRef.close();
  }

  setShiftTiming(id: any): void {
    this.SelectedShiftTimings = this.ShiftTimingsData.filter(value => value._id == id);
  }
}
