import { NgModule } from '@angular/core';
import { RouterModule, Routes, PreloadAllModules } from '@angular/router';
import { vehiclesRoutes } from './vehicles/vehicles-routing.module';
import { vehicleModelsRoutes } from './vehicle-models/vehicle-models-routing.module';
import { vehicleCategoryRoutes } from './vehicle-categories/vehicle-category-routing.module';
import { driversRoutes } from './drivers/drivers-routing.module';
import { vehicleCategoryMessageRoutes } from './vehicle-categories-messages/vehicle-category-messages-routing.module';
import { deviceRoutes } from './devices/devices-routing.module';
import { blockedDeviceRoutes } from './blocked-devices/blocked-devices-routing.module';
import { driverGroupsRoutes } from './driver-groups/driver-groups-routing.module';
import { additionalServiceRoutes } from './additional-service/additional-service-routing.module';
import { FleetComponent } from './fleet/fleet.component';
import { AclAuthervice } from '../../common/services/access-control/acl-auth.service';
import { DriverStatsComponent } from './driver-stats/driver-stats.component';
import { DriverDetailsComponent } from './driver-stats/driver-details/driver-details.component';

import { extDriverRoute } from './ext-driver/ext-driver-routing.module'
import { ListDriverStatsComponent } from './driver-stats/list-driver-stats/list-driver-stats.component';
import { ListDriverAvailabilityComponent } from './driver-stats/list-driver-availability/list-driver-availability.component';
import { ListDriverAcceptanceComponent } from './driver-stats/list-driver-acceptance/list-driver-acceptance.component';
export const fleetManagementRoutes: Routes = [
    { path: '', component: FleetComponent, canActivate: [AclAuthervice], data: { roles: ["Fleet Management"] } },
    { path: 'vehicles', children: vehiclesRoutes, canActivate: [AclAuthervice], data: { roles: ["Vehicles - List"] } },
    { path: 'devices', children: deviceRoutes, canActivate: [AclAuthervice], data: { roles: ["Devices - List"] } },
    { path: 'blocked-devices', children: blockedDeviceRoutes, canActivate: [AclAuthervice], data: { roles: ['blocked-devices'] } },
    { path: 'vehicle-model', children: vehicleModelsRoutes, canActivate: [AclAuthervice], data: { roles: ["Vehicle Models - List"] } },
    { path: 'vehicle-category', children: vehicleCategoryRoutes, canActivate: [AclAuthervice], data: { roles: ["Vehicle Models - List"] } },
    { path: 'vehicle-category-messages', children: vehicleCategoryMessageRoutes, canActivate: [AclAuthervice], data: { roles: ["Vehicle Models - List"] } },
    { path: 'drivers', children: driversRoutes, canActivate: [AclAuthervice], data: { roles: ["Drivers - List"] } },
    { path: 'driver-stats', component: DriverStatsComponent, canActivate: [AclAuthervice], data: { roles: ["Drivers - Stats"] } },
    { path: 'driver-details/:driver_id', component: DriverDetailsComponent, canActivate: [AclAuthervice], data: { roles: ["Drivers - Stats"] } },
    { path: 'driver-groups', children: driverGroupsRoutes, canActivate: [AclAuthervice], data: { roles: ["Driver Groups - List"] } },
    { path: 'servicegroup', children: additionalServiceRoutes, canActivate: [AclAuthervice], data: { roles: ["Additional Services - List"] } },
    { path: 'ext-driver', children: extDriverRoute, canActivate: [AclAuthervice], data: { roles: ["External Drivers - List"] } },
    { path: 'list-driver-stats', component: ListDriverStatsComponent, canActivate: [AclAuthervice], data: { roles: ["Drivers - Stats"] } },
    { path: 'driver-availability', component: ListDriverAvailabilityComponent, canActivate: [AclAuthervice], data: { roles: ["Drivers - Stats"] } },
    { path: 'driver-acceptance', component: ListDriverAcceptanceComponent, canActivate: [AclAuthervice], data: { roles: ["Drivers - Stats"] } }
];
