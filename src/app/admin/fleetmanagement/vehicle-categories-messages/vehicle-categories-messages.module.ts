import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AddVehicleCategoryMessageComponent } from './add-vehicle-category-messages/add-vehicle-category-messages.component';
import { EditVehicleCategoryMessageComponent } from './edit-vehicle-category-messages/edit-vehicle-category.component-messages';
import { ListVehicleCategoryMessageComponent } from './list-vehicle-category-messages/list-vehicle-category-messages.component';
import { RouterModule, Routes, PreloadAllModules } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { VehicleModelsService } from '../../../common/services/vehiclemodels/vehiclemodels.service';
import { VehicleModelsRestService } from '../../../common/services/vehiclemodels/vehiclemodelsrest.service';
import { ImageUploadModule } from 'angular2-image-upload';
import { MatSelectModule } from '@angular/material/select';
import { MatTooltipModule } from '@angular/material/tooltip';
@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    NgbModule,
    ReactiveFormsModule,
    MatSelectModule,
    ImageUploadModule.forRoot(),
    MatTooltipModule
  ],
  declarations: [AddVehicleCategoryMessageComponent, EditVehicleCategoryMessageComponent, ListVehicleCategoryMessageComponent],
  exports: [
    RouterModule
  ],
  providers: [VehicleModelsService, VehicleModelsRestService]
})
export class VehicleCategoriesMessageModule { }
