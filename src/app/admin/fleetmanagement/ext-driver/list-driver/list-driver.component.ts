import { Component, OnInit } from '@angular/core';
import { EncDecService } from '../../../../common/services/encrypt-decrypt-service/encrypt_decrypt_service';
import { ExtDriverService } from '../../../../common/services/ext-driver/ext-driver.service';
import { Router } from '@angular/router';
import { ApproveDialogComponent } from '../approve-dialog/approve-dialog.component';
import { MatDialog } from '@angular/material';
import { Overlay } from '@angular/cdk/overlay';
import { ToastsManager } from 'ng2-toastr';
import { AccessControlService } from '../../../../common/services/access-control/access-control.service';
import { CompanyService } from '../../../../common/services/companies/companies.service';
import { GlobalService } from "../../../../common/services/global/global.service";
import * as moment from "moment/moment";
import { FormControl } from '@angular/forms';
import { DriverService } from '../../../../common/services/driver/driver.service';
import { VehicleService } from '../../../../common/services/vehicles/vehicle.service';
import { Angular2Csv } from 'angular2-csv/Angular2-csv';
import { element } from 'protractor';

@Component({
  selector: 'app-list-driver',
  templateUrl: './list-driver.component.html',
  styleUrls: ['./list-driver.component.css']
})
export class ListDriverComponent implements OnInit {
  public companyId: any = [];
  public session;
  public email;
  public driverData;
  public pageSize = 10;
  public pageNo = 0;
  public listCount;
  public onBoardStatus = ['0', '1', '2', '3', '4'];
  public searchLoader = false;
  submit: boolean;
  public filter_text;
  pNo = 1;
  createDriver: boolean;
  createVehicle: boolean;
  filter = ['1', '2'];
  dtc: boolean;
  companyData: any;
  sort_by = 'created_at';
  unique_driver_id: any = '';
  companyIdFilter: any = [];
  sortOrder = 'asc';
  driverField: FormControl = new FormControl();
  vehicleField: FormControl = new FormControl();
  DriverList = []
  company_id: any;
  vehiclesListing: any;
  companyDataTemp: any=[];
  constructor(private _vehicleService: VehicleService, private _driverService: DriverService, public toastr: ToastsManager, public overlay: Overlay, public encDecService: EncDecService, public extDriverService: ExtDriverService, public router: Router, public dialog: MatDialog, public _aclService: AccessControlService, public _companyservice: CompanyService, private globalService: GlobalService) {
    this.company_id = localStorage.getItem('user_company');
    this.session = localStorage.getItem('Sessiontoken');
    this.email = localStorage.getItem('user_email');
    this.companyId.push(this.company_id);
    this.companyId.push(null);
    if (this.company_id === '5ce12918aca1bb08d73ca25d' || this.company_id === '5cd982667a64fe51e5d3f7a0') {
      this.dtc = true;
    }
  }

  ngOnInit() {
    if (this.globalService.externalDriversList) {
      this.companyId = this.globalService.externalDriversList.company_id;
      this.companyIdFilter = this.globalService.externalDriversList.company_id_filter;
      this.onBoardStatus = this.globalService.externalDriversList.onboarding_status;
      this.sort_by = this.globalService.externalDriversList.sortByColumn;
      this.filter_text = this.globalService.externalDriversList.search;
      this.filter = this.globalService.externalDriversList.filter;
      this.unique_driver_id = this.globalService.externalDriversList.unique_id;
      this.sortOrder = this.globalService.externalDriversList.sortOrder;
      this.driverField.setValue(this.globalService.externalDriversList.driver_id);
      this.vehicleField.setValue(this.globalService.externalDriversList.vehicle_id)
      if (!this.dtc) {
        this.getDriverList();
        this.aclDisplayService();
      }
    }
    else {
      if (!this.dtc) {
        this.getDriverList();
        this.aclDisplayService();
      }
    }
    this.getCompanies();
    this.driverField.valueChanges
      .debounceTime(500)
      .distinctUntilChanged()
      .switchMap((query) => {
        let company_id = this.companyId.slice();
        var foundIndex = company_id.indexOf('null');
        if (foundIndex > -1) {
          company_id.splice(foundIndex, 1);
        }
        var params = {
          limit: 10,
          'search_keyword': query,
          company_id: this.dtc && this.companyDataTemp.length > 0 ? this.companyDataTemp : this.company_id
        }
        var encrypted = this.encDecService.nwt(this.session, params);
        var enc_data = {
          data: encrypted,
          email: this.email
        }
        return this._driverService.searchForDriver(enc_data)
      })
      .subscribe(dec => {
        if (dec && dec.status === 200) {
          var result: any = this.encDecService.dwt(this.session, dec.data);
          this.DriverList = result.driver;
        }
      });
    this.vehicleField.valueChanges
      .debounceTime(500)
      .distinctUntilChanged()
      .switchMap((query) => {
        let company_id = this.companyId.slice();
        var foundIndex = company_id.indexOf('null');
        if (foundIndex > -1) {
          company_id.splice(foundIndex, 1);
        }
        var params = {
          offset: 0,
          limit: 10,
          sortOrder: 'desc',
          sortByColumn: 'vehicle_unique_id',
          'search_keyword': query,
          company_id: this.dtc && this.companyDataTemp.length > 0 ? this.companyDataTemp : this.company_id
        }
        var encrypted = this.encDecService.nwt(this.session, params);
        var enc_data = {
          data: encrypted,
          email: this.email
        }
        return this._vehicleService.searchVehicle(enc_data)
      })
      .subscribe(dec => {
        if (dec && dec.status === 200) {
          var result: any = this.encDecService.dwt(this.session, dec.data);
          this.vehiclesListing = result.searchVehicle;
        }
      });
  }

  public getCompanies() {
    const param = {
      offset: 0,
      //limit: this.itemsPerPage,
      sortOrder: 'desc',
      sortByColumn: 'updated_at',
      company_id: this.companyId
    };
    var encrypted = this.encDecService.nwt(this.session, param);
    var enc_data = {
      data: encrypted,
      email: this.email
    }
    this._companyservice.getCompanyListing(enc_data).then((dec) => {
      if (dec) {
        if (dec.status == 200) {
          var data: any = this.encDecService.dwt(this.session, dec.data);
          this.companyData = data.getCompanies;
          this.companyDataTemp = data.getCompanies.map(x=>x._id);
          this.companyData.push({ _id: 'null', company_name: 'Company Not Assigned' })
          if (this.dtc) {
            //this.companyId = data.getCompanies.map(x => x._id);
            //this.companyIdFilter = data.getCompanies.map(x => x._id)
            //if(!this.globalService.externalDriversList)
            this.companyIdFilter.push('null')
            //this.companyId = this.companyData.map(x => x._id);
            //this.companyId.push(null);
            this.getDriverList();
            this.aclDisplayService();
          }
        }
      }
    });
  }
  public aclDisplayService() {
    var params = {
      company_id: this.companyId
    }
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: this.email
    }
    this._aclService.getAclUserMenu(enc_data).then((dec) => {
      if (dec) {
        if (dec.status == 200) {
          var data: any = this.encDecService.dwt(this.session, dec.data);
          for (let i = 0; i < data.menu.length; i++) {
            if (data.menu[i] == "External Drivers - Create driver") {
              this.createDriver = true;
            } else if (data.menu[i] == "External Drivers - Create vehicle") {
              this.createVehicle = true;
            }
          };
        }
      }
    })
  }
  public getResetList() {
    this.onBoardStatus = ['0', '1', '2', '3', '4'];
    this.filter_text = '';
    this.filter = [];
    this.unique_driver_id = '';
    this.sort_by = '';
    this.companyId = [null];
    this.companyIdFilter = [null];
    this.driverField.setValue('');
    this.vehicleField.setValue('');
    this.getDriverList();
  }
  public getDriverList() {
    this.pNo = 1;
    this.searchLoader = true;
    var foundIndex = this.companyId.indexOf('null');
    if (foundIndex > -1)
      this.companyId[foundIndex] = null;
    let params2 = {
      company_id: this.companyId,
      company_id_filter: this.companyIdFilter,
      onboarding_status: this.onBoardStatus,
      offset: 0,
      limit: 10,
      sortOrder: this.sortOrder ? this.sortOrder : 'desc',
      sortByColumn: this.sort_by ? this.sort_by : 'updated_at',
      search: this.filter_text,
      filter: this.filter,
      unique_id: this.unique_driver_id,
      driver_id: this.driverField.value,
      vehicle_id: this.vehicleField.value
    }
    var params = {
      company_id: this.companyId,
      onboarding_status: this.onBoardStatus,
      offset: 0,
      limit: 10,
      sortOrder: this.sortOrder ? this.sortOrder : 'desc',
      sortByColumn: this.sort_by ? this.sort_by : 'updated_at',
      search: this.filter_text,
      filter: this.filter,
      unique_id: this.unique_driver_id,
      driver_id: this.driverField.value !== null && typeof this.driverField.value == 'object' ? this.driverField.value._id : '',
      vehicle_id: this.vehicleField.value !== null && typeof this.vehicleField.value == 'object' ? this.vehicleField.value._id : ''
    }
    this.globalService.externalDriversList = params2;
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: this.email
    }
    this.extDriverService.extDriverList(enc_data).then((dec) => {
      this.searchLoader = false;
      if (dec && dec.status == 200) {
        var data: any = this.encDecService.dwt(this.session, dec.data);
        this.driverData = data.data;
        this.listCount = data.count;
      }
      else {
        this.driverData = [];
        this.listCount = 0;
      }
    })
  }

  pagingAgent(data) {
    this.pNo = data;
    this.searchLoader = true;
    this.pageNo = (data * this.pageSize) - this.pageSize;
    var foundIndex = this.companyId.indexOf('null');
    if (foundIndex > -1)
      this.companyId[foundIndex] = null;
    const params = {
      offset: this.pageNo,
      limit: this.pageSize,
      sortOrder: this.sortOrder ? this.sortOrder : 'desc',
      sortByColumn: this.sort_by ? this.sort_by : 'updated_at',
      search: this.filter_text,
      filter: this.filter,
      unique_id: this.unique_driver_id,
      driver_id: this.driverField.value !== null && typeof this.driverField.value == 'object' ? this.driverField.value._id : '',
      vehicle_id: this.vehicleField.value !== null && typeof this.vehicleField.value == 'object' ? this.vehicleField.value._id : '',
      company_id: this.companyId,
      onboarding_status: this.onBoardStatus
    };
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: this.email
    }
    this.extDriverService.extDriverList(enc_data).then((dec) => {
      this.searchLoader = false;
      if (dec && dec.status == 200) {
        var data: any = this.encDecService.dwt(this.session, dec.data);
        this.driverData = data.data;
        this.listCount = data.count;
      }
    });
  }
  openDialog(driver, id, index): void {
    this.submit = true;
    let dialogRef = this.dialog.open(ApproveDialogComponent, {
      width: '450px',
      scrollStrategy: this.overlay.scrollStrategies.noop(),
      data: { text: 'Change the training status to completed ', id: id }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.submit = true;
        if (result.status == 1) {
          let params: any = {
            company_id: this.companyId,
            on_boarding_driver_id: driver._id,
            auth_key: driver.auth_key,
            training_completed: '1',
          }
          var encrypted = this.encDecService.nwt(this.session, params);
          var enc_data = {
            data: encrypted,
            email: this.email
          }
          this.extDriverService.extDriverUpdate(enc_data).then((dec) => {
            this.submit = false;
            if (dec && dec.status == 200) {
              this.toastr.success('Training status updated successfully');
              this.getDriverList();
            }
            else {
              this.toastr.error(dec ? dec.message : 'Some error occurred please try later.');
              this.driverData[index].invalid2 = dec ? dec.message : 'Some error occurred please try later.';
            }
          });
        }
        else if (result.status == 2) {
          let params = {
            company_id: result.company,
            on_boarding_driver_id: driver._id,
            auth_key: driver.auth_key,
            is_company_driver: result.driver_type
          }
          var encrypted = this.encDecService.nwt(this.session, params);
          var enc_data = {
            data: encrypted,
            email: this.email
          }
          this.extDriverService.createDriver(enc_data).then((dec) => {
            this.submit = false;
            if (dec && dec.status == 200) {
              this.toastr.success('Driver created successfully');
              var data: any = this.encDecService.dwt(this.session, dec.data);
              this.driverData[index].driver_id = data.data._id;
              this.driverData[index].success = 'Driver created successfully! Driver ID = ' + data.data.username;
              //this.getDriverList();
            }
            else {
              this.toastr.error(dec ? dec.message : 'Some error occurred please try later.');
              this.driverData[index].invalid = dec ? dec.message : 'Some error occurred please try later.';
            }
          });
        }
        else if (result.status == 3) {
          let params = {
            company_id: result.company,
            on_boarding_driver_id: driver._id,
            auth_key: driver.auth_key,
          }
          var encrypted = this.encDecService.nwt(this.session, params);
          var enc_data = {
            data: encrypted,
            email: this.email
          }
          this.extDriverService.createVehicle(enc_data).then((dec) => {
            this.submit = false;
            if (dec && dec.status == 200) {
              this.toastr.success('Vehicle successfully');
              this.getDriverList();
            }
            else {
              this.toastr.error(dec ? dec.message : 'Some error occurred');
              this.driverData[index].invalid = dec ? dec.message : 'Some error occurred please try later.';
            }
          });
        } else if (result.status == 4) {
          let params: any = {
            company_id: this.companyId,
            on_boarding_driver_id: driver._id,
            auth_key: driver.auth_key,
            onboarding_status: '3',
          }
          var encrypted = this.encDecService.nwt(this.session, params);
          var enc_data = {
            data: encrypted,
            email: this.email
          }
          this.extDriverService.extDriverUpdate(enc_data).then((dec) => {
            this.submit = false;
            if (dec && dec.status == 200) {
              let params = {
                company_id: result.company,
                on_boarding_driver_id: driver._id,
                auth_key: driver.auth_key,
              }
              var encrypted = this.encDecService.nwt(this.session, params);
              var enc_data = {
                data: encrypted,
                email: this.email
              }
              this.extDriverService.createVehicle(enc_data).then((dec) => {
                if (dec && dec.status == 200) {
                  this.toastr.success('Vehicle created successfully');
                  this.getDriverList();
                }
                else {
                  this.toastr.error(dec ? dec.message : 'Some error occurred');
                  this.driverData[index].invalid = dec ? dec.message : 'Some error occurred please try later.';
                }
              });
            }
            else {
              this.toastr.error(dec ? dec.message : 'Some error occurred please try later.');
              this.driverData[index].invalid2 = dec ? dec.message : 'Some error occurred please try later.';
            }
          });
        } else {
          this.submit = false;
        }
      } else {
        this.submit = false;
      }
    });
  }
  disableStatusSelect(id) {
    switch (id) {
      case 1:
        return ['4', '7'].some(r => this.filter.includes(r))
      case 2:
        return ['5', '7'].some(r => this.filter.includes(r))
      case 3:
        return this.filter.includes('6')
      case 4:
        return this.filter.includes('1')
      case 5:
        return this.filter.includes('2')
      case 6:
        return this.filter.includes('3')
      case 7:
        return ['1', '2'].some(r => this.filter.includes(r))
      case 8:
        return ['4', '5', '7'].some(r => this.filter.includes(r))
    }
  }
  selectAllCompany() {
    if (this.companyId.length !== this.companyData.length) {
      this.companyIdFilter = this.companyData.slice().map(x => x._id);
      this.companyId = this.companyIdFilter.slice()
      this.companyIdFilter.push('all')
    }
    else {
      this.companyId = [];
      this.companyIdFilter = [];
    }
  }
  companyManualSelect() {
    let tempArray = this.companyIdFilter.slice()
    this.companyIdFilter = [];
    let index = tempArray.indexOf('all');
    if (index > -1) {
      tempArray.splice(index, 1);
      this.companyIdFilter = tempArray;
      this.companyId = this.companyIdFilter.slice()
    }
    else {
      this.companyId = tempArray.slice();
      this.companyIdFilter = tempArray;
    }
  }
  displayFnDriver(data) {
    return data ? data.username : '';
  }
  displayFnVehicles(data) {
    return data ? data.vehicle_identity_number : '';
  }
  findCompany(even){
    var key = this.companyData.find(element=>(element._id == even)
    )
    return key.company_name;
  }
  public createCsv() {
    var params = {
      company_id: this.companyId,
      onboarding_status: this.onBoardStatus,
      sortOrder: this.sortOrder ? this.sortOrder : 'desc',
      sortByColumn: this.sort_by ? this.sort_by : 'updated_at',
      search: this.filter_text,
      filter: this.filter,
      offset:'0',
      limit: this.listCount,
      unique_id: this.unique_driver_id,
      driver_id: this.driverField.value !== null && typeof this.driverField.value == 'object' ? this.driverField.value._id : '',
      vehicle_id: this.vehicleField.value !== null && typeof this.vehicleField.value == 'object' ? this.vehicleField.value._id : ''
    }
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: this.email
    }
    this.extDriverService.extDriverList(enc_data).then((dec) => {
      if (dec) {
        if (dec.status == 200) {
          var res: any = this.encDecService.dwt(this.session, dec.data);
          let labels = [
            'Ref ID',
            'Name',
            'First Name',
            'Last Name',
            'Phone number',
            'Email',
            'Training status',
            'Company ID'
          ];
          let resArray = [];
          for (let i = 0; i < res.data.length; i++) {
            const csvArray = res.data[i];
            resArray.push
              ({
                _id: csvArray.unique_id ? csvArray.unique_id : 'N/A',
                name: csvArray.name ? csvArray.name : 'N/A',
                first_name: csvArray.first_name ? csvArray.first_name : 'NA',
                last_name: csvArray.last_name ? csvArray.last_name : 'NA',
                phone_number: csvArray.phone_number ? csvArray.phone_number : 'NA',
                email: csvArray.email ? csvArray.email : 'NA',
                training: csvArray.training_completed == '0' ? 'Not Completed' : 'Completed',
                company_id : csvArray.company_id ? (this.findCompany(csvArray.company_id)) :'NA'
              });
          }
          var options =
          {
            fieldSeparator: ',',
            quoteStrings: '"',
            decimalseparator: '.',
            showLabels: true,
            showTitle: false,
            useBom: true,
            headers: (labels)
          };
          new Angular2Csv(resArray, 'Device-List' + moment().format('YYYY-MM-DD_HH_mm_ss'), options);
        }
      }
    })
  }
}
