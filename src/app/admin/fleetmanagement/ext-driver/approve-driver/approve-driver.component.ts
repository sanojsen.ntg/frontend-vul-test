import { Component, OnInit } from '@angular/core';
import { environment } from '../../../../../environments/environment';
import { MatDialog } from '@angular/material';
import { ImageDialogComponent } from '../image-dialog/image-dialog.component';
import { EncDecService } from '../../../../common/services/encrypt-decrypt-service/encrypt_decrypt_service';
import { ExtDriverService } from '../../../../common/services/ext-driver/ext-driver.service';
import { ActivatedRoute, Router } from "@angular/router";
import { FormBuilder, Validators, FormGroup, FormControl } from "@angular/forms";
import { Overlay } from '@angular/cdk/overlay';
import { ApproveDialogComponent } from '../approve-dialog/approve-dialog.component';
import { ChangePasswordDialogComponent } from '../change-password-dialog/change-password-dialog.component';
import { ToastsManager } from 'ng2-toastr';
import { VehicleModelsService } from '../../../../common/services/vehiclemodels/vehiclemodels.service';

@Component({
  selector: 'app-approve-driver',
  templateUrl: './approve-driver.component.html',
  styleUrls: ['./approve-driver.component.css']
})
export class ApproveDriverComponent implements OnInit {
  public companyId: any = [];
  public session;
  public email;
  public cUser;
  public token;
  public extDriver: any = [];
  public profileImg;
  public licenceFrontImg;
  public licenceBackImg;
  public insuranceFrontImg;
  public insuranceBackImg
  public rcFrontImg;
  public rcBackImg;
  public customerId;
  public emiratesFrontImg;
  public emiratesBackImg;
  public rtaCardBack
  public rtaCardFront;
  public form: FormGroup;
  public licence = '0';
  public registration = '0';
  public emirates = '0';
  public submit = false;
  public profile;
  isLoading: boolean=false;
  invalid: string;
  invalid2: string;
  vehicleModelData: any;
  rtaCard: any;
  country_list = ["Afghanistan", "Albania", "Algeria", "Andorra", "Angola", "Anguilla", "Antigua &amp; Barbuda", "Argentina", "Armenia", "Aruba", "Australia", "Austria", "Azerbaijan", "Bahamas"
    , "Bahrain", "Bangladesh", "Barbados", "Belarus", "Belgium", "Belize", "Benin", "Bermuda", "Bhutan", "Bolivia", "Bosnia &amp; Herzegovina", "Botswana", "Brazil", "British Virgin Islands"
    , "Brunei", "Bulgaria", "Burkina Faso", "Burundi", "Cambodia", "Cameroon", "Canada", "Cape Verde", "Cayman Islands", "Chad", "Chile", "China", "Colombia", "Congo", "Cook Islands", "Costa Rica"
    , "Cote D Ivoire", "Croatia", "Cruise Ship", "Cuba", "Cyprus", "Czech Republic", "Denmark", "Djibouti", "Dominica", "Dominican Republic", "Ecuador", "Egypt", "El Salvador", "Equatorial Guinea"
    , "Estonia", "Ethiopia", "Falkland Islands", "Faroe Islands", "Fiji", "Finland", "France", "French Polynesia", "French West Indies", "Gabon", "Gambia", "Georgia", "Germany", "Ghana"
    , "Gibraltar", "Greece", "Greenland", "Grenada", "Guam", "Guatemala", "Guernsey", "Guinea", "Guinea Bissau", "Guyana", "Haiti", "Honduras", "Hong Kong", "Hungary", "Iceland", "India"
    , "Indonesia", "Iran", "Iraq", "Ireland", "Isle of Man", "Israel", "Italy", "Jamaica", "Japan", "Jersey", "Jordan", "Kazakhstan", "Kenya", "Kuwait", "Kyrgyz Republic", "Laos", "Latvia"
    , "Lebanon", "Lesotho", "Liberia", "Libya", "Liechtenstein", "Lithuania", "Luxembourg", "Macau", "Macedonia", "Madagascar", "Malawi", "Malaysia", "Maldives", "Mali", "Malta", "Mauritania"
    , "Mauritius", "Mexico", "Moldova", "Monaco", "Mongolia", "Montenegro", "Montserrat", "Morocco", "Mozambique", "Namibia", "Nepal", "Netherlands", "Netherlands Antilles", "New Caledonia"
    , "New Zealand", "Nicaragua", "Niger", "Nigeria", "Norway", "Oman", "Pakistan", "Palestine", "Panama", "Papua New Guinea", "Paraguay", "Peru", "Philippines", "Poland", "Portugal"
    , "Puerto Rico", "Qatar", "Reunion", "Romania", "Russia", "Rwanda", "Saint Pierre &amp; Miquelon", "Samoa", "San Marino", "Satellite", "Saudi Arabia", "Senegal", "Serbia", "Seychelles"
    , "Sierra Leone", "Singapore", "Slovakia", "Slovenia", "South Africa", "South Korea", "Spain", "Sri Lanka", "St Kitts &amp; Nevis", "St Lucia", "St Vincent", "St. Lucia", "Sudan"
    , "Suriname", "Swaziland", "Sweden", "Switzerland", "Syria", "Taiwan", "Tajikistan", "Tanzania", "Thailand", "Timor L'Este", "Togo", "Tonga", "Trinidad &amp; Tobago", "Tunisia"
    , "Turkey", "Turkmenistan", "Turks &amp; Caicos", "Uganda", "Ukraine", "United Arab Emirates", "United Kingdom", "United States", "United States Minor Outlying Islands", "Uruguay"
    , "Uzbekistan", "Venezuela", "Vietnam", "Virgin Islands (US)", "Yemen", "Zambia", "Zimbabwe"];
  min: Date;
  max: Date;
  bankList: any=[];
  invalid3: any;
  bankCtrl = new FormControl()
  tempBank: any=[];
  constructor(private _vehicleModelsService: VehicleModelsService, public overlay: Overlay, private fb: FormBuilder, public dialog: MatDialog, public encDecService: EncDecService, public extDriverService: ExtDriverService, private route: ActivatedRoute, public router: Router, public toastr: ToastsManager) {
    const company_id: any = localStorage.getItem('user_company');
    this.session = localStorage.getItem('Sessiontoken');
    this.email = localStorage.getItem('user_email');
    this.cUser = localStorage.getItem('CUser');
    this.token = localStorage.getItem('jwtToken');
    this.companyId.push(company_id);
    this.customerId = this.route.snapshot.params.id;
    this.form = fb.group({
      lice: ['0'],
    });
  }

  ngOnInit() {
    this.min = new Date(Date.now())
    this.max = new Date(Date.now())
    this.getDriverDets();
    this.getVehicleModels();
    this.getBankList();
    this.bankCtrl.valueChanges.subscribe(data => {
      this.invalid3='';
      this.tempBank = data ? this.bankList.filter(s => (s && new RegExp(`${data}`, 'gi').test(s.entity_name)))
        : this.bankList;
    });
  }
  getDriverDets() {
    var params = {
      on_boarding_driver_id: this.customerId,
      company_id: this.companyId,
    }
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: this.email
    }
    this.extDriverService.extDriverById(enc_data).then((dec) => {
      if (dec) {
        if (dec.status == 200) {
          var data: any = this.encDecService.dwt(this.session, dec.data);
          this.extDriver = data.data;
          this.licence = this.extDriver.driver_license_approved;
          this.registration = this.extDriver.car_registration_approved;
          this.emirates = this.extDriver.emirates_id_approved;
          this.rtaCard = this.extDriver.rta_card_approved;
          this.profile = this.extDriver.profile_picture_approved;

          this.profileImg = this.extDriver.profile_picture ? (environment.imageApiUrl + '/' + this.extDriver.profile_picture + '/' + this.cUser + '/' + this.token + '/OBD/profile_picture') : "assets/img/noimg.jpg";

          this.licenceFrontImg = this.extDriver.driver_license_front ? (environment.imageApiUrl + '/' + this.extDriver.driver_license_front + '/' + this.cUser + '/' + this.token + '/OBD/driver_license_front') : "assets/img/noimg.jpg";
          this.licenceBackImg = this.extDriver.driver_license_back ? (environment.imageApiUrl + '/' + this.extDriver.driver_license_back + '/' + this.cUser + '/' + this.token + '/OBD/driver_license_back') : "assets/img/noimg.jpg";

          this.insuranceFrontImg = this.extDriver.insurance_details_front ? environment.imageApiUrl + '/' + this.extDriver.insurance_details_front + '/' + this.cUser + '/' + this.token + '/OBD/insurance_details_front' : "assets/img/noimg.jpg";
          this.insuranceBackImg = this.extDriver.insurance_details_back ? environment.imageApiUrl + '/' + this.extDriver.insurance_details_back + '/' + this.cUser + '/' + this.token + '/OBD/insurance_details_back' : "assets/img/noimg.jpg";

          this.rcFrontImg = this.extDriver.car_registration_front ? environment.imageApiUrl + '/' + this.extDriver.car_registration_front + '/' + this.cUser + '/' + this.token + '/OBD/car_registration_front' : "assets/img/noimg.jpg";
          this.rcBackImg = this.extDriver.car_registration_back ? environment.imageApiUrl + '/' + this.extDriver.car_registration_back + '/' + this.cUser + '/' + this.token + '/OBD/car_registration_back' : "assets/img/noimg.jpg";

          this.emiratesFrontImg = this.extDriver.emirates_id_front ? environment.imageApiUrl + '/' + this.extDriver.emirates_id_front + '/' + this.cUser + '/' + this.token + '/OBD/emirates_id_front' : "assets/img/noimg.jpg";
          this.emiratesBackImg = this.extDriver.emirates_id_back ? environment.imageApiUrl + '/' + this.extDriver.emirates_id_back + '/' + this.cUser + '/' + this.token + '/OBD/emirates_id_back' : "assets/img/noimg.jpg";

          this.rtaCardFront = this.extDriver.rta_card_front ? environment.imageApiUrl + '/' + this.extDriver.rta_card_front + '/' + this.cUser + '/' + this.token + '/OBD/rta_card_front' : "assets/img/noimg.jpg";
          this.rtaCardBack = this.extDriver.rta_card_back ? environment.imageApiUrl + '/' + this.extDriver.rta_card_back + '/' + this.cUser + '/' + this.token + '/OBD/rta_card_back' : "assets/img/noimg.jpg";
        }
      }
    })
  }
  openImg(event, type) {
    let dialogRef = this.dialog.open(ImageDialogComponent, {
      hasBackdrop: true,
      width:"1000px",
      data: { type: type, event: event, driver_data: this.extDriver }
    });
    dialogRef.afterClosed().subscribe(result => {
    this.getDriverDets();
    });
  }
  openDialog(event): void {
    this.submit = true;
    if (event == 'approve' || event == 'approve_again') {
      var txt = 'Are you sure you want to approve this driver '
    } else if (event == 'pending') {
      var txt = 'Are you sure you want to move this record to pending '
    } else if (event == 'reject') {
      var txt = 'Are you sure you want to reject this record '
    } else if (event == 'training') {
      var txt = 'Change the training status to completed '
    } else if(event == 'reject_and_upload'){
      var txt = 'Are you sure you want to reject this record'
    }
    let dialogRef = this.dialog.open(ApproveDialogComponent, {
      width: '450px',
      scrollStrategy: this.overlay.scrollStrategies.noop(),
      data: { text: txt }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.driverApproval(event);
      } else {
        this.submit = false;
      }
    });
  }

  changePassword() {
    this.dialog.open(ChangePasswordDialogComponent, {
      hasBackdrop: true,
      data: this.extDriver
    });
  }

  driverApproval(event) {
    let params: any;
    if (event == 'approve') {
      params = {
        company_id: this.companyId,
        on_boarding_driver_id: this.extDriver._id,
        auth_key: this.extDriver.auth_key,
        onboarding_status: '3',
        profile_picture_approved: this.profile,
        driver_license_approved: this.licence,
        insurance_details_approved: '0',
        car_registration_approved: this.registration,
        rta_card_approved: this.rtaCard,
        emirates_id_approved: this.emirates
      }
    } else if (event == 'pending') {
      params = {
        company_id: this.companyId,
        on_boarding_driver_id: this.extDriver._id,
        auth_key: this.extDriver.auth_key,
        onboarding_status: '0',
        profile_picture_approved: this.profile,
        driver_license_approved: this.licence,
        insurance_details_approved: '0',
        car_registration_approved: this.registration,
        rta_card_approved: this.rtaCard,
        emirates_id_approved: this.emirates
      }
    } else if (event == 'reject') {
      params = {
        company_id: this.companyId,
        on_boarding_driver_id: this.extDriver._id,
        auth_key: this.extDriver.auth_key,
        onboarding_status: '2',
        profile_picture_approved: this.profile,
        driver_license_approved: this.licence,
        insurance_details_approved: '0',
        car_registration_approved: this.registration,
        rta_card_approved: this.rtaCard,
        emirates_id_approved: this.emirates
      }
    }
    else if (event == 'training') {
      params = {
        company_id: this.companyId,
        on_boarding_driver_id: this.extDriver._id,
        auth_key: this.extDriver.auth_key,
        training_completed: '1',
      }
    }
    else if (event == 'reject_and_upload') {
      params = {
        company_id: this.companyId,
        on_boarding_driver_id: this.extDriver._id,
        auth_key: this.extDriver.auth_key,
        onboarding_status: '2',
        car_registration_approved: this.registration,
        profile_picture_approved: this.profile,
        driver_license_approved: this.licence,
        insurance_details_approved: '0',
        rta_card_approved: this.rtaCard,
        emirates_id_approved: this.emirates
        // traffic_plate_number: '',
        // vehicle_tc_number: '',
        // vehicle_expiry_date: null,
        // vehicle_insurance_expiry_date: null,
        // vehicle_policy_number: null,
        // vehicle_model: null,
        // vehicle_model_id: null,
        // vehicle_owner: '',
        // vehicle_registration_date: null,
        // vehicle_origin: '',
        // vehicle_type: '',
        // vehicle_gvw: '',
        // vehicle_weight: '',
        // vehicle_engine_number: '',
        // vehicle_chasis_number: '',
        // vehicle_licensing_authority: '',
        // vehicle_identity_number: ''
      }
    }else if (event == 'approve_again') {
        params = {
          company_id: this.companyId,
          on_boarding_driver_id: this.extDriver._id,
          auth_key: this.extDriver.auth_key,
          car_registration_approved: this.registration,
          onboarding_status: '4',
          profile_picture_approved: this.profile,
          driver_license_approved: this.licence,
          insurance_details_approved: '0',
          rta_card_approved: this.rtaCard,
          emirates_id_approved: this.emirates
        }
      }
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: this.email
    }
    this.extDriverService.extDriverUpdate(enc_data).then((dec) => {
      this.submit = false;
      if (dec) {
        if (dec.status == 200) {
          var data: any = this.encDecService.dwt(this.session, dec.data);
          this.toastr.success('Status updated successfully.');
          this.router.navigate(['/admin/fleet/ext-driver/list']);
        }
      }
    });
  }
  submitDriverForm(data) {
    this.isLoading = true;
    this.invalid = '';
    var params = {
      company_id: this.companyId,
      on_boarding_driver_id: this.extDriver._id,
      auth_key: this.extDriver.auth_key,
      nationality: data.nationality,
      birth_date: data.birth_date,
      driver_licence_number: data.licence_number,
      driver_licence_expiry_date: data.driver_licence_expiry_date,
      driver_licence_issue_date: data.driver_licence_issue_date,
      driver_license_place_of_issue: data.driver_license_place_of_issue,
      driver_licensing_authority: data.driver_licensing_authority,
      emirates_id_expiry: data.emirates_id_expiry,
      emirates_id: data.emirates_id,
      display_name: data.display_name,
      permit_number: data.permit_number,
      permit_expiry_date: data.permit_expiry_date
    }
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: this.email
    }
    this.extDriverService.extDriverUpdate(enc_data).then((dec) => {
      this.isLoading = false;
      if (dec && dec.status == 200) {
        this.toastr.success('Status updated successfully.');
        let el: HTMLElement = document.getElementById('driverInfo') as HTMLElement;
        el.click()
      }
      else {
        this.invalid = dec.message ? dec.message : 'Something went wrong';
      }
    });
  }
  submitVehicleForm(data) {
    this.isLoading = true;
    this.invalid2 = '';
    var params = {
      company_id: this.companyId,
      on_boarding_driver_id: this.extDriver._id,
      auth_key: this.extDriver.auth_key,
      traffic_plate_number: data.traffic_plate_number,
      vehicle_tc_number: data.vehicle_tc_number,
      vehicle_expiry_date: data.vehicle_expiry_date,
      vehicle_insurance_expiry_date: data.vehicle_insurance_expiry_date,
      vehicle_policy_number: data.vehicle_policy_number,
      vehicle_model: data.vehicle_model,
      vehicle_model_id: data.vehicle_model,
      vehicle_owner: data.vehicle_owner,
      vehicle_registration_date: data.vehicle_registration_date,
      vehicle_origin: data.vehicle_origin,
      vehicle_type: data.vehicle_type,
      vehicle_gvw: data.vehicle_gvw,
      vehicle_weight: data.vehicle_weight,
      vehicle_engine_number: data.vehicle_engine_number,
      vehicle_chasis_number: data.vehicle_chasis_number,
      vehicle_licensing_authority: data.vehicle_licensing_authority,
      vehicle_identity_number: data.vehicle_identity_number,
      vehicle_color: data.vehicle_color
    }
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: this.email
    }
    this.extDriverService.extDriverUpdate(enc_data).then((dec) => {
      this.isLoading = false;
      if (dec && dec.status == 200) {
        this.toastr.success('Status updated successfully.');
        let el: HTMLElement = document.getElementById('driverInfo') as HTMLElement;
        el.click()
      }
      else {
        this.invalid2 = dec.message ? dec.message : 'Something went wrong';
      }
    });
  }
  getVehicleModels() {
    const params = {
      offset: 0,
      limit: 0,
      sortOrder: 'asc',
      sortByColumn: 'name',
      search: '',
      company_id: this.companyId
    };
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: this.email
    }
    this._vehicleModelsService.getVehicleModels(enc_data).then((dec) => {
      if (dec && dec.status == 200) {
        var data: any = this.encDecService.dwt(this.session, dec.data);
        this.vehicleModelData = data.vehicleModelsList;
      }
    });
  }
  checkApprovalStatus() {
    return this.profile != '1' ||
      this.rtaCard != '1' ||
      this.licence != '1' ||
      this.registration != '1' ||
      this.emirates != '1' ||
      this.extDriver.onboarding_status == '3' ||
      !this.extDriver.nationality ||
      !this.extDriver.birth_date ||
      !this.extDriver.driver_licence_number ||
      !this.extDriver.driver_licence_expiry_date ||
      !this.extDriver.driver_licence_issue_date ||
      !this.extDriver.vehicle_identity_number ||
      !this.extDriver.traffic_plate_number ||
      !this.extDriver.vehicle_tc_number ||
      !this.extDriver.vehicle_expiry_date ||
      !this.extDriver.vehicle_model ||
      !this.extDriver.vehicle_color ||
      !this.extDriver.vehicle_registration_date ||
      !this.extDriver.display_name
  }
  checkApproveAgainStatus() {
    return this.profile != '1' ||
      this.rtaCard != '1' ||
      this.licence != '1' ||
      this.registration != '1' ||
      this.emirates != '1' ||
      !this.extDriver.vehicle_identity_number ||
      !this.extDriver.traffic_plate_number ||
      !this.extDriver.vehicle_tc_number ||
      !this.extDriver.vehicle_expiry_date ||
      !this.extDriver.vehicle_model ||
      !this.extDriver.vehicle_registration_date ||
      !this.extDriver.display_name ||
      this.extDriver.onboarding_status !== '2'
  }
  getBankList() {
    var params = {
      company_id: this.companyId,
      on_boarding_driver_id: this.extDriver._id,
      auth_key: this.extDriver.auth_key,
    }
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: this.email
    }
    this.extDriverService.bankList(enc_data).then((dec) => {
      if (dec) {
        if (dec.status == 200) {
          var data: any = this.encDecService.dwt(this.session, dec.data);
          this.bankList = data.data;
          this.tempBank = data.data;
        }
      }
    });
  }

  submitIbanForm(data) {
    this.isLoading = true;
    this.invalid3 = '';
    if (this.bankCtrl.value._id==undefined) {
      this.isLoading = false;
      this.invalid3 = "Invalid bank details"
      return;
    }
    var params = {
      on_boarding_driver_id: this.customerId,
      company_id: this.companyId,
      auth_key: this.extDriver.auth_key,
      account_holder_title: data.account_holder_title,
      iban_number: data.iban_number,
      bank_name: this.bankCtrl.value.entity_name,
      bank_id: this.bankCtrl.value._id
    }
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: this.email
    }
    if(this.extDriver.driver_id)
    this.extDriverService.extDriverBankUpdate(enc_data).then((dec) => {
      this.isLoading = false;
      if (dec && dec.status == 200) {
        this.toastr.success('Status updated successfully.');
        let el: HTMLElement = document.getElementById('driverInfo') as HTMLElement;
        el.click()
      }
      else {
        this.invalid3 = dec.message ? dec.message : 'Something went wrong';
      }
    })
    else
    this.extDriverService.extDriverUpdate(enc_data).then((dec) => {
      this.isLoading = false;
      if (dec && dec.status == 200) {
        this.toastr.success('Status updated successfully.');
        let el: HTMLElement = document.getElementById('driverInfo') as HTMLElement;
        el.click()
      }
      else {
        this.invalid3 = dec.message ? dec.message : 'Something went wrong';
      }
    })
  }
  checkRejectedStatus() {
    return this.profile !== '2' &&
      this.rtaCard !== '2' &&
      this.licence !== '2' &&
      this.registration !== '2' &&
      this.emirates !== '2'
  }
  displayFnBank(data) {
    return data ? data.entity_name : '';
  }
}
