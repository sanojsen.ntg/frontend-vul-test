import { Component, OnInit } from '@angular/core';
import { AccessControlService } from '../../../common/services/access-control/access-control.service';
import { ActivatedRoute, Router } from '@angular/router';
import { EncDecService } from '../../../common/services/encrypt-decrypt-service/encrypt_decrypt_service';

@Component({
  selector: 'app-performance-monitoring',
  templateUrl: './performance-monitoring.component.html',
  styleUrls: ['./performance-monitoring.component.css']
})
export class PerformanceMonitoringComponent implements OnInit {

  public aclCheck;
  constructor(public _aclService: AccessControlService, private route: ActivatedRoute,
    public encDecService: EncDecService,
    private router: Router) {
    this.aclDisplayService();
    this.aclDisplayService();
  }

  ngOnInit() {
  }
  public aclDisplayService() {
    this.aclCheck = JSON.parse(localStorage.getItem('userMenu'));
  }

}
