import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { PerformanceMailStatsComponent } from './performance-mail-stats/performance-mail-stats.component';
import { PerformanceMonitoringComponent } from './performance-monitoring/performance-monitoring.component';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';
import { FormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    OwlDateTimeModule,
    OwlNativeDateTimeModule,
    FormsModule
  ],
  declarations: [PerformanceMailStatsComponent, PerformanceMonitoringComponent],
  exports: [
    RouterModule
  ]
})
export class PerformanceMonitoringModule { }
