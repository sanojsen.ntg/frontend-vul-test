import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AccessControlService } from '../../../common/services/access-control/access-control.service';
import { EncDecService } from '../../../common/services/encrypt-decrypt-service/encrypt_decrypt_service';

@Component({
  selector: 'app-marketing-dashboard',
  templateUrl: './marketing-dashboard.component.html',
  styleUrls: ['./marketing-dashboard.component.css']
})
export class MarketingDashboardComponent implements OnInit {
  public companyId:any = [];
  public aclCheck;
  email: string;
  session: string;
  constructor(private router: Router,
    public encDecService:EncDecService,
    private _aclService:AccessControlService) {const company_id:any = localStorage.getItem('user_company');
    this.session= localStorage.getItem('Sessiontoken');
    this.email= localStorage.getItem('user_email');
    this.companyId.push(company_id);
    this.aclDisplayService();
   }

  ngOnInit() {
  }

  public aclDisplayService(){
    this.aclCheck = JSON.parse(localStorage.getItem('userMenu'));
  //   var params = {
  //     company_id: this.companyId
  //   }
  //   var encrypted = this.encDecService.nwt(this.session,params);
  //   var enc_data = {
  //     data: encrypted,
  //     email: this.email
  //   }
  //   this._aclService.getAclUserMenu(enc_data).then((dec) => {
  //     if(dec.status==200){
  //     var data:any = this.encDecService.dwt(this.session,dec.data);
  //     this.aclCheck = data.menu;
  //     }
  //   })
  }
}
