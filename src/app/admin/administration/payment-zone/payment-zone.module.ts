import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AddPaymentZoneComponent } from './add-payment-zone/add-payment-zone.component';
import { NguiMapModule } from '@ngui/map';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule, Routes, PreloadAllModules } from '@angular/router';
import { BrowserModule  } from '@angular/platform-browser';
import {environment} from '../../../../environments/environment';
import { MatSelectModule } from '@angular/material/select';
import { PaymentZoneService } from './../../../common/services/paymentzone/paymentzone.service';
import { PaymentZoneRestService } from './../../../common/services/paymentzone/paymentzonerest.service';
import { ListPaymentZoneComponent } from './list-payment-zone/list-payment-zone.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { MatTooltipModule } from '@angular/material/tooltip';
import { EditPaymentZoneComponent } from './edit-payment-zone/edit-payment-zone.component';

@NgModule({
  imports: [
	 BrowserModule,
     FormsModule,
     ReactiveFormsModule,
     MatSelectModule,
     RouterModule,
     CommonModule,
     NgbModule,
     MatTooltipModule,
	NguiMapModule.forRoot({apiUrl: 'https://maps.google.com/maps/api/js?key=' + environment.map_key + '&libraries=visualization,places,drawing'}),
  ],
  declarations: [AddPaymentZoneComponent, ListPaymentZoneComponent, EditPaymentZoneComponent],
  providers: [ 
    PaymentZoneService,  
    PaymentZoneRestService
  ],
    exports:[MatSelectModule]
})
export class PaymentZoneModule { }
