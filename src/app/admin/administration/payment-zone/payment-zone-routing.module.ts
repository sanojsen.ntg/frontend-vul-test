
import { NgModule } from '@angular/core';
import { RouterModule, Routes , PreloadAllModules } from '@angular/router';
import {AddPaymentZoneComponent} from './add-payment-zone/add-payment-zone.component';
import { ListPaymentZoneComponent } from './list-payment-zone/list-payment-zone.component';
import { AclAuthervice } from '../../../common/services/access-control/acl-auth.service';
import { EditPaymentZoneComponent } from './edit-payment-zone/edit-payment-zone.component';

export const paymentZoneRoutes: Routes = [
  { path: '', component: ListPaymentZoneComponent, canActivate: [AclAuthervice], data: {roles: ["Drive through payment zones / Tolls"]}},
  { path: 'add', component: AddPaymentZoneComponent, canActivate: [AclAuthervice], data: {roles: ["Drive through payment zones / Tolls -Add"]}},
  { path: 'viewzone/:id', component: AddPaymentZoneComponent, canActivate: [AclAuthervice], data: {roles: ["Drive through payment zones / Tolls - View"]}},
  ];
