import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListPaymentZoneComponent } from './list-payment-zone.component';

describe('ListPaymentZoneComponent', () => {
  let component: ListPaymentZoneComponent;
  let fixture: ComponentFixture<ListPaymentZoneComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListPaymentZoneComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListPaymentZoneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
