
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { NguiMapModule } from '@ngui/map';
import { MiniZoneListComponent } from './mini-zone-list/mini-zone-list.component';
import { MiniZoneAddComponent } from './mini-zone-add/mini-zone-add.component';
import { MiniZoneEditComponent } from './mini-zone-edit/mini-zone-edit.component';
import { MiniZoneAllComponent } from './mini-zone-all/mini-zone-all.component';

@NgModule({
  imports: [
    CommonModule,
    NgbModule,
    FormsModule,
    RouterModule,
    NguiMapModule
  ],
  declarations: [
    MiniZoneListComponent,
    MiniZoneAddComponent,
    MiniZoneEditComponent,
    MiniZoneAllComponent
  ],
  providers: [ 
  ],
})
export class MiniZoneModule { }
