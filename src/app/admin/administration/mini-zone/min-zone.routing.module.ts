
import { RouterModule, Routes , PreloadAllModules } from '@angular/router';
import { AclAuthervice } from '../../../common/services/access-control/acl-auth.service';
import { MiniZoneListComponent } from './mini-zone-list/mini-zone-list.component';
import { MiniZoneAddComponent } from './mini-zone-add/mini-zone-add.component';
import { MiniZoneEditComponent } from './mini-zone-edit/mini-zone-edit.component';
import { MiniZoneAllComponent } from './mini-zone-all/mini-zone-all.component';

export const MiniZoneRoutes: Routes = [
    {path:'', component: MiniZoneListComponent, canActivate: [AclAuthervice], data: {roles: ["Dubai Major Zone - List"]}},
    {path:'all', component: MiniZoneAllComponent, canActivate: [AclAuthervice], data: {roles: ["Dubai Major Zone - List"]}},
    {path:'edit/:id', component: MiniZoneEditComponent, canActivate: [AclAuthervice], data: {roles: ["Dubai Major Zone - Edit"]}},
    {path:'add', component: MiniZoneAddComponent, canActivate: [AclAuthervice], data: {roles: ["Dubai Major Zone - Add"]}}
];
