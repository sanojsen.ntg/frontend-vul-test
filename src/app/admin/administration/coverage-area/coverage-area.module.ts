import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NguiMapModule } from '@ngui/map';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule, Routes, PreloadAllModules } from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';
import { environment } from '../../../../environments/environment';
import { MatSelectModule } from '@angular/material/select';
import { CoverageareaService } from './../../../common/services/coveragearea/coveragearea.service';
import { CoveragearearestService } from './../../../common/services/coveragearea/coveragearearest.service';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { MatTooltipModule } from '@angular/material/tooltip';
//import { ListCoverageAreaComponent } from './list-coverage-area/list-coverage-area.component';
//import { AddCoverageAreaComponent } from './add-coverage-area/add-coverage-area.component';


@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    MatSelectModule,
    RouterModule,
    CommonModule,
    NgbModule,
    MatTooltipModule,
    NguiMapModule.forRoot({ apiUrl: 'https://maps.google.com/maps/api/js?key=' + environment.map_key + '&libraries=visualization,places,drawing' }),
  ],
  declarations: [],
  providers: [
    CoverageareaService,
    CoveragearearestService
  ],
  exports: [MatSelectModule]

})
export class CoverageAreaModule { }
