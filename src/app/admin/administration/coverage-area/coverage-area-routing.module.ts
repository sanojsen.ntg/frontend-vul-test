
import { NgModule } from '@angular/core';
import { RouterModule, Routes, PreloadAllModules } from '@angular/router';
import { AddCoverageAreaComponent } from './add-coverage-area/add-coverage-area.component';
import { ListCoverageAreaComponent } from './list-coverage-area/list-coverage-area.component';
import { AclAuthervice } from '../../../common/services/access-control/acl-auth.service';

export const coverageAreaRoutes: Routes = [
  { path: '', component: ListCoverageAreaComponent, canActivate: [AclAuthervice], data: {roles: ["FAQ - List"]} },
  { path: 'add', component: AddCoverageAreaComponent, canActivate: [AclAuthervice], data: {roles: ["FAQ - List"]} },
  { path: 'viewcoverage/:id', component: AddCoverageAreaComponent, canActivate: [AclAuthervice], data: {roles: ["FAQ - List"]}},
];
