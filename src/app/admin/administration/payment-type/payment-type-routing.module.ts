import { NgModule } from '@angular/core';
import { RouterModule, Routes , PreloadAllModules } from '@angular/router';
import { ListPaymentTypeComponent } from './list-payment-type/list-payment-type.component';
import { AddPaymentTypeComponent } from './add-payment-type/add-payment-type.component';
import { EditPaymentTypeComponent } from './edit-payment-type/edit-payment-type.component';
import { AclAuthervice } from '../../../common/services/access-control/acl-auth.service';

export const paymentTypeRoutes: Routes = [
     { path: '', component: ListPaymentTypeComponent , canActivate: [AclAuthervice], data: {roles: ["Payment Type List"]}},
     { path: 'add', component: AddPaymentTypeComponent , canActivate: [AclAuthervice], data: {roles: ["Payment Type Add"]}},
     { path: 'edit/:id', component: EditPaymentTypeComponent , canActivate: [AclAuthervice], data: {roles: ["Payment Type - Edit"]} } 
  ];
