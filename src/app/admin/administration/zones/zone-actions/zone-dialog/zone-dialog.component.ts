import { Component, OnInit, ViewChild, Inject, ViewContainerRef } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ToastsManager } from 'ng2-toastr';
import { Router } from '@angular/router';
@Component({
  selector: 'app-zone-dialog',
  templateUrl: './zone-dialog.component.html'
})
export class ZoneDialogComponent implements OnInit {


  public addPaymentZone: FormGroup;
  public paymentZone;
  public paymentData;
  constructor(
    public dialogRef: MatDialogRef<ZoneDialogComponent>,
    private toastr: ToastsManager,
    private vcr: ViewContainerRef,
    private formBuilder: FormBuilder,
    private router: Router,
    @Inject(MAT_DIALOG_DATA) public data: any) {
    this.paymentZone = data;
  }
  public zone = {
    name: '',
    pin_zone: 'false'
  }

  ngOnInit() {
  }

  public saveZone() {
  }
  closePop() {
    this.dialogRef.close(false);
  }

}
