import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ZoneMultipleComponent } from './zone-multiple.component';

describe('ZoneMultipleComponent', () => {
  let component: ZoneMultipleComponent;
  let fixture: ComponentFixture<ZoneMultipleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ZoneMultipleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ZoneMultipleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
