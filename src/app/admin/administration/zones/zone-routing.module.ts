import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes , PreloadAllModules } from '@angular/router';
import { ZoneMenuComponent } from './zone-menu/zone-menu.component';
import { ZonesComponent } from './zones/zones.component';
import { ZoneActions } from './zone-actions/zone-action-routes.module';
import { AclAuthervice } from '../../../common/services/access-control/acl-auth.service';
export const zoneRoutes: Routes = [
  { path: '', component: ZoneMenuComponent, canActivate: [AclAuthervice], data: {roles: ["Heat Map"]}},
  { path: 'allzones', component:ZonesComponent, canActivate: [AclAuthervice], data: {roles: ["Heat Map"]}},
  { path: 'zonelist', children: ZoneActions, canActivate: [AclAuthervice], data: {roles: ["Heat Map"]} }
];
