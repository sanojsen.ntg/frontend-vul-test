import { NgModule } from '@angular/core';
import { RouterModule, Routes , PreloadAllModules } from '@angular/router';
import { ListVehicleCategoriesComponent } from './list-sub-categories/list-sub-categories.component';
import { AddVehicleCategoriesComponent } from './add-sub-categories/add-sub-categories.component'
import { EditVehicleCategoriesComponent } from './edit-sub-categories/edit-sub-categories.component'
import { AclAuthervice } from '../../../common/services/access-control/acl-auth.service';

export const vehicleCategoryRoutes: Routes = [
     { path: '', component: ListVehicleCategoriesComponent, canActivate: [AclAuthervice], data: {roles: ["Vehicle Sub Categories - List"]}},
     { path: 'add', component: AddVehicleCategoriesComponent, canActivate: [AclAuthervice], data: {roles: ["Vehicle Sub Categories - Add"]}},
     { path: 'edit/:id', component: EditVehicleCategoriesComponent, canActivate: [AclAuthervice], data: {roles: ["Vehicle Sub Categories - Edit"]}},
  
  ];
