import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditVehicleCategoriesComponent } from './edit-vehicle-categories.component';

describe('EditVehicleCategoriesComponent', () => {
  let component: EditVehicleCategoriesComponent;
  let fixture: ComponentFixture<EditVehicleCategoriesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditVehicleCategoriesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditVehicleCategoriesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
