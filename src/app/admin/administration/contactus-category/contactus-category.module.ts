import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { MatTooltipModule } from '@angular/material/tooltip';

import { ContactusCategoryService } from '../../../common/services/contactus-category/contactus-category.service';
import { ContactusCategoryRestService } from '../../../common/services/contactus-category/contactus-category-rest.service';

import { ListContactusCategoryComponent } from './list-contactus-category/list-contactus-category.component';
import { AddContactusCategoryComponent } from './add-contactus-category/add-contactus-category.component';
import { EditContactusCategoryComponent } from './edit-contactus-category/edit-contactus-category.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    NgbModule,
    MatTooltipModule
  ],
  declarations: [
    ListContactusCategoryComponent, 
    AddContactusCategoryComponent, 
    EditContactusCategoryComponent
  ],
  providers: [
    ContactusCategoryService,
    ContactusCategoryRestService
  ]
})

export class ContactusCategoryModule { }
