import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ZendeskCommentsComponent } from './zendesk-comments.component';

describe('ZendeskCommentsComponent', () => {
  let component: ZendeskCommentsComponent;
  let fixture: ComponentFixture<ZendeskCommentsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ZendeskCommentsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ZendeskCommentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
