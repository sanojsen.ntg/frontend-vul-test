import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ListMessageHistoryComponent } from './list-message-history/list-message-history.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule } from '@angular/forms';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';

@NgModule({
  imports: [
    CommonModule,
    NgbModule,
    FormsModule,
    OwlDateTimeModule,
    OwlNativeDateTimeModule
  ],
  declarations: [ListMessageHistoryComponent]
})
export class MessageHistoryModule { }
