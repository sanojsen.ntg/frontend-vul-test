import { NgModule } from '@angular/core';
import { RouterModule, Routes, PreloadAllModules } from '@angular/router';
import { ListMessageHistoryComponent } from './list-message-history/list-message-history.component';
import { AclAuthervice } from '../../../common/services/access-control/acl-auth.service';

export const MessageHistoryRoutes: Routes = [
  { path: '', component: ListMessageHistoryComponent, canActivate: [AclAuthervice], data: {roles: ["Message History"]} }
];
