import { NgModule } from '@angular/core';
import { RouterModule, Routes , PreloadAllModules } from '@angular/router';
import { ListPredefinedMessagesComponent } from './list-predefined-messages/list-predefined-messages.component';
import { AddPredefinedMessagesComponent } from './add-predefined-messages/add-predefined-messages.component';
import { EditPredefinedMessagesComponent } from './edit-predefined-messages/edit-predefined-messages.component';
import { AclAuthervice } from '../../../common/services/access-control/acl-auth.service';

export const PredefinedMessagesRoutes: Routes = [
     { path: '', component: ListPredefinedMessagesComponent, canActivate: [AclAuthervice], data: {roles: ["Predefined Messages - List"]}},
     { path: 'add', component: AddPredefinedMessagesComponent, canActivate: [AclAuthervice], data: {roles: ["Predefined Messages - Add"]}},
     { path: 'edit/:id', component: EditPredefinedMessagesComponent, canActivate: [AclAuthervice], data: {roles: ["Predefined Messages - Edit"]}}
  ];
