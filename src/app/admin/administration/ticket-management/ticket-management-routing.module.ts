import { Routes, RouterModule } from '@angular/router';
import { TicketListComponent } from './ticket-list/ticket-list.component';
import { TicketAddComponent } from './ticket-add/ticket-add.component';
import { TicketEditComponent } from './ticket-edit/ticket-edit.component';

export const ticketRoutes: Routes = [
  {path:'', component:TicketListComponent},
  {path:'add', component:TicketAddComponent},
  {path:'edit/:id', component:TicketEditComponent}
];

