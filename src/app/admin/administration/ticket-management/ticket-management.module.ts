import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes, PreloadAllModules } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { TicketListComponent } from './ticket-list/ticket-list.component';
import { TicketAddComponent } from './ticket-add/ticket-add.component';
import { TicketEditComponent } from './ticket-edit/ticket-edit.component';
import { MatInputModule,MatChipsModule ,MatIconModule} from '@angular/material';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    FormsModule, 
    ReactiveFormsModule,
    MatInputModule,
    MatChipsModule ,
    MatIconModule,
    NgbModule,
    OwlDateTimeModule, 
    OwlNativeDateTimeModule
  ],
  declarations: [TicketListComponent, TicketAddComponent, TicketEditComponent]
})
export class TicketManagementModule { }
