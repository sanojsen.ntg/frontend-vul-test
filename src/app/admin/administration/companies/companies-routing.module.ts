import { NgModule } from '@angular/core';
import { RouterModule, Routes, PreloadAllModules } from '@angular/router';
import { ListCompaniesComponent } from './list-companies/list-companies.component';
import { AddCompaniesComponent } from './add-companies/add-companies.component';
import { EditCompaniesComponent } from './edit-companies/edit-companies.component';
import { AclAuthervice } from '../../../common/services/access-control/acl-auth.service';

export const CompanyRoutes: Routes = [
    { path: '', component: ListCompaniesComponent, canActivate: [AclAuthervice], data: { roles: ["Companies-List"] } },
    { path: 'add', component: AddCompaniesComponent, canActivate: [AclAuthervice], data: { roles: ["Companies-Add"] } },
    { path: 'edit/:id', component: EditCompaniesComponent, canActivate: [AclAuthervice], data: { roles: ["Companies-Edit"] } }

];