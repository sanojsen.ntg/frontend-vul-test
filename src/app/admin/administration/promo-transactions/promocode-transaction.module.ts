import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { PromocoderestService } from './../../../common/services/promocode/promocoderest.service';
import { PromocodeService } from './../../../common/services/promocode/promocode.service';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { MatSelectModule } from '@angular/material/select';
import { MatTooltipModule } from '@angular/material/tooltip';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';
import { PromoTransactionListComponent } from './promo-transaction-list/promo-transaction-list.component';
import { MatInputModule,MatChipsModule ,MatIconModule} from '@angular/material';
import { MatAutocompleteModule } from '@angular/material/autocomplete';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    NgbModule,
    OwlDateTimeModule,
    OwlNativeDateTimeModule,
    MatSelectModule,
    MatTooltipModule,
    MatInputModule,
    MatAutocompleteModule,
    MatChipsModule,
    MatIconModule
  ],
  declarations: [PromoTransactionListComponent],
  providers: [PromocodeService, PromocoderestService]
})
export class PromocodeTransactionModule { }
