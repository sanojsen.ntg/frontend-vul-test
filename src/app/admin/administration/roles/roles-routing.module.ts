import { NgModule } from '@angular/core';
import { RouterModule, Routes , PreloadAllModules } from '@angular/router';
import { ListRolesComponent } from './list-roles/list-roles.component';
import { AddRolesComponent } from './add-roles/add-roles.component';
import { EditRolesComponent } from './edit-roles/edit-roles.component';


export const RolesRoutes: Routes = [
     { path: '', component: ListRolesComponent},
     { path: 'add', component: AddRolesComponent},
     { path: 'edit/:id' , component: EditRolesComponent}
     
  ];
