import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdministrationSubmenusComponent } from './administration-submenus.component';

describe('AdministrationSubmenusComponent', () => {
  let component: AdministrationSubmenusComponent;
  let fixture: ComponentFixture<AdministrationSubmenusComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdministrationSubmenusComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdministrationSubmenusComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
