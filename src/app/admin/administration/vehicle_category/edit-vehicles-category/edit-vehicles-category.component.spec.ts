import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditVehicleSubCategoryComponent } from './edit-vehicle-sub-category.component';

describe('EditVehicleSubCategoryComponent', () => {
  let component: EditVehicleSubCategoryComponent;
  let fixture: ComponentFixture<EditVehicleSubCategoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditVehicleSubCategoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditVehicleSubCategoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
