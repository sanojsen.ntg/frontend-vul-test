import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { Router } from '@angular/router';
import { VehicleCategoryService } from '../../../../common/services/vehicle-category/vehicle-category.service';
import { ToastsManager } from 'ng2-toastr';
import { FileHolder } from 'angular2-image-upload';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { EncDecService } from '../../../../common/services/encrypt-decrypt-service/encrypt_decrypt_service';

@Component({
  selector: 'app-add-vehicles-category',
  templateUrl: './add-vehicles-category.component.html',
  styleUrls: ['./add-vehicles-category.component.css']
})
export class AddVehicleSubCategoryComponent implements OnInit {
  public VehicleData;
  public driverGroupData;
  public ShiftTimingsData;
  public categoriesData;
  public dateObj = new Date();
  public spandata = 'no';
  public saveForm: FormGroup;
  public companyId:any = [];
  public session;
  constructor(private _categoryService: VehicleCategoryService,
    private _toasterService: ToastsManager,
    private router: Router,
    formBuilder: FormBuilder,
    public encDecService:EncDecService,
    private vcr: ViewContainerRef) {  
      this.saveForm = formBuilder.group({
      category: ['', [
        Validators.required,
      ]],
      image: ['', Validators.required,]
    });
  }

  ngOnInit() {
    this.companyId.push( localStorage.getItem('user_company'))
    this.session = localStorage.getItem('Sessiontoken');
  }

  /**
  * Save vehicle categories
  */
 addVehicleCategory(){
 
  if (!this.saveForm.valid) {
    return;
  }
  const vehicleModel = {
    category: this.saveForm.value.category,
    image: this.saveForm.value.image,
    company_id: localStorage.getItem('user_company')
  };
  var encrypted = this.encDecService.nwt(this.session,vehicleModel);
  var enc_data = {
    data: encrypted,
    email: localStorage.getItem('user_email')
  }
  this._categoryService.addVehicleCategory(enc_data)
    .then((dec) => {
      if (dec.status == 200) {
        this._toasterService.success('Vehicle category added successfully.');
        this.router.navigate(['/admin/administration/vehicle-categories']);
      }
      else if (dec.status === 201) {
                this._toasterService.error('Vehicle categories addition failed.');
              }
              else{
                this._toasterService.warning('Vehicle category name already exist.');
              }
    }).catch((error) => {
    });
}

imageFinishedUploading(file: FileHolder) {
  this.saveForm.controls['image'].setValue(file.src);
  this.spandata = 'yes';
}

onRemoved(file: FileHolder) {
  this.saveForm.controls['image'].setValue(null);
this.spandata = 'no';
}

}
