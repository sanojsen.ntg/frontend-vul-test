import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListVehicleSubCategoryComponent } from './list-vehicle-sub-category.component';

describe('ListVehicleSubCategoryComponent', () => {
  let component: ListVehicleSubCategoryComponent;
  let fixture: ComponentFixture<ListVehicleSubCategoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListVehicleSubCategoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListVehicleSubCategoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
