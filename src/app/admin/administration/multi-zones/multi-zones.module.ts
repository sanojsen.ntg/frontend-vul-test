import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NguiMapModule } from '@ngui/map';
import { PetrolZonesComponent } from './petrol-zones/petrol-zones.component';
import { HighwayZonesComponent } from './highway-zones/highway-zones.component';
@NgModule({
  imports: [
    CommonModule,
    NguiMapModule,
  ],
  declarations: [PetrolZonesComponent, HighwayZonesComponent]
})
export class MultiZonesModule { }
