
import { RouterModule, Routes , PreloadAllModules } from '@angular/router';
import { ListPartnersComponent } from './list-partners/list-partners.component';
import { AddPartnersComponent } from './add-partners/add-partners.component';
import { EditPartnersComponent } from './edit-partners/edit-partners.component';
import { AclAuthervice } from '../../../common/services/access-control/acl-auth.service';

export const partnersRoutes: Routes = [
     { path: '', component: ListPartnersComponent, canActivate: [AclAuthervice], data: {roles: ["Partners List"]}},
     { path: 'add', component: AddPartnersComponent, canActivate: [AclAuthervice], data: {roles: ["Partners -Create"]} },
     { path: 'edit/:id', component: EditPartnersComponent, canActivate: [AclAuthervice], data: {roles: ["Partners -Delete"]} } 
];
