import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatTooltipModule } from '@angular/material/tooltip';
import { RouterModule, Routes, PreloadAllModules } from '@angular/router';
import { ListPartnersComponent } from './list-partners/list-partners.component';
import { EditPartnersComponent } from './edit-partners/edit-partners.component';
import { AddPartnersComponent } from './add-partners/add-partners.component';
import { PartnerService } from '../../../common/services/partners/partner.service';
import { PartnerRestService } from '../../../common/services/partners/partnerrest.service';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatInputModule, MatNativeDateModule } from '@angular/material';
import { MatSelectModule } from '@angular/material/select';

@NgModule({
  imports: [
    CommonModule,
    NgbModule,
    FormsModule,
    ReactiveFormsModule,
    MatTooltipModule,
    RouterModule,
    MatAutocompleteModule,
    MatNativeDateModule,
    MatSelectModule
  ],
  declarations: [
    ListPartnersComponent, 
    EditPartnersComponent, 
    AddPartnersComponent
  ],
  providers: [ 
    PartnerService,  
    PartnerRestService
  ],
})
export class PartnersModule { }
