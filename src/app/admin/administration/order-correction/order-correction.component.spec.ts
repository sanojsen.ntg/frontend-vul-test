import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrderCorrectionComponent } from './order-correction.component';

describe('OrderCorrectionComponent', () => {
  let component: OrderCorrectionComponent;
  let fixture: ComponentFixture<OrderCorrectionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrderCorrectionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrderCorrectionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
