import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes, PreloadAllModules } from '@angular/router';
import { AclAuthervice } from './../../common/services/access-control/acl-auth.service'
import { FinanceMenuComponent} from './finance-menu/finance-menu.component';
import { FinanceListComponent } from './finance-list/finance-list.component';

export const FinanceRoute: Routes = [
      { path: '', component: FinanceMenuComponent, canActivate: [AclAuthervice], data: { roles: ["Finance"] } },
      { path: 'finance-list', component: FinanceListComponent, canActivate: [AclAuthervice], data: { roles: ["Finance - List"] } },
    ];