import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FinanceMenuComponent } from './finance-menu/finance-menu.component';
import { FinanceListComponent } from './finance-list/finance-list.component';
import { RouterModule, Routes, PreloadAllModules } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { MatCardModule } from '@angular/material';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatSelectModule } from '@angular/material/select';
import { FinanceServicesService } from '../../common/services/finance/finance-services.service';
import { FinanceServicesRestService } from '../../common/services/finance/finance-services-rest.service';
import { TimeDifferencePipe2 } from '../../common/pipes/time_difference_2_pipe';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    MatCardModule,
    MatAutocompleteModule,
    OwlDateTimeModule, OwlNativeDateTimeModule,
    MatTooltipModule,
    RouterModule,
    MatSelectModule,
    ReactiveFormsModule,
    NgbModule,
  ],
  providers: [FinanceServicesRestService, FinanceServicesService],
  exports: [
    RouterModule, FormsModule
  ],
  declarations: [FinanceMenuComponent, FinanceListComponent,TimeDifferencePipe2]
})
export class FinanceModule { }
