import { ViewEncapsulation , Component } from '@angular/core';



@Component({
  selector: 'app-dispatcher',
  templateUrl: './dispatcher.component.html',
  styleUrls: ['./dispatcher.component.css'],
  encapsulation : ViewEncapsulation.None
})
export class DispatcherComponent {
  constructor() {}
}
