import { Component, Inject, OnInit, ViewChild, ViewContainerRef } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { VehicleService } from '../../../../common/services/vehicles/vehicle.service';
import { OrderService } from '../../../../common/services/order/order.service';
import { EncDecService } from '../../../../common/services/encrypt-decrypt-service/encrypt_decrypt_service';
import { JwtService } from '../../../../common/services/api/jwt.service';
import { ToastsManager } from 'ng2-toastr';
import { Router } from '@angular/router';


@Component({
  selector: 'app-order-info',
  templateUrl: 'order-info.html',
  styleUrls: ['./order-info.css']
})
export class OrderInfoComponent {
  public session_token;
  public useremail;
  public company_id_array = [];
  public usercompany;
  public socket_session_token;
  public vehicleData;
  public driver_id;
  public orderData;
  public orderlength;
  public currentPage;
  public pageSize = 5;
  public pageNo = 1;
  constructor(
    private router: Router,
    public _vehicleService: VehicleService,
    private _EncDecService: EncDecService,
    public _orderService: OrderService,
    public dialogRef: MatDialogRef<OrderInfoComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public toastr: ToastsManager,
    private _jwtService: JwtService) {
    this.vehicleData = data;
  }

  ngOnInit() {
    this.session_token = window.localStorage['Sessiontoken'];
    this.useremail = window.localStorage['user_email'];
    this.company_id_array = [];
    this.usercompany = window.localStorage['user_company'];
    this.socket_session_token = window.localStorage['SocketSessiontoken'];
    this.company_id_array.push(this.usercompany)
    this.driver_id = this.vehicleData.driver_id._id ? this.vehicleData.driver_id._id : this.vehicleData.drivers['0']._id;
    const params = {
      offset: 0,
      limit: 5,
      sortOrder: 'desc',
      sortByColumn: '_id',
      driver_id: this.driver_id,
      order_status: 'all',
      company_id: this.company_id_array
    };
    let enc_data = this._EncDecService.nwt(this.session_token, params);
    //alert(this.dispatcher_id.email)
    let data1 = {
      data: enc_data,
      email: this.useremail
    }
    this._orderService.getOrderByDriverId(data1).then((data) => {
      if (data && data.status == 200) {
        data = this._EncDecService.dwt(this.session_token, data.data);
        this.orderData = data.orders;
        this.orderlength = data.count;
      }
    });
  }

  pagingAgent(data) {
    this.currentPage = data;
    this.orderData = [];
    this.pageNo = (data * this.pageSize) - this.pageSize;
    const params_pagingAgent = {
      offset: this.pageNo,
      limit: this.pageSize,
      sortOrder: 'desc',
      sortByColumn: '_id',
      driver_id: this.driver_id,
      order_status: 'all',
      company_id: this.company_id_array
    };
    let enc_data = this._EncDecService.nwt(this.session_token, params_pagingAgent);
    //alert(this.dispatcher_id.email)
    let data1 = {
      data: enc_data,
      email: this.useremail
    }
    this._orderService.getOrderByDriverId(data1).then((data) => {
      if (data && data.status == 200) {
        data = this._EncDecService.dwt(this.session_token, data.data);
        this.orderData = data.orders;
      }
    });
  }


  closePop() {
    this.dialogRef.close();
  }
}
