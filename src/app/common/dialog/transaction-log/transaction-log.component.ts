import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-transaction-log',
  templateUrl: './transaction-log.component.html',
  styleUrls: ['./transaction-log.component.css']
})
export class TransactionLogComponent implements OnInit {
  public transaction_data:any = [];
  public keys = [];
  public heading='';
  public reasonCodes={
    100:': Successful transaction',
    102:': One or more fields in the request contain Invalid data',
    104:': Duplicate transaction was detected. The transaction might have already been processed',
    110:': Only partial amount was approved',
    150:': General system failure',
    151:': The request was received but a server timeout occurred',
    152:': The request was received but a server timeout occurred',
    200:': The authorization request was approved',
    201:': The issuing bank has questions about the request',
    202:': Expired card or Expiry date entered by the customer does not match the date in the bank records',
    203:': General decline of the card. No other information was provided by the bank',
    204:': Insufficient funds in the account',
    205:': Stolen or lost card',
    207:': Issuing bank unavailable',
    208:': Inactive card or card not authorized for card-not-present transactions',
    210:': The card has reached the credit limit',
  }
  constructor(
    public dialogRef: MatDialogRef<TransactionLogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) {
    if (data.data.length > 0) {
      this.transaction_data = data.data[data.data.length-1].payment_gateway_response;
    }
    this.heading=data.flag?'NI Success log':'NI Failure log';
  }
  ngOnInit() {
  }
}
