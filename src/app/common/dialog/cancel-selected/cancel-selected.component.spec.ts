import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CancelSelectedComponent } from './cancel-selected.component';

describe('CancelSelectedComponent', () => {
  let component: CancelSelectedComponent;
  let fixture: ComponentFixture<CancelSelectedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CancelSelectedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CancelSelectedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
