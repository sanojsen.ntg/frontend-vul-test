import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormBuilder } from '@angular/forms';
import { OrderService } from '../../../common/services/order/order.service';

@Component({
  selector: 'app-poi-dialog',
  templateUrl: './poi-dialog.component.html',
  styleUrls: ['./poi-dialog.component.css']
})
export class PoiDialogComponent implements OnInit {
  
  public poiModel: any = {
    poi_name: '',
    poi_lat: '',
    poi_lng: '',
    poi_is_fav: 0
  };
  constructor(
    formBuilder: FormBuilder, 
    public dialogRef: MatDialogRef<PoiDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) {
      this.poiModel.poi_lat = data.lat;
      this.poiModel.poi_lng = data.lng;
    }

  ngOnInit() {
  }

  public onFilterChange(eve: any) {
    if(this.poiModel.poi_is_fav==1){
      this.poiModel.poi_is_fav = 0
    }
    this.poiModel.poi_is_fav = 1
  }
}
