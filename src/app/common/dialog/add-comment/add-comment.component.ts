import { Component, OnInit, Inject, ViewContainerRef } from '@angular/core';
import { JwtService } from '../../services/api/jwt.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ToastsManager } from 'ng2-toastr';
import { Router } from '@angular/router';
import { OrderService } from "../../../common/services/order/order.service";
import { AccessControlService } from '../../services/access-control/access-control.service';
import { EncDecService } from '../../services/encrypt-decrypt-service/encrypt_decrypt_service';
//import moment = require('moment');

@Component({
  selector: 'app-add-comment',
  templateUrl: './add-comment.component.html',
  styleUrls: ['./add-comment.component.css']
})
export class AddCommentComponent implements OnInit {
  public orderId;
  public dispatcher_id;
  public comments: any = [];
  public commentModel: any = {
    comment: ''
  };
  public errormsg = 'Loading...';
  public isLoading = true;
  public pageNo = 1;
  public count = 0;
  public comment_status = '';
  completeButton: boolean = false;
  companyId: any = [];
  session: string;
  constructor(private _jwtService: JwtService, public dialogRef: MatDialogRef<AddCommentComponent>,
    public toastr: ToastsManager, private router: Router, @Inject(MAT_DIALOG_DATA) public data: any,
    vcr: ViewContainerRef, private _orderService: OrderService,
    public _aclService: AccessControlService,
    private encDecService: EncDecService) {
    this.toastr.setRootViewContainerRef(vcr);
    this.orderId = data.order_id;
    this.comment_status = data.status;
    if (window.localStorage["dispatcherUser"])
      this.dispatcher_id = JSON.parse(window.localStorage["dispatcherUser"]);
    this.companyId.push(localStorage.getItem('user_company'))
    this.session = localStorage.getItem('Sessiontoken');
  }
  ngOnInit() {
    this.aclDisplayService();
    let params = {
      offset: 0,
      limit: 10,
      order_id: this.orderId,
      company_id: this.companyId
    }
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: localStorage.getItem('user_email')
    }
    this._orderService.getComments(enc_data).then(dec => {
      if (dec) {
        if (dec.status == 200) {
          var data: any = this.encDecService.dwt(this.session, dec.data);
          this.comments = data.data;
          this.count = data.count;
          this.isLoading = false;
          if (this.count == 0 || this.comments == []) {
            this.isLoading = true;
            this.errormsg = 'No comments yet.';
          }
          this.pageNo++;
          setTimeout(() => {
            this.updateScroll();
          }, 10)
        }
        else {
          this.isLoading = false;
          this.errormsg = "Something went wrong, Please try again."
        }
      }
    });
  }
  addComment() {
    let params = {
      order_id: this.orderId,
      comments: this.commentModel.comment,
      user_id: this.dispatcher_id._id,
      company_id: this.companyId
    }
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: localStorage.getItem('user_email')
    }
    this._orderService.addComments(enc_data).then(dec => {
      if (dec) {
        if (dec.status == 200) {
          var element = document.getElementById("comment");
          element.scrollTop = 0;
          let pData = {
            user_id: { firstname: this.dispatcher_id.firstname, lastname: this.dispatcher_id.lastname },
            comments: this.commentModel.comment,
            created_at: new Date()
          }
          this.comments.unshift(pData);
          setTimeout(() => {
            this.updateScroll();
          }, 10)
          this.commentModel.comment = '';
          this.count++;
          this.comment_status = "pending";
        }
        else {
          this.toastr.error('Adding comment failed')
        }
      }
    });

  }
  changeStatus() {
    var params = {
      order_id: this.orderId,
      company_id: this.companyId
    }
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: localStorage.getItem('user_email')
    }
    this._orderService.changeCommentStatus(enc_data).then(dec => {
      if (dec.status == 200) {
        this.comment_status = "completed";
        this.dialogRef.close();
      }
      else {
        this.toastr.error('Something went wrong')
      }

    });

  }
  getMoment(date) {
    //return moment(date).format('MMMM Do YYYY, h:mm:ss a');
    return date;
  }
  pagination() {
    let offset = this.pageNo * 10 - 10;
    const param = {
      offset: offset,
      limit: 10,
      order_id: this.orderId,
      company_id: this.companyId
    }
    var encrypted = this.encDecService.nwt(this.session, param);
    var enc_data = {
      data: encrypted,
      email: localStorage.getItem('user_email')
    }
    this._orderService.getComments(enc_data).then(dec => {
      if (dec) {
        if (dec.status == 200) {
          var data: any = this.encDecService.dwt(this.session, dec.data);
          data.data.forEach(element => {
            this.comments.push(element)
          });
          this.isLoading = false;
          this.errormsg = '';
          this.pageNo++;
        }
        else {
          this.isLoading = false;
          this.errormsg = "Something went wrong, Please try again."
        }
      }
    });
  }
  updateScroll() {
    var element = document.getElementById("comment");
    element.scrollTop = element.scrollHeight;
  }
  public aclDisplayService() {
    var params = {
      company_id: this.companyId
    }
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: window.localStorage['user_email']
    }
    this._aclService.getAclUserMenu(enc_data).then((data) => {
      if (data && data.status == 200) {
        data = this.encDecService.dwt(this.session, data.data);
        for (let i = 0; i < data.menu.length; i++) {
          if (data.menu[i] == "Comments - status button")
            this.completeButton = true;
        }
      }
    });
  }
}
