import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RenameSubzoneComponent } from './rename-subzone.component';

describe('RenameSubzoneComponent', () => {
  let component: RenameSubzoneComponent;
  let fixture: ComponentFixture<RenameSubzoneComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RenameSubzoneComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RenameSubzoneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
