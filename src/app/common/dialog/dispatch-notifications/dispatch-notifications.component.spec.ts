import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DispatchNotificationsComponent } from './dispatch-notifications.component';

describe('DispatchNotificationsComponent', () => {
  let component: DispatchNotificationsComponent;
  let fixture: ComponentFixture<DispatchNotificationsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DispatchNotificationsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DispatchNotificationsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
