import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ZoneSavingComponent } from './zone-saving.component';

describe('ZoneSavingComponent', () => {
  let component: ZoneSavingComponent;
  let fixture: ComponentFixture<ZoneSavingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ZoneSavingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ZoneSavingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
