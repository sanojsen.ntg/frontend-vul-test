import { Component, Inject, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { NG_VALIDATORS, AbstractControl, ValidatorFn, FormBuilder, FormGroup, Validator, Validators } from '@angular/forms';
//import { OrderService } from '../../../common/services/order/order.service';

@Component({
  selector: 'app-driverblockreason',
  templateUrl: './driverblockreason.component.html',
  styleUrls: ['./driverblockreason.component.css']
})
export class DriverblockreasonComponent implements OnInit {

  public cancelReasonlModel: any = {
    cancelreason: '',
    cancelText: '',
    gender: ''
  };
  public datareason = [{ "reason": "Pending Payment" }, { "reason": "Report to Office" }, { "reason": "Rule Violation" }, { "reason": "Suspended" }, { "reason": "Terminated" }, { "reason": "Other" }];
  public reasonsItems: any;
  public cancelItems: any;
  public type='';
  constructor(
    formBuilder: FormBuilder,
    public dialogRef: MatDialogRef<DriverblockreasonComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) {
    this.type = data.type;
    this.reasonsItems = this.datareason;
  }

  ngOnInit() {
  }

  reasonPopup(formData): void {

  };

}
