
import { Injectable } from '@angular/core';
import { Headers, Http, RequestOptions } from '@angular/http';
import { environment } from '../../../../environments/environment';
import { ApiService } from '../api/api.service';
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';
import { AccessControlService } from '../access-control/access-control.service';

@Injectable()

export class PromocoderestService {

  private headers = new Headers({ 'content-type': 'application/json' });
  private authUrl = environment.authUrl;
  private promocodeUrl = environment.apiUrl + '/promocode';

  /**
  *
  * @param {Http} http
  * @param {ApiService} _apiService
  */
  constructor(private http: Http, private _apiService: ApiService,
    private accessControl: AccessControlService) { };

	/**
   * Get all Promocodes
   * @param params
   * @return {Promise<TResult2|TResult1>|Promise<TResult|any>|Promise<TResult>|Promise<any>}
   */
  public getPromocodeListing(params) {
    return this._apiService.post(this.promocodeUrl + '/get_promocodes', params)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }
  public getindividualPromo(params) {
    return this._apiService.post(this.promocodeUrl + '/getcustomerspecificpromo', params)
      .map((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }
  public getPromocodeTransactionListing(params) {
    return this._apiService.post(this.promocodeUrl + '/get_promocodes_transactions', params)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }

  /**
   * Add a new promocode
   * @param params 
   */
  public addPromocode(params) {
    return this._apiService.post(this.promocodeUrl + '/add_promocode', params)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }

  /**
   * Get promocode by id
   * @param id 
   */
  public getPromocodeById(id) {
    return this._apiService.post(this.promocodeUrl + '/getById/',id)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }

  /**
   * Update promocode 
   * @param id 
   * @param params 
   */
  public updatePromocodeById(params) {
    return this._apiService.post(this.promocodeUrl + '/updatePromocode/',params)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }

  /**
   * Update delete status of promocode
   * @param id 
   */
  public updateDeletedStatus(id) {
    return this._apiService.post(this.promocodeUrl + '/updateDeleteStatus/',id)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }

  public updateCustomerPromo(params) {
    return this._apiService.post(this.promocodeUrl + '/updateCustomerPromo/', params)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }
  public getPromocodeHistory(params) {
    return this._apiService.post(this.promocodeUrl + '/promocodes_transactions', params)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }
  public changePromoRole(params) {
    return this._apiService.post(this.promocodeUrl + '/change-role', params)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }
}

