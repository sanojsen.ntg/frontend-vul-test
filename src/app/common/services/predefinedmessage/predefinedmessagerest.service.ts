
import { Injectable } from '@angular/core';
import { Headers, Http, RequestOptions } from '@angular/http';
import { environment } from '../../../../environments/environment';
import { ApiService } from '../api/api.service';
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';
import { AccessControlService } from '../access-control/access-control.service';

@Injectable()

export class PredefinedmessagerestService {
  private headers = new Headers({ 'content-type': 'application/json' });
  private authUrl = environment.authUrl;
  private predefinedMessageUrl = environment.apiUrl + '/predefinedmessages';

  /**
  *
  * @param {Http} http
  * @param {ApiService} _apiService
  */
  constructor(private http: Http, private _apiService: ApiService,
    private accessControl: AccessControlService) { };

  /**
   * Save Predefined-message details.
   * @param array data
   * @return {Promise<TResult2|TResult1>|Promise<TResult|any>|Promise<TResult>|Promise<any>}
   */
  public savePredefinedMessages(data) {
    return this._apiService.post(this.predefinedMessageUrl + '/create', data)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }

  /**
   * Get all Predefined-message.
   * @param params
   * @return {Promise<TResult2|TResult1>|Promise<TResult|any>|Promise<TResult>|Promise<any>}
   */
  public getPredefinedMessages(params) {
    return this._apiService.post(this.predefinedMessageUrl, params)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }

  /**
   * Update Predefined-message for corrsponding Predefined-message Id.
   * @param id
   * @param params
   * @return {Promise<TResult2|TResult1>|Promise<TResult|any>|Promise<TResult>|Promise<any>}
   */
  public updatePredefinedMessages(params) {
    return this._apiService.post(this.predefinedMessageUrl + '/update' , params)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }

  /**
   * Get Predefined-message  for corrsponding Predefined-message Id.
   * @param id
   * @return {Promise<TResult2|TResult1>|Promise<TResult|any>|Promise<TResult>|Promise<any>}
   */
  public getPredefinedMessagesById(id) {
    return this._apiService.post(this.predefinedMessageUrl + '/getById/',id)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }

  public deletePredefinedMessagesById(id) {
    return this._apiService.post(this.predefinedMessageUrl + '/delete/',id)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }

  /**
   * Update delete status for corrsponding payment-type Id.
   * @param id
   * @return {Promise<TResult2|TResult1>|Promise<TResult|any>|Promise<TResult>|Promise<any>}
   */
  public updateDeletedStatus(id) {
    return this._apiService.post(this.predefinedMessageUrl + '/updateDeleteStatus/',id)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }

  public searchPredefinedMessages(params) {
    return this._apiService.post(this.predefinedMessageUrl + '/searchPredefinedMessages', params)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }
}

