import { Injectable } from '@angular/core';
import { AccessControlService } from './access-control.service';

@Injectable()
export class AclUrlCheckService {
  public aclCheck;
  constructor(public _aclService: AccessControlService) { }
  public isAuthenticated(roles): boolean {
    let Menu = localStorage.getItem('userMenu');
    let userMenu = JSON.parse(Menu);
    for(let i=0; i<userMenu.length; i++){
      if(userMenu[i] == roles ){
        return true;
      }
    }
    return false;
  }
}
