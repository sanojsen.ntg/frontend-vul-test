import { Injectable } from '@angular/core';
import { Headers, Http, RequestOptions } from '@angular/http';
import { environment } from '../../../../environments/environment';
import { ApiService } from '../api/api.service';
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';
import { ToastsManager } from 'ng2-toastr';
import { JwtService } from '../api/jwt.service';
import { Router } from '@angular/router';
import { GlobalService } from '../global/global.service';


@Injectable()
export class AccessControlService {

  private headers = new Headers({ 'content-type': 'application/json' });
  private getUrl = environment.apiUrl + '/access_control';
  private usersUrl = environment.apiUrl + '/users';
  logout: any=false;

  constructor(private http: Http, private _apiService: ApiService,
    public toastr: ToastsManager,
    private _jwtService: JwtService,
    private router: Router,
    private global: GlobalService) {
    this.global.observableLog.subscribe(item => {
      this.logout = item;
    })
  }
  public aclList1(data) {
    return this._apiService.post(this.getUrl + '/searchRoles', data).toPromise().then((res) => {
      if (this.checkSession(res.status))
        return res
    });
  }
  public aclList(params) {
    return this._apiService.post(this.getUrl + '/searchRoles', params).toPromise().then((res) => {
      if (this.checkSession(res.status))
        return res
    });
  }
  public addAclRole(data) {
    return this._apiService.post(this.getUrl + '/addRole', data)
      .toPromise()
      .then((res) => {
        if (this.checkSession(res.status))
          return res
      });
  }
  public getAclUsers(data) {
    return this._apiService.post(this.getUrl + '/searchUsers', data)
      .toPromise()
      .then((res) => {
        if (this.checkSession(res.status))
          return res
      });
  }
  public getUserlog(data) {
    return this._apiService.post(this.getUrl + '/getUserActivities', data)
      .toPromise()
      .then((res) => {
        if (this.checkSession(res.status))
          return res
      });
  }

  public exportSupport(data) {
    return this._apiService.post(this.getUrl + '/exportSupport', data)
      .toPromise()
      .then((res) => {
        if (this.checkSession(res.status))
          return res
      });
  }

  public addNewAclUsers(data) {
    return this._apiService.post(this.getUrl + '/addUser', data)
      .toPromise()
      .then((res) => {
        if (this.checkSession(res.status))
          return res
      });
  }

  public getAclUsersById(data) {
    return this._apiService.post(this.getUrl + '/userDetails', data)
      .toPromise()
      .then((res) => {
        if (this.checkSession(res.status))
          return res
      });
  }

  public updateAclUsers(data) {
    return this._apiService.post(this.getUrl + '/updateUserDetails', data)
      .toPromise()
      .then((res) => {
        if (this.checkSession(res.status))
          return res
      });
  }

  public deleteAclUser(data) {
    return this._apiService.post(this.getUrl + '/deleteUserAccount', data)
      .toPromise()
      .then((res) => {
        if (this.checkSession(res.status))
          return res
      });
  }
  public getAclUser(data) {
    return this._apiService.post(this.getUrl + '/getUserMenu', data)
      .toPromise()
      .then((res) => {
        if (this.checkSession(res.status))
          return res
      });
  }
  public getAclUserMenu(params) {
    return this._apiService.post(this.getUrl + '/getUserMenu', params)
      .toPromise()
      .then((res) => {
        if (this.checkSession(res.status))
          return res
      });
  }
  public getAccessControlList(data) {
    return this._apiService.post(this.getUrl + '/getFullMenuList', data)
      .toPromise()
      .then((res) => {
        if (this.checkSession(res.status))
          return res
      });
  }

  public addAccessControlList(data) {
    return this._apiService.post(this.getUrl + '/addAccess', data)
      .toPromise()
      .then((res) => {
        if (this.checkSession(res.status))
          return res
      });
  }
  public GetAclFullMenu(data) {
    return this._apiService.post(this.getUrl + '/getFullMenuNameList', data)
      .toPromise()
      .then((res) => {
        if (this.checkSession(res.status))
          return res
      });
  }
  public updatePassword(data) {
    return this._apiService.post(this.getUrl + '/changePassword', data)
      .toPromise()
      .then((res) => {
        if (this.checkSession(res.status))
          return res
      });
  }
  public deleteRole(data) {
    return this._apiService.post(this.getUrl + '/deleteRole', data)
      .toPromise()
      .then((res) => {
        if (this.checkSession(res.status))
          return res
      });
  }
  public dropDownValue(data) {
    return this._apiService.post(this.getUrl + '/get-dropdown-values', data)
      .toPromise()
      .then((res) => {
        if (this.checkSession(res.status))
          return res
      });
  }
  public checkSession(status) {
    if (status == 403 || status == 404) {
      if (!this.logout) {
        this.toastr.error("Session timed out");
        this._jwtService.destroyDispatcherToken();
        this._jwtService.destroyAdminToken();
        this.router.navigate(["/dispatcher/login"]);
        this.global.setLogoutStatus(true);
      }
    }
    else {
      return true;
    }
  }
  
  public activeUsers(params) {
    return this._apiService.post(this.usersUrl + '/active-user', params)
      .toPromise()
      .then((res) => {
        if (this.checkSession(res.status))
          return res
      });
  }
}
