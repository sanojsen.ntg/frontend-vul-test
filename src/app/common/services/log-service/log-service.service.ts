import { Injectable } from '@angular/core';

@Injectable()
export class LogServiceService {
  log(msg: any) {
    console.log(new Date() + ": "  + JSON.stringify(msg));
  }
}
