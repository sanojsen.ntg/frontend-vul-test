import { Injectable } from '@angular/core';
import { Headers, Http, RequestOptions } from '@angular/http';
import { environment } from '../../../../environments/environment';
import { ApiService } from '../api/api.service';
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';
import { AccessControlService } from '../access-control/access-control.service';

@Injectable()
export class SyncTripsRestService {
  private headers = new Headers({ 'content-type': 'application/json' });
  private authUrl = environment.authUrl;
  private syncUrl = environment.apiUrl + '';

  /**
   *
   * @param {Http} http
   * @param {ApiService} _apiService
   */
  constructor(private _http: Http,
    private _apiService: ApiService,
    private accessControl: AccessControlService) { }

  /**
   * Function to get all sync trips
   *
   */
  public get(params) {
    return this._apiService.post(this.syncUrl + '/trip/trips', params)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }


}


