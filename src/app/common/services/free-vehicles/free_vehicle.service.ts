import { Injectable } from '@angular/core';
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';
import { ApiService } from '../api/api.service';
import { FreeVehicleRestService } from '../free-vehicles/freevehiclerest.service';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable, Subject } from 'rxjs';

@Injectable()
export class FreeVehicleService {
  deviceList=[];
  orderList=[];
  timedOutList=[];
  notificationList=[];
  overSpeedList=[];
  orderId='';
  count={devices:0,scheduled:0,timedout:0,free:0,overspeed:0};
  observableDeviceList;
  observableOrderList;
  observableTimedOutList;
  observableCount;
  observableNotificationList;
  observableOverSpeedList;
  observableOrderId;
  private subject = new Subject<any>();

  constructor(private freeVehicleRestService: FreeVehicleRestService) {
    this.deviceList = new Array();
    this.observableDeviceList = new BehaviorSubject(this.deviceList);
    this.observableOrderList = new BehaviorSubject(this.orderList);
    this.observableTimedOutList = new BehaviorSubject(this.timedOutList);
    this.observableCount = new BehaviorSubject(this.count);
    this.observableNotificationList = new BehaviorSubject(this.notificationList);
    this.observableOverSpeedList = new BehaviorSubject(this.overSpeedList);
    this.observableOrderId = new BehaviorSubject(this.orderId);
  }
  public getFreeVehicle(data) {
    return this.freeVehicleRestService.getFreeVehicle(data)
      .then((res) => res);
  }
  public getAlert(data) {
    return this.freeVehicleRestService.getAlert(data)
      .then((res) => res);
  }
  public getInactiveVehicles(data){
    return this.freeVehicleRestService.inactiveVehicle(data)
      .then((res) => res);
  }

  setDevices(devices) {
    this.deviceList=devices;
    this.count.devices=devices.vehicleStatus.length;
    this.eventChange();
    }
  eventChange() {
    this.observableDeviceList.next(this.deviceList);
    this.observableCount.next(this.count);
  }
  setScheduledTrips(trips) {
    this.orderList=trips;
    this.count.scheduled=trips.Scheduled.length;
    this.eventChange2();
    }
  eventChange2() {
    this.observableOrderList.next(this.orderList);
    this.observableCount.next(this.count);
  }
  setTimedOutOrders(orders){
    this.timedOutList=orders;
    this.count.timedout=orders.timed_out.length;
    this.eventChange3();
  }
  eventChange3() {
    this.observableTimedOutList.next(this.timedOutList);
    this.observableCount.next(this.count);
  }
  setFreeVehicle(count){
    this.count.free=count;
  }
  setNotification(orders){
    this.notificationList=orders;
    this.eventChange4();
  }
  eventChange4() {
    this.observableNotificationList.next(this.notificationList);
  }
  setOverSpeed(orders){
    this.overSpeedList=orders;
    this.count.overspeed=orders.length;
    this.eventChange5();
  }
  eventChange5() {
    this.observableOverSpeedList.next(this.overSpeedList);
  }
  setOrderId(id){
    this.orderId=id;
    this.eventChange6();
  }
  eventChange6() {
    this.observableOrderId.next(this.orderId);
  }
  findnoOccvehicle(car) {
    this.subject.next(car);
  }

  getMessage(): Observable<any> {
      return this.subject.asObservable();
  }
}



