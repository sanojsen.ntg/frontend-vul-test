/**
 * Created by sandhuharjodh2561 on 8/10/2017.
 */
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';
import {
  CanActivate,
  Router,
  ActivatedRouteSnapshot,
  RouterStateSnapshot
} from '@angular/router';

import { JwtService } from '../api/jwt.service';
@Injectable()
export class AdminService implements CanActivate {

  /**
   * Constructor for admin services
   * @param {JwtService} _jwtService
   * @param {Router} router
   */
  constructor(private _jwtService: JwtService, private router: Router) {
  };

  /**
   * Activate route
   * @param route
   * @param state
   * @returns {boolean}
   */
  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    let url: string = state.url;
    return this.verifyAdminLogin(url);
  }

  /**
   * Activate child route
   * @param route
   * @param state
   * @returns {boolean}
   */
  canActivateChild(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    let type = route.data["type"] as Array<string>;
    let url: string = state.url;
    return this.verifyRole(url, type);
  }

  /**
   * Get Current Admin User
   * @returns {any}
   */
  getCurrentAdminUser() {
    if (this._jwtService.getToken()) {
      return JSON.parse(this._jwtService.getUser());
    } else {
      return false;
    }
  }

  /**
   * Verify if admin user is logged in or not
   * @param url
   * @returns {boolean}
   */
  verifyAdminLogin(url: string): boolean {
    if (this._jwtService.getToken()) {
      if (window.localStorage['adminUser']) {
        return true;
      }else if (window.localStorage['dispatcherUser']){
        let Menu = localStorage.getItem('userMenu');
        let userMenu = JSON.parse(Menu);
        for(let i=0; i<userMenu.length; i++){
          if(userMenu[i]=="Admin Login"){
            return true;
          }
        }
      }
    }
    this._jwtService.destroyAdminToken();
    this.router.navigate(['/admin/login']);
    return false;
  }


  /**
   * Verify if admin user is logged in or not
   * @param url
   * @returns {boolean}
   */
  verifyRole(url: string, type): boolean {

    let userdata = JSON.parse(window.localStorage['adminUser']);
    if (window.localStorage['adminUser']) {
      if (type[0] == "orders") {
        if (userdata.role == "1" || userdata.role == "3") {
          return true;
        } else {
          this.router.navigate(['/admin/dashboard']);
          return false;
        }
      } else if (type[0] == "administration") {
        if (userdata.role == "1" || userdata.role == "3") {
          return true;

        } else {
          this.router.navigate(['/admin/dashboard']);
          return false;
        }
      } else if (type[0] == "fleet") {
        if (userdata.role == "1" || userdata.role == "3" || userdata.role == "4") {
          return true;
        } else {
          this.router.navigate(['/admin/dashboard']);
          return false;
        }
      } else if (type[0] == "shift") {
        if (userdata.role == "1" || userdata.role == "3") {
          return true;
        } else {
          this.router.navigate(['/admin/dashboard']);
          return false;
        }
      } else if (type[0] == "customer") {
        if (userdata.role == "1" || userdata.role == "3") {
          return true;
        } else {
          this.router.navigate(['/admin/dashboard']);
          return false;
        }
      } else {
        return true;
      }
    }
    return true;


  }



  /**
   * Log out service for admin
   */
  adminLogOut() {
    localStorage.clear();
    this._jwtService.destroyAdminToken();
    this.router.navigate(['/admin/login']);
  }


}
