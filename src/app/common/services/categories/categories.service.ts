import { Injectable } from '@angular/core';
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';

import { ApiService } from '../api/api.service';
import { CategoriesrestService } from './categoriesrest.service';

@Injectable()
export class Categories {
  constructor(private _categoriesrestService: CategoriesrestService) {
  }
  public get(params) {
    return this._categoriesrestService.get(params)
      .map((res) => res);
  }
  public searchCategoyByName(params) {
    return this._categoriesrestService.searchCategoyByName(params)
      .map((res) => res);
  }

  public getCategories(params) {
    return this._categoriesrestService.getCategories(params)
      .map((res) => res);
  }

  public updateCategoryForDeletion(id) {
    return this._categoriesrestService.updateCategoriesForDeletion(id)
      .then((res) => res);
  }


 /**
   * Function to save categories
   * @returns {Observable<any>}
   */
  public addCategories(data) {
    return this._categoriesrestService.saveCategories(data)
      .then((data) => data);
  }

  public searchData(params) {
    return this._categoriesrestService.search(params)
      .map((res) => res);
  }


  public updateServiceForDeletion(id) {
    return this._categoriesrestService.updateServiceForDeletion(id)
      .then((res) => res);
  }

  public getById(id) {
    return this._categoriesrestService.getCategoryById(id)
      .then((res) => res);
  }

  public getSubCategoriesById(params) {
    return this._categoriesrestService.getSubCategoryForCategory( params)
      .then((data) => data);
  }

  public getSubCategoryById(id) {
    return this._categoriesrestService.getSubCategoryById(id)
      .then((data) => data);
  }

  public updateCategories(params) {
  
    return this._categoriesrestService.updateCategory(params)
      .then((data) => data);
  }
}
