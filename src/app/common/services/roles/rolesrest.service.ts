
import { Injectable } from '@angular/core';
import { Headers, Http, RequestOptions } from '@angular/http';
import { environment } from '../../../../environments/environment';
import { ApiService } from '../api/api.service';
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';
import { AccessControlService } from '../access-control/access-control.service';

@Injectable()

export class RolesrestService {

  private headers = new Headers({ 'content-type': 'application/json' });
  private authUrl = environment.authUrl;
  private rolesUrl = environment.apiUrl + '/roles';

  /**
  *
  * @param {Http} http
  * @param {ApiService} _apiService
  */
  constructor(private http: Http, private _apiService: ApiService,
    private accessControl: AccessControlService) { };

	/**
   * Get all users
   * @param params
   * @return {Promise<TResult2|TResult1>|Promise<TResult|any>|Promise<TResult>|Promise<any>}
   */
  public getUsersListing(params) {
    return this._apiService.post(this.rolesUrl + '/getListAllRolesUsers', params)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }

  /**
   * Update delete status of users
   * @param id 
   */
  public updateDeletedStatus(id) {
    return this._apiService.post(this.rolesUrl + '/updateDeleteStatus/', id)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }

  /**
  * Add a new promocode
  * @param params 
  */
  public addUsers(params) {
    return this._apiService.post(this.rolesUrl + '/add_users', params)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }

  /**
   * Get promocode by id
   * @param id 
   */
  public getUserById(id) {
    return this._apiService.post(this.rolesUrl + '/getById/',id)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }

  /**
   * Update User 
   * @param id 
   * @param params 
   */
  public updateUserById(params) {
    return this._apiService.post(this.rolesUrl + '/updateUser/', params)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }



}

