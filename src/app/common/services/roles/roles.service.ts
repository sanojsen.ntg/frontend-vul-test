import { Injectable } from '@angular/core';
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';
import { ApiService } from '../api/api.service';
import { RolesrestService } from './rolesrest.service';

@Injectable()

export class RolesService {
  constructor(  private _rolesrestService: RolesrestService ) { }

  /**
   * Get all promocodes
   * @param data 
   */
  public getUsersListing(data) {
      return this._rolesrestService.getUsersListing(data)
      .then((res) => res);
  }

  /**
   * Update delete status of a promocode
   * @param id 
   */
  public updateDeletedStatus(id) {
    return this._rolesrestService.updateDeletedStatus(id)
      .then((res) => res);
  }

  /**
   * Add New promocode
   * @param data 
   */
  public addUsers(data) {
    return this._rolesrestService.addUsers(data)
    .then((res) => res);
  }

  /**
   * Get a user by id
   * @param id 
   */
  public getUserById (id) {
    return this._rolesrestService.getUserById(id)
    .then((res) => res);
  }

  /**
   * Update a role by id
   * @param id 
   * @param data 
   */
  public updateuserById (params) {
    return this._rolesrestService.updateUserById(params)
    .then((res) => res);
  }

  /**
   * Update a promocode by id
   * @param id 
   * @param data 
   */
  public updateUserById (params) {
    return this._rolesrestService.updateUserById(params)
    .then((res) => res);
  }
}




