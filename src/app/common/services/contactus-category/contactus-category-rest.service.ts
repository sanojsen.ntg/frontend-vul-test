import { Injectable } from '@angular/core';
import { Headers, Http, RequestOptions} from '@angular/http';
import { environment } from '../../../../environments/environment';
import { ApiService } from '../api/api.service';
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';
import { AccessControlService } from './../access-control/access-control.service';

@Injectable()

export class ContactusCategoryRestService {

  private headers = new Headers({'content-type': 'application/json'});
  private ContactusCategoryUrl = environment.apiUrl + '/contactus_categories';

  constructor(private http: Http, private _apiService: ApiService,
    private accessControl: AccessControlService) {  }

  /**
   * Get all contactus category
   * @param params 
   */
  public getAllContactusCategory(params) {
    return this._apiService.post(this.ContactusCategoryUrl , params )
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }

  /**
   * Add new contactus category
   * @param params 
   */
  public AddNewContactusCategory(params) {
    return this._apiService.post(this.ContactusCategoryUrl + '/addContactusCategory', params )
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }

  /**
   * Udate Contactus Category by id
   * @param id 
   * @param params 
   */
  public UpdateContactusCategory(params) {
    return this._apiService.post(this.ContactusCategoryUrl + '/updateContactusCategory/', params )
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }

  /**
   * Udate contactus category by id
   * @param id 
   * @param params 
   */
  public getContactusCategoryById(id) {
    return this._apiService.post(this.ContactusCategoryUrl + '/getContactusCategoryById/', id)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }

  /**
   * Update delete status
   * @param id 
   * @param params 
   */
  public updateDeleteStatus(id) {
    return this._apiService.post(this.ContactusCategoryUrl + '/updateDeleteStatus/', id)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }
 
}
