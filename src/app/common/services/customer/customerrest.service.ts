import { Injectable } from '@angular/core';
import { Headers, Http, RequestOptions } from '@angular/http';
import { environment } from '../../../../environments/environment';
import { ApiService } from '../api/api.service';
import 'rxjs/add/operator/toPromise';
import { AccessControlService } from './../access-control/access-control.service';


@Injectable()
export class CustomerRestService {
  private headers = new Headers({ 'content-type': 'application/json' });
  private authUrl = environment.authUrl;
  private customerUrl = environment.apiUrl + '/customers';
  private customerRatingUrl = environment.apiUrl + '/customer_ratings'
  private dispatcherUrl = environment.apiUrl + '/users/getDispatcherUSer';
  private orderUrl = environment.apiUrl + '/order';
  private notification = environment.apiUrl + '/app-notifications/send-notifications';
  private notificationUrl = environment.apiUrl + '/app-notifications';

  /**
   *
   * @param {Http} http
   * @param {ApiService} _apiService
   */
  constructor(private http: Http, private _apiService: ApiService,
    private accessControl: AccessControlService) {
  }

  /**
   * Function to get all customer
   *
   */

  public get(params) {
    return this._apiService.post(this.customerUrl, params)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }

  public fetch(params) {
    return this._apiService.post(this.customerUrl + '/fetchCustomers', params)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }
  public fetchCustomerGroups(params) {
    return this._apiService.post(this.customerUrl + '/getcustomergroup', params)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }

  /**
  * Function to search customer
  */

  public search(params) {
    return this._apiService.post(this.customerUrl + '/search', params)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }

  public ChangeBlockStatus(params) {
    return this._apiService.post(this.customerUrl + '/updateBlockedStatus/', params)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }

  /**
  * Function to get Dispatcher
  *
  */

  public getDispatcherForDropDown(params) {
    return this._apiService.post(this.dispatcherUrl,params)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }

  public getCustomerByName(params) {
    return this._apiService.post(this.customerUrl + '/getCustomerByName', params)
      .map((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }

  public getCustomerByNameFrOrder(params) {
    return this._apiService.post(this.customerUrl + '/getCustomerByName', params)
      .map((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }

  public getPhoneNumber(params) {
    return this._apiService.post(this.customerUrl, params)
      .map((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });

  }

  public getCustomerByPhone(params) {
    return this._apiService.post(this.customerUrl + '/getCustomerByPhone', params)
      .map((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }
  public getGuestPhone(params){
    return this._apiService.post(this.customerUrl + '/get_guest_phone', params)
    .map((res) => {
      if (this.accessControl.checkSession(res.status))
        return res
    });
  }

  public getCustomerAddrress(params) {
    return this._apiService.post(this.customerUrl + '/getCustomerAddress', params)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });

  }
  public deleteAddress(params) {

    return this._apiService.post(this.customerUrl + '/deleteCustomerAddress', params)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });


  }
  public getCustomerMessages(params) {
    return this._apiService.post(this.customerUrl + '/getCustomerMessages', params)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });

  }

  public getCustomerById(id) {
    return this._apiService.post(this.customerUrl + '/customer_details', id)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }

  public getCustomerTrips(params) {
    return this._apiService.post(this.orderUrl + '/get_customer_orders', params).toPromise().then((res) => {
      if (this.accessControl.checkSession(res.status))
        return res
    });
  }

  public notificationServ(params) {
    return this._apiService.post(this.notification, params).toPromise().then((res) => {
      if (this.accessControl.checkSession(res.status))
        return res
    });
  }

  public blockCustomer(params) {
    return this._apiService.post(this.customerUrl + '/block-device', params)
      .toPromise()
      .then((res) => res);
  }

  public notificationEmail(params){
    return this._apiService.post(this.notificationUrl + '/send-emails', params).toPromise().then((res)=>res);
  }

  public notificationSms(params){
    return this._apiService.post(this.notificationUrl + '/send-sms', params).toPromise().then((res)=>res);
  }
  public fetchCount(params) {
    return this._apiService.post(this.customerUrl + '/fetchCustomers-count', params)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }
  public dispatcherSms(params){
    return this._apiService.post(this.customerUrl + '/send-sms', params)
    .toPromise()
    .then((res) => {
      if (this.accessControl.checkSession(res.status))
        return res
    });
  }
  public appDeviceVersion(params) {
    return this._apiService.post(this.notificationUrl  + '/os-versions', params)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }

  public appNotification(params) {
    return this._apiService.post(this.notificationUrl  + '/add-notification', params)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }
  public updateCustomerDetails(params) {
    return this._apiService.post(this.customerUrl  + '/update-customer', params)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }
  public dispatcherCustomers(params) {
    return this._apiService.post(this.customerUrl  + '/dispatcher-customers', params)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }
  public zendeskTickets(params) {
    return this._apiService.post(this.customerUrl  + '/zendesk', params)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }
  public zendeskComments(params) {
    return this._apiService.post(this.customerUrl  + '/zendesk-comments', params)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }
  public zendeskTags(params) {
    return this._apiService.post(this.customerUrl  + '/zendesk-tags', params)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }
  public zendeskStatus(params) {
    return this._apiService.post(this.customerUrl  + '/zendesk-status', params)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }
  public getOrderComments(params) {
    return this._apiService.post(this.customerRatingUrl  + '/comments', params)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }

  public zendeskDetails(params) {
    return this._apiService.post(this.customerUrl  + '/zendesk-details', params)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }
  public zendeskAdd(params) {
    return this._apiService.post(this.customerUrl  + '/zendesk-add', params)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }
  public zendeskList(params) {
    return this._apiService.post(this.customerUrl  + '/zendesk-list', params)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }
  public zendeskId(params) {
    return this._apiService.post(this.customerUrl  + '/zendesk-id', params)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }
  public zendeskEdit(params) {
    return this._apiService.post(this.customerUrl  + '/zendesk-edit', params)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }
  public zendeskDelete(params) {
    return this._apiService.post(this.customerUrl  + '/zendesk-delete', params)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }
  
}
