import { Injectable } from '@angular/core';
import { environment } from '../../../../environments/environment';
import { ApiService } from '../api/api.service';
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';
import { AccessControlService } from './../access-control/access-control.service';


@Injectable()
export class DevicelocationRestService {

  private headers = new Headers({'content-type': 'application/json'});
  private authUrl = environment.authUrl;
  private devicelocationUrl = environment.apiUrl + '/devicelocation';

  constructor( private _apiService: ApiService,
    private accessControl: AccessControlService) { }

  /**
   * Function to get all customer
   *
   */
  public getDeviceLocations (params) {
    return this._apiService.post( this.devicelocationUrl + '/getdevicelocation',params  )
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }
  

   /**
   * Function to get all getLoginDeviceLocation
   *
   */
  public getLoginDeviceLocation (params) {
    return this._apiService.post( this.devicelocationUrl + '/getLoginDeviceLocation' ,params )
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }

  public getFilteredDataStatus(params) {
    return this._apiService.post(this.devicelocationUrl + '/devicesForDifferentStatus', params )
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }

  public getVehicleLastLocation(params) {
    return this._apiService.post(this.devicelocationUrl + '/getdevicelocationforvehicle', params)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }

  public getdatabyvehicleid(params) {
    return this._apiService.post(this.devicelocationUrl + '/getdatabyvehicleid' , params )
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });

  }
  public getdatabyvehicleidNew(params) {
    return this._apiService.post(this.devicelocationUrl + '/getdatabyvehicleidnew' , params )
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });

  }
  public vehicleTrackLocations(params) {
    return this._apiService.post(this.devicelocationUrl + '/getVehicleTrackInfo', params)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }
  public getMapData(params) {
    return this._apiService.post(this.devicelocationUrl + '/devicesForDifferentStatusMap', params )
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }

}


