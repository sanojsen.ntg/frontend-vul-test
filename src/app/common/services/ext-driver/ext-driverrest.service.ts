import { Injectable } from '@angular/core';
import { environment } from '../../../../environments/environment';
import { ApiService } from '../api/api.service';
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';
import { AccessControlService } from '../access-control/access-control.service';

@Injectable()
export class ExtDriverRestService {
    private extDriver = environment.apiUrl + '/driver-onboard';
    constructor(public _apiService: ApiService, public accessControl: AccessControlService) { }

    public extDriverList(params) {
        return this._apiService.post(this.extDriver + '/list', params)
            .toPromise()
            .then((res) => {
                if (this.accessControl.checkSession(res.status))
                    return res
            });
    }

    public extDriverById(params) {
        return this._apiService.post(this.extDriver + '/detail', params)
            .toPromise()
            .then((res) => {
                if (this.accessControl.checkSession(res.status))
                    return res
            });
    }

    public extDriverUpdate(params) {
        return this._apiService.post(this.extDriver + '/update', params)
            .toPromise()
            .then((res) => {
                if (this.accessControl.checkSession(res.status))
                    return res
            });
    }

    public createDriver(params) {
        return this._apiService.post(this.extDriver + '/create-driver', params)
            .toPromise()
            .then((res) => {
                if (this.accessControl.checkSession(res.status))
                    return res
            });
    }
    public createVehicle(params) {
        return this._apiService.post(this.extDriver + '/create-vehicle', params)
            .toPromise()
            .then((res) => {
                if (this.accessControl.checkSession(res.status))
                    return res
            });
    }
    public bankList(params) {
        return this._apiService.post(this.extDriver + '/bank-list', params)
            .toPromise()
            .then((res) => {
                if (this.accessControl.checkSession(res.status))
                    return res
            });
    }
    public updatePassword(params) {
        return this._apiService.post(this.extDriver + '/change-password', params)
            .toPromise()
            .then((res) => {
                if (this.accessControl.checkSession(res.status))
                    return res
            });
    }
    public extDriverBankUpdate(params) {
        return this._apiService.post(this.extDriver + '/update-iban', params)
            .toPromise()
            .then((res) => {
                if (this.accessControl.checkSession(res.status))
                    return res
            });
    }
}
