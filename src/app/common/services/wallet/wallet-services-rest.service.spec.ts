import { TestBed, inject } from '@angular/core/testing';

import { WalletServicesRestService } from './wallet-services-rest.service';

describe('WalletServicesRestService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [WalletServicesRestService]
    });
  });

  it('should be created', inject([WalletServicesRestService], (service: WalletServicesRestService) => {
    expect(service).toBeTruthy();
  }));
});
