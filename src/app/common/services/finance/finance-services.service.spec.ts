import { TestBed, inject } from '@angular/core/testing';

import { FinanceServicesService } from './finance-services.service';

describe('FinanceServicesService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [FinanceServicesService]
    });
  });

  it('should be created', inject([FinanceServicesService], (service: FinanceServicesService) => {
    expect(service).toBeTruthy();
  }));
});
