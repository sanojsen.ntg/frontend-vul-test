import { Injectable } from '@angular/core';
import { Headers, Http, RequestOptions } from '@angular/http';
import { environment } from '../../../../environments/environment';
import { ApiService } from '../api/api.service';
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';
import { AccessControlService } from '../access-control/access-control.service';
@Injectable()
export class FinanceServicesRestService {
  private headers = new Headers({ 'content-type': 'application/json' });
  private financeUrl = environment.apiUrl + '/finance';
  constructor(private http: Http, private _apiService: ApiService,
    private accessControl: AccessControlService) {
  }
  public getLastReco(params) {
    return this._apiService.post(this.financeUrl + '/getlastreco', params)
      .map((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }
  public getTripfromReco(params) {
    return this._apiService.post(this.financeUrl + '/getTripDetails', params)
      .map((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }
  public getRecoDetails(params) {
    return this._apiService.post(this.financeUrl + '/getRecoDetails', params)
      .map((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }
  public getRecoCount(params) {
    return this._apiService.post(this.financeUrl + '/getRecoCount', params)
      .map((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }
  public searchReco(params) {
    return this._apiService.post(this.financeUrl + '/searchReco', params)
      .map((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }


}