import { TestBed, inject } from '@angular/core/testing';

import { FinanceServicesRestService } from './finance-services-rest.service';

describe('FinanceServicesRestService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [FinanceServicesRestService]
    });
  });

  it('should be created', inject([FinanceServicesRestService], (service: FinanceServicesRestService) => {
    expect(service).toBeTruthy();
  }));
});
