import { Injectable } from '@angular/core';
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';
import { ApiService } from '../api/api.service';
import { BlockedDeviceRestService } from './blocked-devicesrest.service';

@Injectable()
export class BlockedDeviceService {

  constructor(private _blockedDevicesRestService: BlockedDeviceRestService) { }
  // params['deviceid'] = keyword;
  public getBlockedDevicesListing(params) {
    return this._blockedDevicesRestService.getBlockedDevices(params)
      .then((res) => res);
  }
  //  params['deviceid'] = keyword;
  public getBlockedDevicesIdListing(params) {
    return this._blockedDevicesRestService.getBlockedDevicesIdForSearch(params)
      .then((res) => res);
  }

  public delete(id) {
    return this._blockedDevicesRestService.delete(id)
      .then((res) => res);
  }

  public addBlockDevices(data) {
    return this._blockedDevicesRestService.addBlockedDevice(data)
      .then((res) => res);
  }

  public getBlockDevicesById(id) {
    return this._blockedDevicesRestService.getBlockedDeviceById(id)
      .then((res) => res);
  }

  public updateBlockedDevice(params) {
    return this._blockedDevicesRestService.updateDevice(params)
      .then((res) => res);
  }

  public updateBlockedDeviceForDeletion(id) {
    return this._blockedDevicesRestService.updateforDeletion(id)
      .then((res) => res);
  }

  public getBlockDevicesForDeletion(id) {
    return this._blockedDevicesRestService.getBlockedDevicesforDeletion(id)
      .then((res) => res);
  }

}