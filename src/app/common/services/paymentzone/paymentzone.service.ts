import { Injectable } from '@angular/core';
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';
import { ApiService } from '../api/api.service';
import { PaymentZoneRestService } from './paymentzonerest.service';

@Injectable()
export class PaymentZoneService {
  constructor(private _paymentZoneRestService: PaymentZoneRestService) { }

  public savePaymentType(data) {
    return this._paymentZoneRestService.savePaymentZone(data)
      .then((res) => res);
  }

  public getPaymentZones(params) {
    return this._paymentZoneRestService.getPaymentZone(params)
      .then((res) => res);
  }

  public updateDeletedStatus(id) {
    return this._paymentZoneRestService.updateDeletedStatus(id)
      .then((res) => res);
  }

   public getPaymentZoneById(id) {
    return this._paymentZoneRestService.getPaymentZoneById(id)
      .then((res) => res);
  }
}



