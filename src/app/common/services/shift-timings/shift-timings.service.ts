import { Injectable } from '@angular/core';
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';
import { ApiService } from '../api/api.service';
import { ShiftTimingsrestService } from './shift-timingsrest.service';

@Injectable()
export class ShiftTimingsService {
  constructor(  private _shiftTimingsrestService: ShiftTimingsrestService ) { }

  /**
   * get list of shift timings
   * @param data
   * @returns {Promise<any>}
   */
  public getShiftTiming (data) {
    return this._shiftTimingsrestService.getShiftTiming(data)
      .then((res) => res);
  }

  /**
   * get shift by id
   * @param data
   * @returns {Promise<any>}
   */
  public getShiftTimingById (data) {
    return this._shiftTimingsrestService.getShiftTimingById(data)
      .then((res) => res);
  }

  /**
   * save new shift timing
   * @param data
   * @returns {Promise<any>}
   */
  public saveShiftTiming (data) {
    return this._shiftTimingsrestService.saveShiftTiming(data)
      .then((res) => res);
  }

  /**
   * update Shift Timings
   */
  public updateShiftTimings(params) {
    return this._shiftTimingsrestService.updateShiftTiming(params)
      .then((res) => res);

  }

  /**
   * Delete Shift timings
   * @param shift_id
   */
  public deleteShiftTimings(shift_id) {
    return this._shiftTimingsrestService.deleteShiftTimings(shift_id)
      .then((res) => res);

  }
}
