import { Injectable } from '@angular/core';
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';
import { ApiService } from '../api/api.service';
import { CoveragearearestService } from './coveragearearest.service';

@Injectable()
export class CoverageareaService {
  constructor(private _coveragearearestservice: CoveragearearestService) { }

  public saveCoverageArea(data) {
    return this._coveragearearestservice.saveCoverageArea(data)
      .then((res) => res);
  }

  public getCoverageAreas(params) {
    return this._coveragearearestservice.getCoverageArea(params)
      .then((res) => res);
  }

  public updateDeletedStatus(id) {
    return this._coveragearearestservice.updateDeletedStatus(id)
      .then((res) => res);
  }

  public getCoverageAreaById(id) {
    return this._coveragearearestservice.getCoverageAreaById(id)
      .then((res) => res);
  }
}



