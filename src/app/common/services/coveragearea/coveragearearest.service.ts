import { Injectable } from '@angular/core';
import { Headers, Http, RequestOptions } from '@angular/http';
import { environment } from '../../../../environments/environment';
import { ApiService } from '../api/api.service';
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';
import { AccessControlService } from './../access-control/access-control.service';

@Injectable()
export class CoveragearearestService {
  private headers = new Headers({ 'content-type': 'application/json' });
  private authUrl = environment.authUrl;
  private coverageareaUrl = environment.apiUrl + '/coveragearea';

  /**
  *
  * @param {Http} http
  * @param {ApiService} _apiService
  */
  constructor(private http: Http, private _apiService: ApiService,
    private accessControl: AccessControlService) { };

  /**
   * Save Payment Zone details.
   * @param array data
   * @return {Promise<TResult2|TResult1>|Promise<TResult|any>|Promise<TResult>|Promise<any>}
   */
  public saveCoverageArea(data) {
    return this._apiService.post(this.coverageareaUrl + '/create', data)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }

  /**
   * Get all Payment Zone.
   * @param params
   * @return {Promise<TResult2|TResult1>|Promise<TResult|any>|Promise<TResult>|Promise<any>}
   */
  public getCoverageArea(params) {
    return this._apiService.post(this.coverageareaUrl, params)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }

  /**
   * Update delete status for corrsponding payment-Zone Id.
   * @param id
   * @return {Promise<TResult2|TResult1>|Promise<TResult|any>|Promise<TResult>|Promise<any>}
   */
  public updateDeletedStatus(id) {
    return this._apiService.post(this.coverageareaUrl + '/updateDeleteStatus/' , id)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }
  /**
  * Get payment-type  for corrsponding payment-type Id.
  * @param id
  * @return {Promise<TResult2|TResult1>|Promise<TResult|any>|Promise<TResult>|Promise<any>}
  */
  public getCoverageAreaById(id) {
    return this._apiService.post(this.coverageareaUrl + '/get/' , id)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }
}
