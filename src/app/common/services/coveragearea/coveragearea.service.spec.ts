import { TestBed, inject } from '@angular/core/testing';

import { CoverageareaService } from './coveragearea.service';

describe('CoverageareaService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CoverageareaService]
    });
  });

  it('should be created', inject([CoverageareaService], (service: CoverageareaService) => {
    expect(service).toBeTruthy();
  }));
});
