import { Injectable } from '@angular/core';
import { Headers, Http, RequestOptions } from '@angular/http';
import { environment } from '../../../../environments/environment';
import { ApiService } from '../api/api.service';
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';
import { AccessControlService } from './../access-control/access-control.service';

@Injectable()

export class DeviceRestService {
  private headers = new Headers({ 'content-type': 'application/json' });
  private deviceUrl = environment.apiUrl + '/devices';
  private deviceDeleteUrl = environment.apiUrl + '/devices/delete';
  private devicesUrl = environment.apiUrl + '/devices/add';

  constructor(private http: Http, private _apiService: ApiService,
    private accessControl: AccessControlService) { }

  public getDevices(params) {
    return this._apiService.post(this.deviceUrl, params)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });

  }

  // public getDeviceBlocked(params) {
  //   return this._apiService.post(this.deviceUrl + '/blocked' , params )
  //     .toPromise()
  //     .then((res) => res);
  // }

  public delete(id) {
    return this._apiService.post(this.deviceDeleteUrl + '/' , id)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }

  public updateDriver( params) {
    return this._apiService.post(this.deviceUrl + '/updateDrivers/' , params)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }

  public updateVehicle(params) {
    return this._apiService.post(this.deviceUrl + '/updatevehicles', params)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }

  public addDevice(params) {
    return this._apiService.post(this.devicesUrl, params, { headers: this.headers })
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }

  public updateDevice(params) {
    return this._apiService.post(this.deviceUrl + '/updateDevice/' , params)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }

  /**
  * Function to get Device based on status
  *
  */
  public getDeviceStatus(params) {
    return this._apiService.post(this.deviceUrl + '/getStatus', params)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }
  public searchForTempshift(params) {
    return this._apiService.post(this.deviceUrl + '/getTempshiftdetails', params)
      .map((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }
  public deleteTempshift(params) {
    return this._apiService.post(this.deviceUrl + '/deleteTempshiftdetails', params)
      .map((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }
  public addTempshift(params) {
    return this._apiService.post(this.deviceUrl + '/addTempshiftdetails', params)
      .map((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }
  public updateDeviceStatus(id) {
    return this._apiService.post(this.deviceUrl + '/updateDeviceStatus/' , id)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }

  public undoDeviceStatus(id) {
    return this._apiService.post(this.deviceUrl + '/undoDeviceStatus/' , id)
      .toPromise()
      .then((res) =>{
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }

  public updateForDeletion(id) {
    return this._apiService.post(this.deviceUrl + '/updateStatus/' , id)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }

  public getDeviceById(id) {
    return this._apiService.post(this.deviceUrl + '/details/',id)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }
  public ChangeBlockStatus(params) {
    return this._apiService.post(this.deviceUrl + '/updateBlockedStatus/', params)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }

  public searchForDevices(params) {
    return this._apiService.post(this.deviceUrl + '/searchForDevices', params)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }

  // public searching (params) {
  //   return this._apiService.post(this.deviceUrl + '/multipleSearch', params)
  //     .toPromise()
  //     .then((response) => response);
  // }
  public deleteDeviceArray(params) {
    return this._apiService.post(this.deviceUrl + '/deleteDeviceArray', params)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }

}
