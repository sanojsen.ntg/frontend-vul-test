import { Injectable } from '@angular/core';
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';
import { ApiService } from '../api/api.service';
import { DeviceRestService } from './devicesrest.service';

@Injectable()
export class DeviceService {

  constructor(private _devicesRestService: DeviceRestService) { }

  // params['unique_device_id'] = keyword;
  public getDevicesListing( params) {
    return this._devicesRestService.getDevices(params)
      .then((res) => res);
  }

  //  public getStatusBasedDevice(request) {
  //   return this._devicesRestService.getDeviceBlocked(request)
  //     .then((data) => data);
  // }

  public updateDriver(params) {
    return this._devicesRestService.updateDriver(params)
      .then((res) => res);
  }

  public updateVehicle(params) {
    return this._devicesRestService.updateVehicle(params)
      .then((res) => res);
  }

  public delete(id) {
    return this._devicesRestService.delete(id)
      .then((res) => res);
  }

  public addDevices(data) {
    return this._devicesRestService.addDevice(data)
      .then((res) => res);
  }

  public getDevicesById(id) {
    return this._devicesRestService.getDeviceById(id)
      .then((res) => res);
  }
  public searchForTempshift(params) {
    return this._devicesRestService.searchForTempshift(params)
      .map((res) => res);
  }
  public deleteTempshift(params) {
    return this._devicesRestService.deleteTempshift(params)
      .map((res) => res);
  }
  public addTempshift(params) {
    return this._devicesRestService.addTempshift(params)
      .map((res) => res);
  }
  public getDeviceStatus(params) {
    return this._devicesRestService.getDeviceStatus(params)
      .then((res) => res);
  }

  public updatedeviceForDeletion(id) {
    return this._devicesRestService.updateForDeletion(id)
      .then((res) => res);
  }


  public updateDevice(params) {
    return this._devicesRestService.updateDevice(params)
      .then((res) => res);
  }

  public updateDeviceStatus(id) {
    return this._devicesRestService.updateDeviceStatus(id)
      .then((res) => res);
  }

  public undoDeviceStatus(id) {
    return this._devicesRestService.undoDeviceStatus(id)
      .then((res) => res);
  }

  public ChangeBlockStatus(params) {
    return this._devicesRestService.ChangeBlockStatus(params)
      .then((res) => res);
  }

  public searchForDevices(params) {
    return this._devicesRestService.searchForDevices(params)
      .then((res) => res);
  }

  // public dispatcherSearch(params) {
  //   return this._devicesRestService.searching(params)
  //   .then((res) => res);
  // }

  public deleteDeviceArray(data) {
    return this._devicesRestService.deleteDeviceArray(data)
      .then((res) => res);
  }

}
