import { Injectable } from '@angular/core';
import { Headers, Http, RequestOptions} from '@angular/http';
import { environment } from '../../../../environments/environment';
import { ApiService } from '../api/api.service';
import 'rxjs/add/operator/toPromise';
import { AccessControlService } from '../access-control/access-control.service';

@Injectable()
export class UserlogService {

  private headers = new Headers({'content-type': 'application/json'});
  private getUrl = environment.apiUrl + '/access_control';

  constructor(private http: Http, private _apiService: ApiService,
    private accessControl: AccessControlService) { }
  public getUserLog(params) {
    return this._apiService.post(this.getUrl+'/getUserActivities', params).toPromise().then((res) => {
      if (this.accessControl.checkSession(res.status))
        return res;
    });
  }
}
