import { Injectable } from '@angular/core';
import { Headers, Http, RequestOptions} from '@angular/http';
import { environment } from '../../../../environments/environment';
import { ApiService } from '../api/api.service';
import 'rxjs/add/operator/toPromise';
import { AccessControlService } from '../access-control/access-control.service';


@Injectable()
export class VehicleCategoryRestService {

  private vehicleCategoryUrl = environment.apiUrl + '/categories';

  /**
   *
   * @param {Http} http
   * @param {ApiService} _apiService
   */
  constructor(private http: Http, private _apiService: ApiService,
    private accessControl: AccessControlService) { }

  /**
   * Function to get list of vehicle category
   *
   * @param params
   * @returns {Observable<any>}
   */
  public getVehicleCategory(params) {
    return this._apiService.post(this.vehicleCategoryUrl ,  params )
    .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res;
      });
  }

  /**
   * Function to get vehicle category by id
   *
   * @returns {Observable<any>}
   */
  public getVehicleCategoryById(id) {
    return this._apiService.post(this.vehicleCategoryUrl + '/getById/', id)
    .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res;
      });
  }

  /**
   * Function to save vehicle category
   *
   * @param params
   * @returns {Observable<any>}
   */
  public saveVehicleCategory(data) {
    return this._apiService.post(this.vehicleCategoryUrl + '/add', data )
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res;
      });
  }

  /**
   * Function to update vehicle category
   *
   * @param params
   * @returns {Observable<any>}
   */
  public updateVehicleCategory(params) {
    return this._apiService.post(this.vehicleCategoryUrl + '/updateCategory/', params)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res;
      });
  }

  /**
   * Function to delete vehicle category by id
   * 
   */
  public deleteVehicleCategory(id) {
    return this._apiService.post(this.vehicleCategoryUrl + '/updateForDelete/' , id)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res;
      });
  }

}