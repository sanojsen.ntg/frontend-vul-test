
import { Injectable } from '@angular/core';
import { Headers, Http, RequestOptions } from '@angular/http';
import { environment } from '../../../../environments/environment';
import { ApiService } from '../api/api.service';
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';
import { AccessControlService } from '../access-control/access-control.service';

@Injectable()

export class ShiftsrestService {
  private headers = new Headers({ 'content-type': 'application/json' });
  private authUrl = environment.authUrl;
  private shiftUrl = environment.apiUrl + '/shiftinfo';
  private shiftsUrl = environment.apiUrl + '/shifts';
  private shiftdetailUrl = environment.apiUrl + '/shift-details/addShiftDetail';

  /**
   *
   * @param {Http} http
   * @param {ApiService} _apiService
   */
  constructor(private http: Http, private _apiService: ApiService,
    private accessControl: AccessControlService) { };

  /**
   * Save Predefined-message details.
   * @param array data
   * @return {Promise<TResult2|TResult1>|Promise<TResult|any>|Promise<TResult>|Promise<any>}
   */
  public getshiftinfo(data) {
    return this._apiService.post(this.shiftUrl, data)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }

  public addshiftdetail(data) {
    return this._apiService.post(this.shiftdetailUrl, data)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }

  public getOrdersBasedOnShift(data) {
    return this._apiService.post(this.shiftsUrl + '/getOrders', data)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }

  /**
   * Get shift detail by driver id.
   * @param array data
   * @return {Promise<TResult2|TResult1>|Promise<TResult|any>|Promise<TResult>|Promise<any>}
   */
  public getShiftDetailByDriverId(data) {
    return this._apiService.post(this.shiftsUrl + '/shiftForDriver', data)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }

  public updateApprovedShifts(id) {
    return this._apiService.post(this.shiftsUrl + '/updateStatus/', id)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }

  public getShiftData(data) {
    return this._apiService.post(this.shiftsUrl + '/vehicle', data)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }

  public getAssignedDrivers(params) {
    return this._apiService.post(this.shiftsUrl + '/drivers', params)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }

  public getAllShifts(params) {
    return this._apiService.post(this.shiftsUrl + '/getAllShiftsForAdmin', params)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }

  public searchShifts(params) {
    return this._apiService.post(this.shiftsUrl + '/searchShifts', params)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }
  public getShiftIDs(params) {
    return this._apiService.post(this.shiftsUrl + '/getShiftIds', params)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }
  public getShiftCount(params) {
    return this._apiService.post(this.shiftsUrl + '/shiftsCount', params)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }
  public getShiftForCsv(params) {
    return this._apiService.post(this.shiftsUrl + '/shiftsForCsv', params)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }
  public getAllShiftsummary(params) {
    return this._apiService.post(this.shiftsUrl + '/shiftsCalculatedSummary', params)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }
  public getAssignedSignedOutDrivers(params) {
    return this._apiService.post(this.shiftsUrl + '/getDeviceShiftDetails', params)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }
}

