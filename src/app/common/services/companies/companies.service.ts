import { Injectable } from '@angular/core';
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';
import { ApiService } from '../api/api.service';
import { CompanyrestService } from './companiesrest.service';

@Injectable()

export class CompanyService {
  constructor(private _companyrestService: CompanyrestService) { }

  /**
   * Get all company
   * @param data 
   */
  public getCompanyListing(data) {
    return this._companyrestService.getCompanyListing(data)
      .then((res) => res);
  }

  /**
   * Add New company
   * @param data 
   */
  public addCompany(data) {
    return this._companyrestService.addCompany(data)
      .then((res) => res);
  }

  /**
   * Get a company by id
   * @param id 
   */
  public getCompanyById(id) {
    return this._companyrestService.getCompanyById(id)
      .then((res) => res);
  }

  /**
   * Update a comapny by id
   * @param id 
   * @param data 
   */
  public updateCompanyById(data) {
    return this._companyrestService.updateCompanyById(data)
      .then((res) => res);
  }

  /**
   * Update delete status of a company
   * @param id 
   */
  public updateDeletedStatus(id) {
    return this._companyrestService.updateDeletedStatus(id)
      .then((res) => res);
  }
}




