import { Injectable } from '@angular/core';
import { UsersRestService } from './userrest.service';
import { ApiService } from '../api/api.service';
import 'rxjs/add/operator/toPromise';
import { JwtService } from '../api/jwt.service';
import 'rxjs/Rx';
import {
  CanActivate,
  Router,
  ActivatedRouteSnapshot,
  RouterStateSnapshot
} from '@angular/router';
import { EncDecService } from '../encrypt-decrypt-service/encrypt_decrypt_service';


@Injectable()
export class UsersService implements CanActivate {

  /**
   *
   * @param {ApiService} _apiService
   * @param {UsersRestService} _usersRestService
   * @param {Router} router
   */
  constructor(
    private _apiService: ApiService,
    private _EncDecService: EncDecService,
    private _usersRestService: UsersRestService,
    private router: Router,
    private _jwtService: JwtService) {
  }

  /**
   * Activate route
   * @param route
   * @param state
   * @returns {boolean}
   */
  public canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    const url: string = state.url;
    return this._jwtService.verifyLogin(url);

  }
  /**
   * Function to login
   * @param data
   * @returns {Promise<T>}
   */
  public login(data: any) {
    return this._usersRestService.login(data)
      .then((res) => res);
  }
  public getSocketsession(data: any) {
    return this._usersRestService.getSocketsession(data)
      .then((res) => res);
  }
  public get_initial_token(data: any) {
    return this._usersRestService.get_initial_token(data)
      .then((res) => res);
  }
  /**
   * Function to get all users
   * @returns {Promise<T>}
   */
  public get(params) {
    return this._usersRestService.get(params)
      .then((res) => res);
  }

  /**
   * Function delete user by id
   * @param id
   * @returns {Promise<TResult2|TResult1>}
   */

  public delete(id) {
    return this._usersRestService.delete(id)
      .then((res) => res);
  }

  /**
   * Function create new user
   * @returns {Promise<T>}
   */
  public addUser(data) {
    return this._usersRestService.addUser(data)
      .then((res) => res);
  }

  /**
   * Get user by id
   * @param id
   * @returns {Promise<TResult2|TResult1>}
   */
  public getById(id) {
    return this._usersRestService.getUserById(id)
      .then((res) => res);
  }

  /**
   * Update user data by id
   * @param id
   * @param data
   * @returns {Promise<TResult2|TResult1>}
   */
  public updateUser(id, data) {
    let params = {
      name: data.name,
      email: data.email,
      parmanent_address: data.parmanent_address,
      gender: data.gender,
      dob: data.dob,
      addressId: data.addressId
    };
    return this._usersRestService.updateUser(id, params)
      .then((res) => res);
  }

  /**
   * Function for forgot password
   * @param data
   * @returns {Promise<T>}
   */
  public forgotPassword(data: any) {
    return this._usersRestService.forgotPassword(data)
      .then((res) => res);
  }

  /**
   * Verify JWT token
   * @param token
   * @returns {Promise<TResult2|TResult1>}
   */
  public verifyJWTToken(token, role) {
    return this._usersRestService.verifyJWTToken(token, role)
      .then((res) => res);
  }

  /**
   * Update password of a user
   * @param token
   * @param data
   * @returns {Promise<TResult2|TResult1>}
   */
  public updateUserPassword(data) {
    return this._usersRestService.updateUserPassword(data)
      .then((res) => res);
  }

  /**
   * Verify old password of a user
   * @param token
   * @param data
   * @returns {Promise<TResult2|TResult1>}
   */
  public verifyCurrentPassword(token, data) {
    let params = {
      current_password: data.current_password
    };
    return this._usersRestService.verifyCurrentPassword(token, params)
      .then((res) => res);
  }

}
