import { Injectable } from '@angular/core';
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';

import { ApiService } from '../api/api.service';
import { DriverGroupRestService } from './driver_groupsrest.service';

@Injectable()
export class DriverGroupService {

  constructor( private _driverGroupService: DriverGroupRestService) { }
  // params['search'] = keyword;
  public getDriversGroup (params) {
   return this._driverGroupService.getDriverGroup(params)
     .map((res) => res);
 }

  
  public addDriverGroup(data) {
    return this._driverGroupService.saveDriverGroup(data)
      .then((res) => res);
  }

   public getById(id) {
    return this._driverGroupService.getDriverGroupById(id)
      .then((res) => res);
  }

  public updateDriverGroupDetails(params) {
 
    return this._driverGroupService.updateDriverGroup( params)
      .then((res) => res);
  }

  public updateDriverGroupForDeletion(id) {
    return this._driverGroupService.updateDriverGroupForDeletion(id)
      .then((res) => res);
  }

   public deleteDriverGroup(id) {
    return this._driverGroupService.delete(id)
      .then((res) => res);
  }


}
