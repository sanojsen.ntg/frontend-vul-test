import { Injectable } from '@angular/core';
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';
import { ApiService } from '../api/api.service';
import { ShiftDetailsRestService } from './shift_detailsrest.service';

@Injectable()
export class ShiftsDetailsService {
  constructor(  private shiftDetailsRestService: ShiftDetailsRestService ) { }

  public getshiftdetails (data) {
    return this.shiftDetailsRestService.getshiftdetails(data)
      .then((res) => res);
  }

  public getshiftdetail (data) {
    return this.shiftDetailsRestService.getshiftdetail(data)
      .then((res) => res);
  }
}
