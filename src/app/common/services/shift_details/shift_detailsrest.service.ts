
import { Injectable } from '@angular/core';
import { Headers, Http, RequestOptions } from '@angular/http';
import { environment } from '../../../../environments/environment';
import { ApiService } from '../api/api.service';
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';
import { AccessControlService } from '../access-control/access-control.service';

@Injectable()

export class ShiftDetailsRestService {
  private headers = new Headers({ 'content-type': 'application/json' });
  private authUrl = environment.authUrl;
  private shiftdetailsUrl = environment.apiUrl + '/shifts';
  private shiftdetailUrl = environment.apiUrl + '/shift-details';

  /**
   *
   * @param {Http} http
   * @param {ApiService} _apiService
   */
  constructor(private http: Http, private _apiService: ApiService,
    private accessControl: AccessControlService) { };

  public getshiftdetails(data) {
    return this._apiService.post(this.shiftdetailsUrl, data)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }

  public getshiftdetail(data) {
    return this._apiService.post(this.shiftdetailUrl, data)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }
}

