import { Injectable } from '@angular/core';
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';

import { ApiService } from '../api/api.service';
import { AdditionalrestService } from './additional_servicerest.service';

@Injectable()
export class AdditionalService {
  constructor(private _additionalrestService: AdditionalrestService) {
  }
  public get(params) {
    
    return this._additionalrestService.get(params)
      .map((res) => res);
  }

  public getByName (params) {
    return this._additionalrestService.get(params)
      .map((res) => res);
  }

    /**
   *
   * @param params
   * @returns {Observable<any>}
   */
  public getaddService(params) {
    return this._additionalrestService.getaddService(params)
      .map((data) => data);

  }

     /**
   * Function to save  vehicle
   *
   * @returns {Observable<any>}
   */
  public saveServiceGroup(params) {
    return this._additionalrestService.saveServiceGroup(params)
      .then((data) => data);
  }

  /**
    * Function to delete selected Vehicle
    * @returns {Observable<any>}
   */
  public delete(id) {
    return this._additionalrestService.delete(id)
      .then((data) => data);
  }

  /**
    * Function to get Vehicle By Id
    * @returns {Observable<any>}
   */
  public getServiceById(id) {
    return this._additionalrestService.getServiceById(id)
      .then((data) => data);
  }


  /**
    * Function to Update vehicle
    *  @returns {Observable<any>}
   */
  public updateServiceGroup(params) {
    return this._additionalrestService.updateService(params)
      .then((data) => data);
  }


  public updateServiceForDeletion(id) {
    return this._additionalrestService.updateServiceForDeletion(id)
      .then((res) => res);
  }
}
