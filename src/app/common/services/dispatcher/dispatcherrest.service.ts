import { Injectable } from '@angular/core';
import { environment } from '../../../../environments/environment';
import { ApiService } from '../api/api.service';
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';
import { AccessControlService } from '../access-control/access-control.service';


@Injectable()


export class DispatcherRestService {

  private headers = new Headers({ 'content-type': 'application/json' });
  private userUrl = environment.apiUrl + '/users';
  private authUrl = environment.apiUrl + '/auth';


  constructor(private _apiService: ApiService,
    private accessControl: AccessControlService) {
  }
  public getDispatcherUser(params) {
    return this._apiService.post(this.userUrl + '/getDispatcherUSer', params)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }
  public verifyJWTToken(token, role) {
    return this._apiService.get(this.authUrl + '/verify-token?role=' + role + '&token=' + token)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }


}
