import { Injectable } from '@angular/core';
import { Headers, Http, RequestOptions} from '@angular/http';
import { environment } from '../../../../environments/environment';
import { ApiService } from '../api/api.service';
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';
import { AccessControlService } from '../access-control/access-control.service';

@Injectable()

export class PaymentTypeRestService {
    private headers = new Headers({'content-type': 'application/json'});
    private authUrl = environment.authUrl;
    private paymentTypeUrl = environment.apiUrl + '/paymenttype';
    
   /**
   *
   * @param {Http} http
   * @param {ApiService} _apiService
   */
 constructor( private http: Http, private _apiService: ApiService,
  private accessControl: AccessControlService ) { };

  /**
   * Save Payment-type details.
   * @param array data
   * @return {Promise<TResult2|TResult1>|Promise<TResult|any>|Promise<TResult>|Promise<any>}
   */
 public savePaymentTypeRest(data) {
   return this._apiService.post(this.paymentTypeUrl + '/create', data )
     .toPromise()
     .then((res) => {
      if (this.accessControl.checkSession(res.status))
        return res
    });
 }

  /**
   * Get all Payment-types.
   * @param params
   * @return {Promise<TResult2|TResult1>|Promise<TResult|any>|Promise<TResult>|Promise<any>}
   */
  public getPaymentTypes (params) {
    return this._apiService.post(this.paymentTypeUrl, params)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }
  
  /**
   * Update payment-type for corrsponding payment-type Id.
   * @param id
   * @param params
   * @return {Promise<TResult2|TResult1>|Promise<TResult|any>|Promise<TResult>|Promise<any>}
   */
  public updatePaymentType(params) {
    return this._apiService.post(this.paymentTypeUrl + '/update/',params)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }
  
  /**
   * Get payment-type  for corrsponding payment-type Id.
   * @param id
   * @return {Promise<TResult2|TResult1>|Promise<TResult|any>|Promise<TResult>|Promise<any>}
   */
  public getPaymentTypeById(id) {
    return this._apiService.post(this.paymentTypeUrl + '/getById/',id)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }
  
  /**
   * Delete payment-type  for corrsponding payment-type Id.
   * @param id
   * @return {Promise<TResult2|TResult1>|Promise<TResult|any>|Promise<TResult>|Promise<any>}
   */
  public deletePaymentTypeById(id) {
    return this._apiService.post(this.paymentTypeUrl + '/delete/',id)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }
  
   /**
   * Update delete status for corrsponding payment-type Id.
   * @param id
   * @return {Promise<TResult2|TResult1>|Promise<TResult|any>|Promise<TResult>|Promise<any>}
   */
  public updateDeletedStatus(id) {
    return this._apiService.post(this.paymentTypeUrl + '/updateDeleteStatus/',id)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }  
}
