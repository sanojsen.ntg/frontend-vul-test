import { TestBed, inject } from '@angular/core/testing';

import { TwoGisService } from './two-gis.service';

describe('TwoGisService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [TwoGisService]
    });
  });

  it('should be created', inject([TwoGisService], (service: TwoGisService) => {
    expect(service).toBeTruthy();
  }));
});
