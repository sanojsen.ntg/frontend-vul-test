import { Injectable } from '@angular/core';
import 'rxjs/add/operator/toPromise';

import { VehicleModelsRestService } from './vehiclemodelsrest.service';

@Injectable()
export class VehicleModelsService {

  constructor(private _vehicleModelsRestService: VehicleModelsRestService) {
  }

  /**
   * Function to get list of vehicle models
   *
   * @returns {Observable<any>}
   */
  public getVehicleModels(params) {
    return this._vehicleModelsRestService.getVehicleModels(params)
      .then((res) => res);
  }
  public getVehicleCategories(params) {
    return this._vehicleModelsRestService.getVehicleCategories(params)
      .then((res) => res);
  }
  public getVehicleCategoriesMessages(params) {
    return this._vehicleModelsRestService.getVehicleCategoriesMessages(params)
      .then((res) => res);
  }

  /**
   * Function to get vehicle model by id
   *
   * @returns {Observable<any>}
   */
  public getVehicleModelById(id) {
    return this._vehicleModelsRestService.getVehicleModelById(id)
      .then((res) => res);
  }
  public getVehicleCategoryById(id) {
    return this._vehicleModelsRestService.getVehicleCategoryById(id)
      .then((res) => res);
  }
  public getVehicleCategoryMessageById(id) {
    return this._vehicleModelsRestService.getVehicleCategoryMessageById(id)
      .then((res) => res);
  }

  /**
   * Function to save  vehicle models
   *
   * @returns {Observable<any>}
   */
  public saveVehicleModels(data) {
    return this._vehicleModelsRestService.saveVehicleModels(data)
      .then((res) => res);
  }
  public saveVehicleCategory(data) {
    return this._vehicleModelsRestService.saveVehicleCategory(data)
      .then((res) => res);
  }
  public saveVehicleCategoryMessages(data) {
    return this._vehicleModelsRestService.saveVehicleCategoryMessage(data)
      .then((res) => res);
  }

  /**
      * Function Update vehicle model by id.
      * 
   */
  public updateVehicleModel(params) {
    return this._vehicleModelsRestService.updateVehicleModel(params)
      .then((res) => res);
  }

  public updateVehicleCategory(params) {
    return this._vehicleModelsRestService.updateVehicleCategory(params)
      .then((res) => res);
  }
  public updateVehicleCategoryMessages(params) {
    return this._vehicleModelsRestService.updateVehicleCategoryMessages(params)
      .then((res) => res);
  }
  /**
   * Function delete vehicle model by id
   * 
   */
  public deleteVehicleModel(id) {
    return this._vehicleModelsRestService.deleteVehicleModel(id)
      .then((res) => res);
  }

  /**
      * Function Update status For Deletion.
      * 
   */
  public updateVehicleModelForDeletion(id) {

    return this._vehicleModelsRestService.updateVehicleModelForDeletion(id)
      .then((res) => res);
  }

  public updateVehicleCategoryForDeletion(id) {

    return this._vehicleModelsRestService.updateVehicleCategoryForDeletion(id)
      .then((res) => res);
  }
  public updateVehicleCategoryMessageForDeletion(id) {

    return this._vehicleModelsRestService.updateVehicleCategoryMessageForDeletion(id)
      .then((res) => res);
  }
}



