import { Injectable } from '@angular/core';
import { Headers, Http, RequestOptions} from '@angular/http';
import { environment } from '../../../../environments/environment';
import { ApiService } from '../api/api.service';
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';
import { AccessControlService } from '../access-control/access-control.service';

@Injectable()

export class MessagerestService {
  private headers = new Headers({'content-type': 'application/json'});
  private authUrl = environment.authUrl;
  private messagesUrl = environment.apiUrl + '/message';
  private preMessagesUrl = environment.apiUrl + '/predefinedmessages';

  /**
   *
   * @param {Http} http
   * @param {ApiService} _apiService
   */
  constructor(private http: Http, private _apiService: ApiService,
    private accessControl: AccessControlService) { };

  /**
   * Save order details.
   * @param array data
   * @return {Promise<TResult2|TResult1>|Promise<TResult|any>|Promise<TResult>|Promise<any>}
   */
  public preMessage(data) {
    return this._apiService.post(this.preMessagesUrl , data )
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }

  public sendMessage(params) {
    return this._apiService.post(this.messagesUrl + '/createMessage', params )
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }

  public sendBroadcastMessage(params) {
    return this._apiService.post(this.messagesUrl + '/sendBroadcastMessage', params )
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });

  }

  public getMessageHistory(params) {
    return this._apiService.post(this.messagesUrl + '/getMessageHistory', params )
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }

  public getAllMessagesHistory(params) {
    return this._apiService.post(this.messagesUrl + '/getAllMessageHistory', params )
      .toPromise()
      .then((res) =>{
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }
  
}
